@ECHO OFF
echo Uninstalling Accsys Framework API
echo ---------------------------------------------------
net stop "AccsysAPIServiceV1"
accsys.crmframework.api uninstall
echo ---------------------------------------------------
echo Done.
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace accsys.crmframework.api.infrastructure
{
    /* This is going to be the basis of the new payloasd 
     *
     */

    public class PayloadTemplate
    {
        //public string fieldName { get; set; } //the actual field name 
        public string value { get; set; } //value of the field
        public string translatedField { get; set; } //translations to be put against the custom_captions field
        public string translatedValue { get; set; } // translations to be put against the custom_captions field
        public string datatype { get; set; }
        public string characterLength { get; set; }
    }
}

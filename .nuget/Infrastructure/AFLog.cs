﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace accsys.crmframework.api.infrastructure
{
    public class AFLog
    {
        public string Path { get; set; }
        public string Request { get; set; }
        public int TotalRecs { get; set; }
        public string userId { get; set; }
        public string fullName { get; set; }
        public string HostName { get; set; }
        public string CreatedDate { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace accsys.crmframework.api.infrastructure
{
    public class MetadataObj
    {
        public string objectname { get; set; }
        //public string columnname { get; set; }
        public string translation { get; set; }
        public string translationValue { get; set; }
        public string captionCode { get; set; }
        public int position { get; set; }
        public string datatype { get; set; }
        public bool isprimarykey { get; set; }
        public bool isnullable { get; set; }
        public int? characterlength { get; set; }
        public int? numericprecision { get; set; }
        public int? numericscale { get; set; }
        public int? datetimeprecision { get; set; }
        public string value { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace accsys.crmframework.api.infrastructure
{
    public class UIPayload
    {
        public string column { get; set; }
        public string value { get; set; }
    }
}

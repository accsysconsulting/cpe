﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace accsys.crmframework.api.infrastructure
{
    public class UserMetadata
    {
        public string userId { get; set; }
        public string userFirstName { get; set; }
        public string userLastName { get; set; }
        public string userFullName { get; set; }
        public string userSID { get; set; }
        public string userRequest { get; set; } //this will determine whether the user did a read/write/delete etc.
        public string userDept { get; set; }
        public string userLogon { get; set; }
        public string userLoginTime { get; set; }
        public string userAdmin { get; set; }


    }
}

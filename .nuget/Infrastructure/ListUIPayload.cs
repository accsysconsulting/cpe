﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace accsys.crmframework.api.infrastructure
{
    public class ListUIPayload
    {
        public List<UIPayload> data { get; set; }
    }
}

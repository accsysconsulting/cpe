﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace accsys.crmframework.api.infrastructure
{
    public class Activity
    {
        public string sid { get; set; }
        public string createdDate { get; set; }
        public string updatedDate { get; set; }
        public string timeStamp { get; set; }
        public string deleted { get; set; }
        public string userid { get; set; }
        public string loginTime { get; set; }
        public string logoutTime { get; set; }
        public string logoutMethod { get; set; }
        public string duration { get; set; }
        public string id { get; set; }
        public string userLogon { get; set; }
        public string SERVERNAME { get; set; }
        public string dbName { get; set; }
        public string method { get; set; } //this could be useless if we're not using IIS
        public string ipAddress { get; set; } //client IP address
        public string appServer { get; set; }
        public string lastAction { get; set; }
        public string userFullName { get; set; }
        public string userDepartment { get; set; }
        public string userFirstName { get; set; }
        public string userLastName { get; set; }
        public string userPerAdmin { get; set; }


    }
}

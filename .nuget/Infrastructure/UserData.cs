﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace accsys.crmframework.api.infrastructure
{
    public class UserData
    {
        public string userName { get; set; }
        public string password { get; set; }
        public string sid { get; set; }
    }
}

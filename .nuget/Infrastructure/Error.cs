﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace accsys.crmframework.api.infrastructure
{
    class ErrorPayload
    {
        public string ErrorMessage { get; set; }

        public string FullException { get; set; }
      
        public string[] Errors { get; set; }
    }
}

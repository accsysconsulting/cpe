﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace accsys.crmframework.api.infrastructure
{
    public class  GetResponsePayload
    {
        public string _uri;
        public List<object> items;
        public List<object> metadata;
    }
}

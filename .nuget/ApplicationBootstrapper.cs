﻿using System;
using accsys.crmframework.api;
using accsys.crmframework.api.infrastructure;
using accsys.crmframework.api.Responses;
using Dapper;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.Conventions;
using Nancy.Diagnostics;
using Nancy.TinyIoc;
using System.Collections.Generic;
using System.Diagnostics;
using Nancy.Session;
using System.Timers;
using System.Configuration;

public class AccsysFrameworkBootstrapper : DefaultNancyBootstrapper
{

    //private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType); //old logging
    private static readonly log4net.ILog log = log4net.LogManager.GetLogger("StartUp"); //THIS POINTS TO startupLog.txt
    

    protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
    {
        CookieBasedSessions.Enable(pipelines);
        //StaticConfiguration.EnableRequestTracing = true; //this can affect the performance of the whole thing from a purist standpoint. THIS IS THE TRACKING!!!
        base.ApplicationStartup(container, pipelines);

        /* wtf is the point of the below code?
         * is this meant to be a place holder for Project Goldilocks?
         * if so...is that the reason that var gogo is named stupidly? 
         * why declare it and get the cache only to be nullified after?
         * 
         */

        Console.WriteLine("Do a lookup to get the Cache");

        //string res = GlobalVariables.LoadSqlScripts();
        string createScripts = ConfigurationManager.AppSettings["LoadSQLScripts"];
        if (createScripts == "1")
        {
            Console.WriteLine("LOADING SQL SCRIPTS....");
            try
            {
                SqlScripts.LoadSqlScripts();
                log.Info("MSG: CORE SCRIPTS Has been succesfully loaded");
                Console.WriteLine("LOADING SQL SCRIPTS COMPLETE");
            }
            catch(Exception ex)
            {
                log.Warn("MSG: " + ex.Message);
            }
        }
        else
        {
            Console.WriteLine("CONFIG IS NOT SET TO LOAD SQL SCRIPTS....");
            log.Warn("MSG: Core Scripts are not set to run");
        }


        //catch
        //{
        //    Process.GetCurrentProcess().Kill(); //don't let it start if the CORE SCRIPTS HAVENT BEEN LOADED!
        //}

        var schema = GlobalVariables.ViewSchema;
        var goldilocks = GlobalVariables.Metadata;
        var version = GlobalVariables.CheckCreateVersion();

        //var userData1 = GlobalVariables.Activity;
        var tes = GlobalVariables.GetUserMetadata();
        //var gogo = GlobalVariables.Metadata;
        //gogo = null;

        //var testVar = GlobalVariables.TestRefresh();

    }




    protected override void RequestStartup(TinyIoCContainer requestContainer, IPipelines pipelines, NancyContext context)
    {

        //Add the forms authentication here.... i think
        base.RequestStartup(requestContainer, pipelines, context);

        pipelines.OnError.AddItemToEndOfPipeline((z, a) =>
        {
            log.Error("Unhandled error on request: " + context.Request.Url + " : " + a.Message, a);
            // Return a standard error response payload
            return ErrorResponse.FromException(a);
        });
    }

    protected override void ConfigureRequestContainer(TinyIoCContainer container, NancyContext context)
    {
        base.ConfigureRequestContainer(container, context);

        Console.WriteLine("Incoming request inject now!");
        container.Register(typeof(ConnectionFactory), container.Resolve<ConnectionFactory>());

    }

    // Section for activating the Diagnostics 
    protected override DiagnosticsConfiguration DiagnosticsConfiguration
    {
        get { return new DiagnosticsConfiguration { Password = @"123" }; }
    }




}
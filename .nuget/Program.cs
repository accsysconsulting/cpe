﻿using System;
using System.Configuration;
using System.Web.Configuration;
using log4net.Config;
using Nancy.Hosting.Self;
using Topshelf;
// Setup for logging
[assembly: XmlConfigurator(ConfigFile = "logging.config", Watch = true)]

namespace accsys.crmframework.api
{
    public class NancySelfHost
    {
        private NancyHost _nancyHost;

        public void Start()
        {
            var listenPort = ConfigurationManager.AppSettings["listenport"];
            var baseURI = ConfigurationManager.AppSettings["baseuri"];
            var fullURI = string.Concat("http://localhost:", listenPort, baseURI);

            _nancyHost = new NancyHost(new Uri(fullURI));
            _nancyHost.Start();
            Console.WriteLine("Accsys API now listening - " + fullURI + " Press ctrl-c to stop");
        }

        public void Stop()
        {
            _nancyHost.Stop();
            Console.WriteLine("Good bye!");
        }
    }

    public class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void Main()
        {    
            HostFactory.Run(x =>                                    
            {
                x.Service<NancySelfHost>(s =>                       
                {
                    s.ConstructUsing(name => new NancySelfHost());  
                    s.WhenStarted(tc => tc.Start());                
                    s.WhenStopped(tc => tc.Stop());                 
                });

                x.UseLog4Net();
                x.RunAsLocalSystem();                               
                x.SetDescription("Provides the Accsys Framework access to the Sage CRM database and its records and acts as the in between layer of the Accsys Framework");
                x.SetDisplayName(ConfigurationManager.AppSettings["APIDisplayName"]);
                x.SetServiceName(ConfigurationManager.AppSettings["ServiceName"]);
                //x.SetDisplayName("AccsysAPIService");
                //x.SetServiceName("AccsysAPIServiceV1");
                x.StartAutomatically();

            });                                                     
        }
    }
}

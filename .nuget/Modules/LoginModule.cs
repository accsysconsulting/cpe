﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nancy;
using accsys.crmframework.api.Responses;
using accsys.crmframework.api.infrastructure;
using System.IO;
using accsys.crmframework.api.crmWeb; //webreference 
using System.Web.Services;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Dapper;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace accsys.crmframework.api.Modules
{
    public static class NancyExtensions
    {
        public static void EnableCors(this LoginModule module)
        {
            module.After.AddItemToEndOfPipeline(x =>
            {
                x.Response.WithHeader("Access-Control-Allow-Origin", "*");
                x.Response.WithHeader("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS");
                x.Response.WithHeader("Access-Control-Allow-Headers", "Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With");
            });
        }
    }

    public class LoginModule : NancyModule
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("UserLogger");
        public LoginModule(ConnectionFactory connectionFactory)
        {

            #region LOGIN MODULE

            Post["/login"] = parameters =>
            {

                this.EnableCors();
                
                NancyContext context = this.Context;
                List<string> payload = new List<string>();

                DynamicModelBinder dm = new DynamicModelBinder();

                string uiData = dm.ProcessJson2(context);               
                //payload = dm.ProcessJson(context);

                UserData userData = new UserData();
                crmWeb.WebService crm = new crmWeb.WebService();
                Dictionary<string, string> userDetails = new Dictionary<string, string>();

                try
                {
                    userDetails = JsonConvert.DeserializeObject<Dictionary<string, string>>(uiData);
                }
                catch (Exception ex)
                {
                    var errMsg = "Error reading payload: " + ex.Message + "See frameworkLog.txt for details";
                    //log.Error(response + System.Environment.NewLine + "STACK TRACE: " + ex.StackTrace.ToString());
                    return Response.AsError(HttpStatusCode.ExpectationFailed, errMsg);
                }

                foreach(var item in userDetails)
                {
                    if(item.Key == "username")
                    {
                        userData.userName = item.Value;
                    }
                    else if (item.Key == "password")
                    {
                        userData.password = item.Value;
                    }
                }

                //CALL THE WEBSERVICE HERE
                try
                {
                    logonresult CRMLogon = crm.logon(userData.userName, userData.password);
                    userData.sid = CRMLogon.sessionid.ToString();

                    /* INSERT TO USER METADATA CACHE AFTER GENERATING THE SID
                     * KNOWN VARS: username, pass, sid
                     * UNKNOWN VARS: loginTime, userId, user fullname, user dept, user_firstname, userlastname
                     */

                    //GET UNKNOWN VARS

                    #region GET UNKNOWN VARS FOR USERS

                    string userSQL = "select user_userid, user_firstname, user_lastname, user_department from users where user_logon = '" + userData.userName + "'";
                    List<object> userResults = new List<object>();
                    string[] rows1;
                    

                    Activity userMetadata = new Activity(); //once the results have been manipulated put in this object

                    List<Activity> userDataList = new List<Activity>();
                    

                    using(var con = connectionFactory.GetOpenConnection())
                    {
                        userResults = con.Query(userSQL).ToList();
                    }

                    #region PREPARE USER DATA TO BE INSERTED TO USER CACHE

                    if (userResults != null)
                    {
                        foreach (var tblResult in userResults)
                        {
                            string res1 = tblResult.ToString();
                            res1 = Regex.Replace(res1.Trim(), @"[\W_-[_=,'-/:]]+", " "); //this is used to allow special characters in the values
                            res1 = res1.Replace("DapperRow, ", "");
                            res1 = res1.Replace("NULL", "'NULL'");
                            
                            rows1 = res1.Split(new string[] { "'," }, StringSplitOptions.None);

                            foreach(var item in rows1)
                            {
                                if(item.Contains("user_userid"))
                                {
                                    string userRow = item.ToString();
                                    userRow = userRow.Replace(" user_userid = '", "");
                                    userMetadata.id = userRow;
                                }

                                if(item.Contains("user_firstname"))
                                {
                                    string userRow = item.ToString();
                                    userRow = userRow.Replace(" user_firstname = '", "");
                                    userMetadata.userFirstName = userRow;
                                }
                                if (item.Contains("user_lastname"))
                                {
                                    string userRow = item.ToString();
                                    userRow = userRow.Replace(" user_lastname = '", "");
                                    userMetadata.userLastName = userRow;
                                }

                                if (item.Contains("user_department"))
                                {
                                    string userRow = item.ToString();
                                    userRow = userRow.Replace(" user_department = '", "");
                                    userMetadata.userDepartment = userRow;
                                    userMetadata.userDepartment = userMetadata.userDepartment.Replace("'", "");
                                }
                            }

                        }

                        userMetadata.userFullName = userMetadata.userFirstName + " " + userMetadata.userLastName;
                        userMetadata.userLogon = userData.userName;
                        userMetadata.sid = userData.sid;
                        userMetadata.loginTime = DateTime.Now.ToString();


                    }
                    #endregion

                    userDataList.Add(userMetadata);

                    //once the above is finished add to the GlobalVariables.Activity.add(userData.sid, userMetadata);
                    var test = GlobalVariables.Activity;
                    GlobalVariables.Activity.Add(userData.sid, userDataList); //this should then add it to the users cache and at least allow to execute queries
                    //LOG WHO LOGS IN

                    string logonDate = DateTime.Now.ToString();

                    string logMsg = "SID: " + userData.sid + " | USER_LOGON_TIME: " + logonDate;

                    log.Info(logMsg);

                    #endregion
                }
                catch(Exception ex)
                {
                    log.Error("USERNAME: " + userData.userName + " | MSG: " + ex.Message);
                    return Response.AsError(HttpStatusCode.NotFound, ex.Message.ToString());
                }
                
                userDetails.Add("sid", userData.sid);

                var response = new { UserData = userDetails };

                return Response.AsJson(response);


            };
            #endregion

            #region LOGOUT MODULE

            Post["/logout"] = parameters =>
            {

                this.EnableCors();

                NancyContext context = this.Context;
                List<string> payload = new List<string>();
                DynamicModelBinder dm = new DynamicModelBinder();
                //payload = dm.ProcessJson(context);

                UserData userData = new UserData();
                crmWeb.WebService crm = new crmWeb.WebService();
                Dictionary<string, string> userDetails = new Dictionary<string, string>();
                
                string uiData = dm.ProcessJson2(context);
                try
                {
                    userDetails = JsonConvert.DeserializeObject<Dictionary<string, string>>(uiData);
                }
                catch (Exception ex)
                {
                    var errMsg = "Error reading payload: " + ex.Message + "See frameworkLog.txt for details";
                    //log.Error(response + System.Environment.NewLine + "STACK TRACE: " + ex.StackTrace.ToString());
                    return Response.AsError(HttpStatusCode.ExpectationFailed, errMsg);
                }

                foreach (var item in userDetails)
                {
                    if (item.Key == "sid")
                    {
                        userData.sid = item.Value;
                    }
                }

                //CALL THE WEBSERVICE HERE
                try
                {
                    crm.SessionHeaderValue = new SessionHeader();
                    crm.SessionHeaderValue.sessionId = userData.sid;

                    logoffresult CRMLogout = crm.logoff(crm.SessionHeaderValue.sessionId);

                    //remove from the USER cache
                    GlobalVariables.Activity.Remove(userData.sid);

                    string logonDate = DateTime.Now.ToString();

                    string logMsg = "USERID: " + userData.sid + " | USER_LOGOUT_TIME: " + logonDate;

                    log.Info(logMsg);


                }
                catch (Exception ex)
                {
                    log.Error("USERNAME: " + userData.sid+ " | MSG: " + ex.Message);
                    return Response.AsError(HttpStatusCode.NotFound, ex.StackTrace.ToString());
                }

                var response = new { date = DateTime.Now };

                return Response.AsJson(response);


            };

            #endregion

            #region OPTIONS
            
            Options["login/"] = parameters =>
            {
                this.EnableCors();
                return HttpStatusCode.NoContent;
            };

            Options["logout/"] = parameters =>
            {
                this.EnableCors();
                return HttpStatusCode.NoContent;
            };

            #endregion

        }

    }
}

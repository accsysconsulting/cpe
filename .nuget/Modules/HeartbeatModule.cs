using accsys.crmframework.api.Responses;
using Nancy;

namespace accsys.crmframework.api
{
    
    public class HeartbeatModule : NancyModule
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("FrameworkLogger");
        public HeartbeatModule()
            : base("/heartbeat")
        {
            Get["/"] = parameters =>
            {
                var response = HeartbeatResponse.GenerateHeartbeat();
                response.StatusCode = HttpStatusCode.OK;
                string perfMsg = "PATH: " + Request.Url.Path.ToString() +  " | STATUS_CODE: " + response.StatusCode.ToString() + " | " + response;
                log.Info(perfMsg);

                return response;
            };
        }
    }
}

using System;
using System.Collections.Generic;
using System.Dynamic;
using accsys.crmframework.api.infrastructure;
using Dapper;
using Nancy;
using Nancy.ViewEngines;

namespace accsys.crmframework.api
{
    public class TestingModule : NancyModule
    {

        public TestingModule(ConnectionFactory connectionFactory)
        {
            Get["/"] = parameters => {
                return View["staticview", Request.Url];
            };

            Get["/testing"] = parameters =>
            {
                return View["staticview", Request.Url];
            };

            Get["/showmeanerror"] = parameters =>
            {
               
                    throw new NotImplementedException();
            };

        }
    }
}
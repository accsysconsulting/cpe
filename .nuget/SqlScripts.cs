﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using accsys.crmframework.api.infrastructure;
using Dapper;
using Nancy;
using System.IO;
using System.Data.Common;
using System.Data.SqlClient;
using System.Reflection;
using System.Diagnostics;


namespace accsys.crmframework.api
{
    public class SqlScripts
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("GlobalVariables");

        public static void LoadSqlScripts() //CALL THIS AT START-UP
        {
            LoadCoreScripts();
        }

        public static string LoadCoreScripts()
        {
            var connectionFactory = new ConnectionFactory();
            string executablePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string coreSqlPath = "";
            string sqlScriptPath = Path.Combine(executablePath, "SQLSCRIPTS\\CORE");
            string newl = Environment.NewLine;
            string loadScript = "";

            using (var con = connectionFactory.GetOpenConnection())
            {
                try
                {
                    string[] sqlScripts;
                    log.Info("Executing SQL Scripts");
                    sqlScripts = Directory.GetFiles(sqlScriptPath); //contains all the filenames of the sql scripts

                    //run the sql script here
                    foreach (string file in sqlScripts)
                    {
                        //string filePath = Path.Combine(sqlScriptPath, fileName); //filepath of the sql script
                        FileInfo fileData = new FileInfo(file);
                        
                        string script = fileData.OpenText().ReadToEnd(); //actual SQL script
                        try
                        {
                            //need to split the views up because mono doesn't support SMO that executes T-SQL specifically
                            #region Process SQL Scripts

                            string[] scriptItems = script.Split(new string[] { "GO" }, StringSplitOptions.None);

                            foreach (string statement in scriptItems)
                            {
                                try
                                {
                                    if (statement != "")
                                    {
                                        if (statement != null)
                                        {
                                            con.Execute(statement);
                                        }
                                    }

                                    //NEED TO INSERT IT BACK TO THE METADATA
                                }
                                catch (Exception ex)
                                {
                                    string error = "SQL ERROR STATMENT LVL: " + statement + newl + "STACK TRACE: " + ex.Message;
                                    log.Error(error);
                                    //return error;
                                }
                            }

                            log.Info("SQL SCRIPT HAS NOW BEEN CREATED");
                            //return "success";
                            #endregion

                            loadScript = "true";
                        }
                        catch (Exception ex)
                        {
                            string error = "ERROR EXECUTING SQL SCRIPT(s): " + file + newl + "STACK TRACE: " + ex.Message;
                            Console.WriteLine(error);
                            log.Error(error);
                            loadScript = error;
                        }
                    }
                }
                catch(Exception ex)
                {
                    string error = "ERROR LOADING SQL SCRIPTS: " + ex.Message;
                    log.Error(error);
                }


            } // <-- end of using statment

            return loadScript;

        } //<-- end of LoadCoreScripts()

    }
}

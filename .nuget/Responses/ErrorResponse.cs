﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using accsys.crmframework.api.infrastructure;
using Nancy;
using Nancy.Responses;
using Nancy.Serializers.Json.ServiceStack;

namespace accsys.crmframework.api.Responses
{
    public class ErrorResponse : JsonResponse
    {
        readonly ErrorPayload error;

        private ErrorResponse(ErrorPayload error)
            : base(error, new DefaultJsonSerializer())
        {
            this.error = error;
        }

        public string ErrorMessage { get { return error.ErrorMessage; } }
        public string FullException { get { return error.FullException; } }
        public string[] Errors { get { return error.Errors; } }

        public static ErrorResponse FromMessage(string message)
        {
            return new ErrorResponse(new ErrorPayload { ErrorMessage = message });
        }

        public static ErrorResponse FromException(Exception ex)
        {
            var exception = ex.GetBaseException();

            var summary = exception.Message;
            if (exception is WebException || exception is SocketException)
            {
                // Commonly returned when connections to the DB fail
                summary = "The Accsys API windows service may not be running: " + summary;
            }

            var statusCode = Nancy.HttpStatusCode.InternalServerError;

            var error = new ErrorPayload { ErrorMessage = summary, FullException = exception.ToString() };

            // Handle Special cases
            

            var response = new ErrorResponse(error);
            response.StatusCode = statusCode;
            return response;
        }

       
    }
}

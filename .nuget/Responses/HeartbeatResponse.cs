﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using accsys.crmframework.api.infrastructure;
using Nancy;
using Nancy.Responses;

namespace accsys.crmframework.api.Responses
{
    public class HeartbeatResponse : JsonResponse
    {
        readonly HeartbeatPayload Heartbeat;

        private HeartbeatResponse(HeartbeatPayload heartbeat)
                : base(heartbeat, new DefaultJsonSerializer())
        {
            this.Heartbeat = heartbeat;
        }

        public static HeartbeatResponse GenerateHeartbeat()
         {
             return new HeartbeatResponse(new HeartbeatPayload { message = "bump" });
         }

   
    }
}

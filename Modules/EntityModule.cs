using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using accsys.crmframework.api.infrastructure;
using Dapper;
using Nancy;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Nancy.ModelBinding;
using System.Timers;
using System.IO;
using System.Web.Script.Serialization;
using ServiceStack.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Collections.Specialized;

namespace accsys.crmframework.api
{
    public static class NancyExtensions
    {
        public static void EnableCors(this NancyModule module)
        {
            module.After.AddItemToEndOfPipeline(x =>
            {
                x.Response.WithHeader("Access-Control-Allow-Origin", "*");
                x.Response.WithHeader("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS");
                x.Response.WithHeader("Access-Control-Allow-Headers", "Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With");
            });
        }
    }

    public class EntityModule : NancyModule
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("FrameworkLogger");
        public EntityModule(ConnectionFactory connectionFactory)
        {
            //CONSTANTS
            //Dictionary<string, List<Activity>> UserMetadata = new Dictionary<string, List<Activity>>();
            //UserMetadata = GlobalVariables.GetUserMetadata();
            //GlobalVariables.TestRefresh();
            //the question is does this run the above once? or not..

            #region OLD METHODS

            //GetMetadata - use for Goldilocks testing
            //NOTE: Chrome will freeze if you given out as a payload
            Get["/metadata/"] = parameters =>
            {
                this.EnableCors();

                var goldilocks = GlobalVariables.Metadata; //now i've got an object that I can index.

                if (goldilocks != null)
                {
                    // var test = results.table.fieldNameslookup;
                    var payload = new { _url = Request.Url, _goldilocks = goldilocks };

                    return Response.AsJson(payload);
                }
                else
                {
                    return HttpStatusCode.NotFound;
                }
            };

            //Return the metadata for a given entity

            Get["/metadata/{entity}"] = parameters =>
            {
                this.EnableCors();

                // cleanup the params as it comes in
                var entityName = ((string)parameters.entity).Trim().ToLower();

                List<Metadata> metadata;
                List<Dictionary<string, MetadataObj>> payloadMetadata = new List<Dictionary<string, MetadataObj>>(); //this is what gets returned
                Dictionary<string, MetadataObj> record = new Dictionary<string,MetadataObj>();
                ListDictionary rec = new ListDictionary();
                if (GlobalVariables.Metadata.TryGetValue(entityName, out metadata))
                {
                    var lookup = metadata.ToLookup(m => m.columnname, m => m);
                    
                    foreach(Metadata item in metadata)
                    {
                        MetadataObj metaObj = new MetadataObj();

                        metaObj.captionCode = item.captionCode;
                        metaObj.characterlength = item.characterlength;
                        metaObj.datatype = item.datatype;
                        metaObj.datetimeprecision = item.datetimeprecision;
                        metaObj.isnullable = item.isnullable;
                        metaObj.isprimarykey = item.isprimarykey;
                        metaObj.numericprecision = item.numericprecision;
                        metaObj.numericscale = item.numericscale;
                        metaObj.position = item.position;
                        metaObj.translation = item.translation;
                        //metaObj.translationValue = item.translationValue;
                        metaObj.value = item.value;
                        metaObj.objectname = item.objectname;

                        //HACK - THIS IS TO AVOID MULTIPLE ITEMS COMING INTO THE PAYLOAD. Reason for this is for column names with multiple translations.
                        if(!record.ContainsKey(item.columnname.ToLower()))
                        {
                            record.Add(item.columnname.ToLower(), metaObj);
                        }                        
                        
                    }

                    // var test = results.table.fieldNameslookup;
                    var payload = new { _url = Request.Url, _metadata = record };

                    return Response.AsJson(payload);
                }
                else
                {
                    return HttpStatusCode.NotFound;
                }
            };

            // Placeholder for entity dedupe


            //Return an entity collection

            Get["/{entity}"] = parameters =>
            {
                Stopwatch timer = Stopwatch.StartNew();
                this.EnableCors();
                var entityName = ((string)parameters.entity).Trim().ToLower();
                List<Metadata> metadata;
                if (GlobalVariables.Metadata.TryGetValue(entityName, out metadata))
                {
                    List<object> entityResults = new List<object>();
                    List<Dictionary<string, PayloadTemplate>> payloadList = new List<Dictionary<string, PayloadTemplate>>();
                    
                    using (var con = connectionFactory.GetOpenConnection())
                    {
                        string sql = "SELECT * FROM [dbo].[" + entityName + "]";
                        try
                        {
                            entityResults = con.Query(sql).ToList();

                            if (entityResults.Count != 0)
                            {
                                payloadList = FormatPayload(metadata, entityResults);
                            }
                        }
                        catch(Exception ex)
                        {
                            string response = "MSG: " + ex.Message;
                            Response ret = Response.AsError(HttpStatusCode.NotFound, response);

                            timer.Stop();
                            TimeSpan timespan = timer.Elapsed;
                            string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                            string ms = timespan.TotalMilliseconds.ToString();
                            string min = timespan.TotalMinutes.ToString();
                            string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | ENTITY: " + entityName + " | STATUS_CODE: " + ret.StatusCode.ToString() + " | " + response;
                            log.Error(perfMsg);

                            return ret;
                        }
                    }

                    var payload = new { _url = Request.Url, payload = payloadList};

                    return Response.AsJson(payload);
                }
                else
                {
                    string response = "MSG :" + entityName + " doesn't exist in metadata please check that the entity name is spelt correctly.";
                    Response ret = Response.AsError(HttpStatusCode.NotFound, response);

                    timer.Stop();
                    TimeSpan timespan = timer.Elapsed;
                    string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                    string ms = timespan.TotalMilliseconds.ToString();
                    string min = timespan.TotalMinutes.ToString();
                    string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | ENTITY: " + entityName + " | STATUS_CODE: " + ret.StatusCode.ToString() + " | " + response;
                    log.Info(perfMsg);

                    return ret;
                }

            };

            #endregion

            #region GETS

            #region PLAIN GETS
            /* Pass the clause into a method then spit out the results 
             * split with (&) use ampersand as delimeter for both fetch and clause
             */
            Get["/{entity}/{fields}/{where}/{sid}"] = parameters =>
            {
                //[PERF-BENCH]
                Stopwatch timer = Stopwatch.StartNew();

                this.EnableCors();
                #region PLAIN GET VARS
                // cleanup the params as it comes in
                var entityName = ((string)parameters.entity).Trim().ToLower();
                var sid = ((string)parameters.sid).Trim();
                var fields = ((string)parameters.fields).Trim();
                var where = ((string)parameters.where).Trim();
                string selectClause, whereClause, sql;
                selectClause = FilterQuery(fields);
                whereClause = FilterQuery(where);
                #endregion

                List<Metadata> metadata;
                if (GlobalVariables.Metadata.TryGetValue(entityName, out metadata)) //entityName is the key in the dictionary, so when searching values in the metadata list, need to firstly get the entityName THEN search for the values
                {
                    List<object> entityResults = new List<object>();

                    List<Dictionary<string, PayloadTemplate>> payloadList = new List<Dictionary<string, PayloadTemplate>>();
                    UserMetadata userMetadata = new UserMetadata();

                    //WHEN THE GETUSERID METHOD IS DONE TAKE IT OUTSIDE OF THE CONNECTION.

                    using (var con = connectionFactory.GetOpenConnection())
                    {
                        #region GETTING USERIDS FROM SID

                        List<Activity> userData; //= new List<Activity>();
                        //List<object> sidIDList = new List<object>();
                        List<object> userDetails = new List<object>();


                        string sqlSID = "SELECT Acty_UserID FROM Activity where acty_logout is null and acty_logoutMethod is null and acty_SID = '" + sid + "'";
                        // Query that gets the user Id from the Activity table
                        //userMetadata.userSID = sid;
                        int count = 0; //used for checking the SID
                        try
                        {

                            RetryLogin:
                            GlobalVariables.Activity.TryGetValue(sid, out userData); //userData will always have one value in it. It can never have the same SID twice
                            //bool sidValid = false;

                            if (userData != null)
                            {
                                #region Process SID
                                
                                foreach (var userDetail in userData)
                                {
                                    if (userDetail.sid.TrimEnd() == sid) //initial check of the SID - this should be where you should check and refresh..
                                    {
                                        if (userDetail.id != null)
                                        {
                                            userMetadata.userFirstName = userDetail.userFirstName;
                                            userMetadata.userLastName = userDetail.userLastName;
                                            userMetadata.userSID = userDetail.sid;
                                            userMetadata.userRequest = this.Request.Method.ToString();
                                            userMetadata.userId = userDetail.id;
                                            userMetadata.userFullName = userDetail.userFullName;
                                            userMetadata.userLoginTime = userDetail.loginTime;
                                            userMetadata.userLogon = userDetail.userLogon;
                                            userMetadata.userDept = userDetail.userDepartment;
                                        }
                                        else
                                        {
                                            var response = "Invalid User ID! Please Ensure ID is not null. ";
                                            return Response.AsError(HttpStatusCode.Unauthorized, response);
                                        }

                                    }
                                    else
                                    {
                                        var response = "Invalid SID! Please ensure that the user is still in session. ";
                                        return Response.AsError(HttpStatusCode.Unauthorized, response);
                                    }
                                }

                                #endregion

                            }
                            else if (count == 0) //1 attempt max - for testing 
                            {
                                count++;
                                GlobalVariables.GetUserMetadata();
                                goto RetryLogin;
                            }
                            else
                            {
                                var response = "Could not retrieve user Id! Please ensure that the user is logged in";
                                return Response.AsError(HttpStatusCode.ExpectationFailed, response);
                            }


                        }
                        catch
                        {
                            var response = "Could not retrieve user Id! Please ensure there is a connection to the database";
                            return Response.AsError(HttpStatusCode.ExpectationFailed, response);
                        }



                        #endregion

                        var metadataFields = FindMetadataFields(metadata);

                        if (metadataFields.deleted == null)
                        {
                            sql = selectClause + " FROM " + entityName + " WHERE " + whereClause;
                        }
                        else
                        {
                            sql = selectClause + " FROM " + entityName + " WHERE " + whereClause + "AND " + metadataFields.deleted + " is NULL";
                        }

                        try
                        {
                            entityResults = con.Query(sql).ToList();
                        }
                        catch(Exception ex)
                        {
                            string response = "MSG: " + ex.Message;
                            Response ret = Response.AsError(HttpStatusCode.NotFound, response);

                            timer.Stop();
                            TimeSpan timespan = timer.Elapsed;
                            string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                            string ms = timespan.TotalMilliseconds.ToString();
                            string min = timespan.TotalMinutes.ToString();
                            string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | ENTITY: " + entityName + " | STATUS_CODE: " + ret.StatusCode.ToString() + " | " + response;
                            log.Error(perfMsg);

                            return ret;
                        }
                        

                        if (entityResults.Count != 0)
                        {
                            payloadList = FormatPayload(metadata, entityResults);
                        }


                    }
                    
                    //var payload = new { userMetadata = userMetadata, _url = Request.Url, payload = payloadList, items = entityResults, _metadata = metadata }; 
                    var payload = new { userMetadata = userMetadata, _url = Request.Url, payload = payloadList };

                    Response retSuccess = Response.AsJson(payload);

                    //[PERF-BENCH]
                    #region PERF-BENCH

                    timer.Stop();
                    TimeSpan timespanSuccess = timer.Elapsed;
                    string timeElapsedSecondsSuccess = timespanSuccess.TotalSeconds.ToString();
                    string msSuccess = timespanSuccess.TotalMilliseconds.ToString();
                    string minSuccess = timespanSuccess.TotalMinutes.ToString();
                    //Console.WriteLine("Elapsed Time in Seconds: " + timeElapsedSeconds);
                    //Console.WriteLine("Elapsed Time in Milliseconds: " + ms);
                    //Console.WriteLine("Elapsed Time in Minutes: " + min);
                    string perfMsgSuccess = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSecondsSuccess + " | TOTAL_RECORDS: " + payloadList.Count() + " | USER: " + userMetadata.userFullName + " | USERID: " + userMetadata.userId + " | ENTITY: " + entityName + " | STATUS_CODE: " + retSuccess.StatusCode.ToString();
                    log.Info(perfMsgSuccess);

                    //INSERT INTO table _tAFLogs

                    //AFLog logRecord = new AFLog();

                    //logRecord.CreatedDate = DateTime.Now.ToString();
                    //logRecord.fullName = userMetadata.userFullName;
                    //logRecord.HostName = Request.Url.HostName;
                    //logRecord.Path = Request.Url.Path;
                    //logRecord.Request = userMetadata.userRequest;
                    //logRecord.TotalRecs = payloadList.Count();
                    //logRecord.userId = userMetadata.userId;

                    //string sqlLog = "INSERT INTO _tAFLogs (Path, Request, TotalRecords, UserId, UserfullName, Hostname, CreatedDate, LoadTime) VALUES ('" + Request.Url.Path + "', '" + userMetadata.userRequest + "', '" + payloadList.Count() + "', '" + userMetadata.userId + "', '" + userMetadata.userFullName + "', '" + Request.Url.HostName + "', '" + DateTime.Now.ToString() + "', '" + timespan.TotalSeconds + "')";
                    //con.Execute(sqlLog);


                    #endregion

                    return retSuccess;


                }
                else
                {
                    string response = "MSG :" + entityName + " doesn't exist in metadata please check that the entity name is spelt correctly.";
                    Response ret = Response.AsError(HttpStatusCode.NotFound, response);

                    timer.Stop();
                    TimeSpan timespan = timer.Elapsed;
                    string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                    string ms = timespan.TotalMilliseconds.ToString();
                    string min = timespan.TotalMinutes.ToString();
                    //Console.WriteLine("Elapsed Time in Seconds: " + timeElapsedSeconds);
                    //Console.WriteLine("Elapsed Time in Milliseconds: " + ms);
                    //Console.WriteLine("Elapsed Time in Minutes: " + min);
                    string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | ENTITY: " + entityName + " | STATUS_CODE: " + ret.StatusCode.ToString() + " | " + response;
                    log.Info(perfMsg);

                    return ret;
                    //return HttpStatusCode.NotFound;
                }

            };
            #endregion

            #region GLOBAL SEARCH

            /* GLOBAL SEARCH FUNCTION
             */

            Get["/search/query={terms}/"] = parameters =>
            {
                //[PERF-BENCH]
                Stopwatch timer = Stopwatch.StartNew();

                this.EnableCors();

                var searchTerms = parameters.terms; //don't clean up as it will have spaces

                string sql;

                sql = "_spAFSearch '" + searchTerms + "'";
                List<object> entityResults;

                try
                {
                    using (var con = connectionFactory.GetOpenConnection())
                    {
                        entityResults = con.Query(sql).ToList();

                    }

                    var payload = new { _url = Request.Url, items = entityResults };

                    return Response.AsJson(payload);
                }

                catch
                {
                    return HttpStatusCode.NotFound;
                }

            };

            #endregion

            #region GETS WITH SORT


            /* GET with SORTING OPTION */

            Get["/{entity}/{fields}/{where}/{order}/{sid}"] = parameters =>
            {
                //[PERF-BENCH]
                Stopwatch timer = Stopwatch.StartNew();

                this.EnableCors();

                // cleanup the params as it comes in

                #region GET with SORT vars

                var entityName = ((string)parameters.entity).Trim().ToLower();
                var sid = ((string)parameters.sid).Trim();
                var fields = ((string)parameters.fields).Trim();
                var where = ((string)parameters.where).Trim();
                var order = ((string)parameters.order).Trim();


                string selectClause, whereClause, sql, orderClause;
                selectClause = FilterQuery(fields);
                whereClause = FilterQuery(where);
                orderClause = SortHandler(order);

                #endregion

                List<Metadata> metadata;
                if (GlobalVariables.Metadata.TryGetValue(entityName, out metadata)) //entityName is the key in the dictionary, so when searching values in the metadata list, need to firstly get the entityName THEN search for the values
                {
                    List<object> entityResults = new List<object>();

                    List<Dictionary<string, PayloadTemplate>> payloadList = new List<Dictionary<string, PayloadTemplate>>();
                    UserMetadata userMetadata = new UserMetadata();



                    using (var con = connectionFactory.GetOpenConnection())
                    {
                        #region GETTING USERIDS FROM SID

                        List<Activity> userData; //= new List<Activity>();
                        //List<object> sidIDList = new List<object>();
                        List<object> userDetails = new List<object>();


                        string sqlSID = "SELECT Acty_UserID FROM Activity where acty_logout is null and acty_logoutMethod is null and acty_SID = '" + sid + "'";
                        // Query that gets the user Id from the Activity table
                        //userMetadata.userSID = sid;

                        try
                        {
                            int count = 0;
                            RetryLogin:
                            GlobalVariables.Activity.TryGetValue(sid, out userData); //userData will always have one value in it. It can never have the same SID twice
                            //bool sidValid = false;

                            if (userData != null)
                            {
                                #region Process SID

                                foreach (var userDetail in userData)
                                {
                                    if (userDetail.sid.TrimEnd() == sid) //initial check of the SID - this should be where you should check and refresh..
                                    {
                                        if (userDetail.id != null)
                                        {
                                            userMetadata.userFirstName = userDetail.userFirstName;
                                            userMetadata.userLastName = userDetail.userLastName;
                                            userMetadata.userSID = userDetail.sid;
                                            userMetadata.userRequest = this.Request.Method.ToString();
                                            userMetadata.userId = userDetail.id;
                                            userMetadata.userFullName = userDetail.userFullName;
                                            userMetadata.userLoginTime = userDetail.loginTime;
                                            userMetadata.userLogon = userDetail.userLogon;
                                            userMetadata.userDept = userDetail.userDepartment;
                                        }
                                        else
                                        {
                                            var response = "Invalid User ID! Please Ensure ID is not null. ";
                                            return Response.AsError(HttpStatusCode.Unauthorized, response);
                                        }

                                    }
                                    else
                                    {
                                        var response = "Invalid SID! Please ensure that the user is still in session. ";
                                        return Response.AsError(HttpStatusCode.Unauthorized, response);
                                    }
                                }

                                #endregion

                            }
                            else if (count == 0) //1 attempt max - for testing 
                            {
                                count++;
                                GlobalVariables.GetUserMetadata();
                                goto RetryLogin;
                            }
                            else
                            {
                                var response = "Could not retrieve user Id! Please ensure that the user is logged in";
                                return Response.AsError(HttpStatusCode.ExpectationFailed, response);
                            }
                        }
                        catch
                        {
                            var response = "Could not retrieve user Id! Please ensure there is a connection to the database";
                            return Response.AsError(HttpStatusCode.ExpectationFailed, response);
                        }



                        #endregion

                        var metadataFields = FindMetadataFields(metadata);

                        if (metadataFields.deleted == null)
                        {
                            sql = selectClause + " FROM " + entityName + " WHERE " + whereClause + " " + orderClause;
                        }
                        else
                        {
                            sql = selectClause + " FROM " + entityName + " WHERE " + whereClause + "AND " + metadataFields.deleted + " is NULL " + orderClause;
                        }

                        try
                        {
                            entityResults = con.Query(sql).ToList();
                        }
                        catch(Exception ex)
                        {
                            string response = "MSG: " + ex.Message;
                            Response ret = Response.AsError(HttpStatusCode.NotFound, response);

                            timer.Stop();
                            TimeSpan timespan = timer.Elapsed;
                            string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                            string ms = timespan.TotalMilliseconds.ToString();
                            string min = timespan.TotalMinutes.ToString();
                            string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | ENTITY: " + entityName + " | STATUS_CODE: " + ret.StatusCode.ToString() + " | " + response;
                            log.Error(perfMsg);
                        }

                        if (entityResults.Count != 0)
                        {
                            payloadList = FormatPayload(metadata, entityResults);
                        }

                    }

                    //make sure to add this part to all of the routes
                    //var payload = new { userMetadata = userMetadata, _url = Request.Url, payload = payloadList, items = entityResults, _metadata = metadata };
                    var payload = new { userMetadata = userMetadata, _url = Request.Url, payload = payloadList };
                    Response retSuccess = Response.AsJson(payload);

                    //[PERF-BENCH]
                    #region PERF-BENCH

                    timer.Stop();
                    TimeSpan timespanSuccess = timer.Elapsed;
                    string timeElapsedSecondsSuccess = timespanSuccess.TotalSeconds.ToString();
                    string msSuccess = timespanSuccess.TotalMilliseconds.ToString();
                    string minSuccess = timespanSuccess.TotalMinutes.ToString();
                    //Console.WriteLine("Elapsed Time in Seconds: " + timeElapsedSeconds);
                    //Console.WriteLine("Elapsed Time in Milliseconds: " + ms);
                    //Console.WriteLine("Elapsed Time in Minutes: " + min);
                    string perfMsgSuccess = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSecondsSuccess + " | TOTAL_RECORDS: " + payloadList.Count() + " | USER: " + userMetadata.userFullName + " | USERID: " + userMetadata.userId + " | ENTITY: " + entityName + " | STATUS_CODE: " + retSuccess.StatusCode.ToString();
                    log.Info(perfMsgSuccess);

                    //INSERT INTO table _tAFLogs

                    //AFLog logRecord = new AFLog();

                    //logRecord.CreatedDate = DateTime.Now.ToString();
                    //logRecord.fullName = userMetadata.userFullName;
                    //logRecord.HostName = Request.Url.HostName;
                    //logRecord.Path = Request.Url.Path;
                    //logRecord.Request = userMetadata.userRequest;
                    //logRecord.TotalRecs = payloadList.Count();
                    //logRecord.userId = userMetadata.userId;

                    //string sqlLog = "INSERT INTO _tAFLogs (Path, Request, TotalRecords, UserId, UserfullName, Hostname, CreatedDate, LoadTime) VALUES ('" + Request.Url.Path + "', '" + userMetadata.userRequest + "', '" + payloadList.Count() + "', '" + userMetadata.userId + "', '" + userMetadata.userFullName + "', '" + Request.Url.HostName + "', '" + DateTime.Now.ToString() + "', '" + timespan.TotalSeconds + "')";
                    //con.Execute(sqlLog);


                    #endregion

                    return retSuccess;


                }
                else
                {
                    string response = "MSG :" + entityName + " doesn't exist in metadata please check that the entity name is spelt correctly.";
                    Response ret = Response.AsError(HttpStatusCode.NotFound, response);

                    timer.Stop();
                    TimeSpan timespan = timer.Elapsed;
                    string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                    string ms = timespan.TotalMilliseconds.ToString();
                    string min = timespan.TotalMinutes.ToString();
                    //Console.WriteLine("Elapsed Time in Seconds: " + timeElapsedSeconds);
                    //Console.WriteLine("Elapsed Time in Milliseconds: " + ms);
                    //Console.WriteLine("Elapsed Time in Minutes: " + min);
                    string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | ENTITY: " + entityName + " | STATUS_CODE: " + ret.StatusCode.ToString() + " | " + response;
                    log.Info(perfMsg);

                    return ret;
                }

            };

            #endregion

            #region GET PAGINATION
            /* This route is responsible for returning results with the sql top clause
             * FEATURES: 
             * TOP CLAUSE - allows to limit the number of results returned by a query, there is no restrictions in the number other than the number of records in teh database
             * ORDER BY - allows to order the results by query in the same fashion as the 'GETS WITH SORT'
             * SAMPLE URL = /limit/company/comp_name, comp_companyid/comp_companyid=1/10/115204172633907
             */

            Get["/limit/{entity}/{fields}/{where}/{start}/{end}/{order}/{sid}"] = parameters =>
            {
                //[PERF-BENCH]
                Stopwatch timer = Stopwatch.StartNew();
                this.EnableCors();
                // cleanup the params as it comes in
                #region variables

                var entityName = ((string)parameters.entity).Trim().ToLower();
                var fields = ((string)parameters.fields).Trim();
                var where = ((string)parameters.where).Trim();
                var start = ((string)parameters.start).Trim();
                var end = ((string)parameters.end).Trim();
                var order = ((string)parameters.order).Trim();
                var sid = ((string)parameters.sid).Trim();
                string orderClause = SortHandler(order);
                string selectClause, whereClause, sql;
                selectClause = FilterQuery(fields);
                whereClause = FilterQuery(where);

                string whereClause2 = "";

                whereClause2 = whereClause.Replace("=", ">="); //essentially the start of the page

                #endregion

                List<Metadata> metadata;
                if (GlobalVariables.Metadata.TryGetValue(entityName, out metadata)) //entityName is the key in the dictionary, so when searching values in the metadata list, need to firstly get the entityName THEN search for the values
                {
                    List<object> entityResults = new List<object>();

                    List<Dictionary<string, PayloadTemplate>> payloadList = new List<Dictionary<string, PayloadTemplate>>();
                    UserMetadata userMetadata = new UserMetadata();

                    using (var con = connectionFactory.GetOpenConnection())
                    {
                        #region GETTING USERIDS FROM SID

                        List<Activity> userData; //= new List<Activity>();
                        //List<object> sidIDList = new List<object>();
                        List<object> userDetails = new List<object>();


                        string sqlSID = "SELECT Acty_UserID FROM Activity where acty_logout is null and acty_logoutMethod is null and acty_SID = '" + sid + "'";
                        // Query that gets the user Id from the Activity table
                        //userMetadata.userSID = sid;

                        try
                        {
                            int count = 0;
                        RetryLogin:
                            GlobalVariables.Activity.TryGetValue(sid, out userData); //userData will always have one value in it. It can never have the same SID twice
                            //bool sidValid = false;

                            if (userData != null)
                            {
                                #region Process SID

                                foreach (var userDetail in userData)
                                {
                                    if (userDetail.sid.TrimEnd() == sid) //initial check of the SID - this should be where you should check and refresh..
                                    {
                                        if (userDetail.id != null)
                                        {
                                            userMetadata.userFirstName = userDetail.userFirstName;
                                            userMetadata.userLastName = userDetail.userLastName;
                                            userMetadata.userSID = userDetail.sid;
                                            userMetadata.userRequest = this.Request.Method.ToString();
                                            userMetadata.userId = userDetail.id;
                                            userMetadata.userFullName = userDetail.userFullName;
                                            userMetadata.userLoginTime = userDetail.loginTime;
                                            userMetadata.userLogon = userDetail.userLogon;
                                            userMetadata.userDept = userDetail.userDepartment;
                                        }
                                        else
                                        {
                                            var response = "Invalid User ID! Please Ensure ID is not null. ";
                                            return Response.AsError(HttpStatusCode.Unauthorized, response);
                                        }

                                    }
                                    else
                                    {
                                        var response = "Invalid SID! Please ensure that the user is still in session. ";
                                        return Response.AsError(HttpStatusCode.Unauthorized, response);
                                    }
                                }

                                #endregion

                            }
                            else if (count == 0) //1 attempt max - for testing 
                            {
                                count++;
                                GlobalVariables.GetUserMetadata();
                                goto RetryLogin;
                            }
                            else
                            {
                                var response = "Could not retrieve user Id! Please ensure that the user is logged in";
                                return Response.AsError(HttpStatusCode.ExpectationFailed, response);
                            }
                        }
                        catch
                        {
                            var response = "Could not retrieve user Id! Please ensure there is a connection to the database";
                            return Response.AsError(HttpStatusCode.ExpectationFailed, response);
                        }



                        #endregion

                        var metadataFields = FindMetadataFields(metadata);
                        
                        string fieldSelect = selectClause.Replace("SELECT", "");

                        sql = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (" + orderClause + ") as RowNum," + fieldSelect + " FROM " + entityName + " WHERE " + whereClause + " ) AS RowConstrainedResult WHERE RowNum >= " + start + " AND RowNum <= " + end + " ORDER BY RowNum";

                        try
                        {
                            entityResults = con.Query(sql).ToList();
                        }
                        catch (Exception ex)
                        {
                            string response = "MSG: " + ex.Message;
                            Response ret = Response.AsError(HttpStatusCode.NotFound, response);

                            timer.Stop();
                            TimeSpan timespan = timer.Elapsed;
                            string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                            string ms = timespan.TotalMilliseconds.ToString();
                            string min = timespan.TotalMinutes.ToString();
                            string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | ENTITY: " + entityName + " | STATUS_CODE: " + ret.StatusCode.ToString() + " | " + response;
                            log.Error(perfMsg);
                        }


                        if (entityResults.Count != 0)
                        {
                            try
                            {
                                payloadList = FormatPayload(metadata, entityResults);
                            }
                            catch(Exception ex)
                            {
                                string response = "MSG: " + ex.Message;
                                string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | ENTITY: " + entityName + " | STATUS_CODE: " + HttpStatusCode.BadRequest.ToString() +" | " + response;
                                log.Error(perfMsg);
                                return Response.AsError(HttpStatusCode.BadRequest, response);
                            }
                            
                        }

                    }

                    //make sure to add this part to all of the routes
                    //var payload = new { userMetadata = userMetadata, _url = Request.Url, payload = payloadList, items = entityResults, _metadata = metadata };
                    var payload = new { userMetadata = userMetadata, _url = Request.Url, payload = payloadList };
                    Response retSuccess = Response.AsJson(payload);

                    //[PERF-BENCH]
                    #region PERF-BENCH

                    timer.Stop();
                    TimeSpan timespanSuccess = timer.Elapsed;
                    string timeElapsedSecondsSuccess = timespanSuccess.TotalSeconds.ToString();
                    string msSuccess = timespanSuccess.TotalMilliseconds.ToString();
                    string minSuccess = timespanSuccess.TotalMinutes.ToString();
                    //Console.WriteLine("Elapsed Time in Seconds: " + timeElapsedSeconds);
                    //Console.WriteLine("Elapsed Time in Milliseconds: " + ms);
                    //Console.WriteLine("Elapsed Time in Minutes: " + min);
                    string perfMsgSuccess = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSecondsSuccess + " | TOTAL_RECORDS: " + payloadList.Count() + " | USER: " + userMetadata.userFullName + " | USERID: " + userMetadata.userId + " | ENTITY: " + entityName + " | STATUS_CODE: " + retSuccess.StatusCode.ToString();
                    log.Info(perfMsgSuccess);

                    //INSERT INTO table _tAFLogs

                    //AFLog logRecord = new AFLog();

                    //logRecord.CreatedDate = DateTime.Now.ToString();
                    //logRecord.fullName = userMetadata.userFullName;
                    //logRecord.HostName = Request.Url.HostName;
                    //logRecord.Path = Request.Url.Path;
                    //logRecord.Request = userMetadata.userRequest;
                    //logRecord.TotalRecs = payloadList.Count();
                    //logRecord.userId = userMetadata.userId;

                    //string sqlLog = "INSERT INTO _tAFLogs (Path, Request, TotalRecords, UserId, UserfullName, Hostname, CreatedDate, LoadTime) VALUES ('" + Request.Url.Path + "', '" + userMetadata.userRequest + "', '" + payloadList.Count() + "', '" + userMetadata.userId + "', '" + userMetadata.userFullName + "', '" + Request.Url.HostName + "', '" + DateTime.Now.ToString() + "', '" + timespan.TotalSeconds + "')";
                    //con.Execute(sqlLog);


                    #endregion

                    return retSuccess;


                }
                else
                {
                    string response = "MSG :" + entityName + " doesn't exist in metadata please check that the entity name is spelt correctly.";
                    Response ret = Response.AsError(HttpStatusCode.NotFound, response);

                    timer.Stop();
                    TimeSpan timespan = timer.Elapsed;
                    string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                    string ms = timespan.TotalMilliseconds.ToString();
                    string min = timespan.TotalMinutes.ToString();
                    //Console.WriteLine("Elapsed Time in Seconds: " + timeElapsedSeconds);
                    //Console.WriteLine("Elapsed Time in Milliseconds: " + ms);
                    //Console.WriteLine("Elapsed Time in Minutes: " + min);
                    string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | ENTITY: " + entityName + " | STATUS_CODE: " + ret.StatusCode.ToString() + " | " + response;
                    log.Info(perfMsg);

                    return ret;
                }

            };




            #endregion

            #endregion

            #region UPDATES V2

            Post["/update/{entity}/{where}"] = parameters =>
            {
                //[PERF-BENCH]
                Stopwatch timer = Stopwatch.StartNew();

                this.EnableCors();

                #region UPDATE V2 vars
                var asd = Request.Session["key"];

                var entityName = ((string)parameters.entity).Trim().ToLower();
                var where = ((string)parameters.where).Trim();
                //string sqlId = "";
                //string id = "";
                string sid = "";
                string userId = "";

                DynamicModelBinder dm = new DynamicModelBinder();
                NancyContext context = this.Context;
                List<ViewSchema> valueClasstype = new List<ViewSchema>();

                #region USING JSON LIBRARY

                string testData = dm.ProcessJson2(context);
                Dictionary<string, string> uiVals = new Dictionary<string, string>();
                try
                {
                    uiVals = JsonConvert.DeserializeObject<Dictionary<string, string>>(testData);
                }
                catch (Exception ex)
                {
                    var response = "Error reading payload: " + ex.Message + "See frameworkLog.txt for details";
                    log.Error(response + System.Environment.NewLine + "STACK TRACE: " + ex.StackTrace.ToString());
                    return Response.AsError(HttpStatusCode.ExpectationFailed, response);
                }
                

                foreach (var item in uiVals)
                {
                    if (item.Key.ToLower() == "sid")
                    {
                        sid = item.Value;
                    }
                }

                valueClasstype = ProcessPayload(uiVals);



                #endregion

                string whereReturn = FilterQuery(where);
                //string columns = "";
                //string values = "";

                //vars for dealing with views
                List<object> dudList = new List<object>(); //un processed tables are kept here
                HashSet<string> tablesToInsert = new HashSet<string>(); //this will contain the tables that needs to be updated

                List<Metadata> metadata;
                List<Metadata> metadataUpdate = new List<Metadata>();
                List<ViewSchema> viewSchema;

                string setQuery = "";
                string whereClause = "";
                string headQuery = "";
                string sql = "";
                string returnSql = "";

                string[] whereItems = where.Split('=');
                string[] whereRes = whereItems[0].Split('_'); //contains the field in the where statement



                //generic errors
                string error = "";


                #endregion

                if (GlobalVariables.Metadata.TryGetValue(entityName, out metadata))
                {
                    List<Dictionary<string, PayloadTemplate>> payloadList = new List<Dictionary<string, PayloadTemplate>>();
                    UserMetadata userMetadata = new UserMetadata();

                    using (var con = connectionFactory.GetOpenConnection())
                    {
                        var metadataFields = FindMetadataFields(metadata);

                        #region GETTING USERIDS FROM SID
                        sid = sid.Replace("'", "");
                        List<Activity> userData; //= new List<Activity>();
                        //List<object> sidIDList = new List<object>();
                        List<object> userDetails = new List<object>();


                        string sqlSID = "SELECT Acty_UserID FROM Activity where acty_logout is null and acty_logoutMethod is null and acty_SID = '" + sid + "'";
                        // Query that gets the user Id from the Activity table
                        //userMetadata.userSID = sid;

                        try
                        {
                            int count = 0;
                            RetryLogin:
                            GlobalVariables.Activity.TryGetValue(sid, out userData); //userData will always have one value in it. It can never have the same SID twice
                            //bool sidValid = false;

                            if (userData != null)
                            {
                                #region Process SID

                                foreach (var userDetail in userData)
                                {
                                    if (userDetail.sid.TrimEnd() == sid) //initial check of the SID - this should be where you should check and refresh..
                                    {
                                        if (userDetail.id != null)
                                        {
                                            userMetadata.userFirstName = userDetail.userFirstName;
                                            userMetadata.userLastName = userDetail.userLastName;
                                            userMetadata.userSID = userDetail.sid;
                                            userMetadata.userRequest = this.Request.Method.ToString();
                                            userMetadata.userId = userDetail.id;
                                            userMetadata.userFullName = userDetail.userFullName;
                                            userMetadata.userLoginTime = userDetail.loginTime;
                                            userMetadata.userLogon = userDetail.userLogon;
                                            userMetadata.userDept = userDetail.userDepartment;
                                        }
                                        else
                                        {
                                            var response = "Invalid User ID! Please Ensure ID is not null. ";
                                            return Response.AsError(HttpStatusCode.Unauthorized, response);
                                        }

                                    }
                                    else
                                    {
                                        var response = "Invalid SID! Please ensure that the user is still in session. ";
                                        return Response.AsError(HttpStatusCode.Unauthorized, response);
                                    }
                                }

                                #endregion

                            }
                            else if (count == 0) //1 attempt max - for testing 
                            {
                                count++;
                                GlobalVariables.GetUserMetadata();
                                goto RetryLogin;
                            }
                            else
                            {
                                var response = "Could not retrieve user Id! Please ensure that the user is logged in";
                                return Response.AsError(HttpStatusCode.ExpectationFailed, response);
                            }

                        }
                        catch
                        {
                            var response = "Could not retrieve user Id! Please ensure there is a connection to the database";
                            return Response.AsError(HttpStatusCode.ExpectationFailed, response);
                        }

                        userId = userMetadata.userId;

                        #endregion

                        #region HANDLER FOR VIEW UPDATES

                        if ((entityName.StartsWith("v")) || (entityName.StartsWith("_v")))
                        {
                            //find in viewSchema
                            GlobalVariables.ViewSchema.TryGetValue(entityName, out viewSchema);
                            HashSet<string> originTables = new HashSet<string>();
                            foreach (var schemaTableName in viewSchema)
                            {
                                string tableName = schemaTableName.tableName;
                                originTables.Add(tableName); //ORIGN tables - check these ones if they are a view or not
                            }

                            foreach (string table in originTables) //queries will be executed in this loop
                            {
                                #region MORE THAN 1 LEVEL DEEP

                                string tableUpdate = "";
                                string tableTypeLink = "";
                                if ((table.StartsWith("v")) || (table.StartsWith("_v"))) //more than one level deep, queries will be executed in the IF statement
                                {
                                    List<ViewSchema> trueOrigin = DrillDown(table);
                                    List<ViewSchema> testList = new List<ViewSchema>();
                                    List<string> testList2 = new List<string>();
                                    HashSet<string> tabNames = new HashSet<string>();
                                    List<ViewSchema> fieldsToUpdate = new List<ViewSchema>();

                                    foreach (var item in valueClasstype)
                                    {
                                        string colN = item.columnName.ToLower();
                                        string val = item.value;
                                        tableUpdate = item.tableName;
                                        var metaValue = trueOrigin.Find(a => a.columnName.ToLower().Contains(colN));

                                        if (metaValue != null)
                                        {
                                            metaValue.value = val;
                                            fieldsToUpdate.Add(metaValue); //has all the fields and the data to update
                                        }
                                    }

                                    foreach (var item in trueOrigin)
                                    {
                                        var tName = item.tableName;
                                        tabNames.Add(tName); //has all the tables that needs to be updates
                                    }

                                    foreach (var t in tabNames)
                                    {
                                        tableTypeLink = t;
                                        GlobalVariables.Metadata.TryGetValue(t.ToLower(), out metadataUpdate);
                                        var tableMetadata = FindMetadataFields(metadataUpdate);
                                        headQuery = "UPDATE " + t + " SET ";

                                        foreach (var field in fieldsToUpdate)
                                        {
                                            string tablename = field.tableName;

                                            if (tablename.ToLower() == t.ToLower())
                                            {
                                                //setQuery = setQuery + field.columnName + " = '" + field.value + "', ";

                                                string fieldValue = field.columnName;
                                                string valueValue = field.value;

                                                if (valueValue == "@NULL@")
                                                {
                                                    setQuery = setQuery + field.columnName + " = NULL, ";
                                                }
                                                else if(valueValue.Contains("'"))
                                                {
                                                    valueValue = valueValue.Replace("'", "''");
                                                    setQuery = setQuery + field.columnName + " = '" + valueValue + "', ";
                                                }
                                                else
                                                {
                                                    setQuery = setQuery + field.columnName + " = '" + field.value + "', ";
                                                }
                                            }
                                        }

                                        //where query

                                        //if (setQuery != "")
                                        //{
                                            #region WHERE CLAUSE

                                            string tb = tableTypeLink.ToLower();

                                            if (tb == "address")
                                            {
                                                #region DEALING WITH ADDRESSES

                                                string addressId;
                                                List<object> addrResults = new List<object>();
                                                string addrQuery = "SELECT Addr_addressid FROM " + entityName + " WHERE " + whereItems[0] + " = '" + whereItems[1] + "'";
                                                addrResults = con.Query(addrQuery).ToList();

                                                if (addrResults.Count > 1)
                                                {
                                                    error = "There is too many Address IDs attached to the record of the view - please review the record. ";
                                                    return Response.AsError(HttpStatusCode.NotFound, error);
                                                }

                                                addressId = GetFieldValFromQuery(addrResults); //gets the processed addressId

                                                var whereCol = metadataUpdate.Find(x => x.columnname.ToLower().Contains("_addressid")); //this shouold always be addr_addressid
                                                //var whereCol = metadataUpdate.Find(x => x.isprimarykey == true);
                                                whereClause = " WHERE " + whereCol.columnname + " = " + addressId;


                                                #endregion
                                            }

                                            if (tb == "phone")
                                            {
                                                #region DEALING WITH PHONE

                                                string addressId;
                                                List<object> addrResults = new List<object>();
                                                string addrQuery = "SELECT phon_phoneid FROM " + entityName + " WHERE " + whereItems[0] + " = '" + whereItems[1] + "'";
                                                addrResults = con.Query(addrQuery).ToList();

                                                if (addrResults.Count > 1)
                                                {
                                                    error = "There is too many phone IDs attached to the record of the view - please review the record. ";
                                                    return Response.AsError(HttpStatusCode.NotFound, error);
                                                }

                                                addressId = GetFieldValFromQuery(addrResults); //gets the processed addressId

                                                var whereCol = metadataUpdate.Find(x => x.columnname.ToLower().Contains("_phoneid")); //this shouold always be addr_addressid
                                                //var whereCol = metadataUpdate.Find(x => x.isprimarykey == true);
                                                whereClause = " WHERE " + whereCol.columnname + " = " + addressId;


                                                #endregion
                                            }

                                            if (tb == "email")
                                            {
                                                #region DEALING WITH EMAIL

                                                string addressId;
                                                List<object> addrResults = new List<object>();
                                                string addrQuery = "SELECT emai_emailid FROM " + entityName + " WHERE " + whereItems[0] + " = '" + whereItems[1] + "'";
                                                addrResults = con.Query(addrQuery).ToList();

                                                if (addrResults.Count > 1)
                                                {
                                                    error = "There is too many email IDs attached to the record of the view - please review the record. ";
                                                    return Response.AsError(HttpStatusCode.NotFound, error);
                                                }

                                                addressId = GetFieldValFromQuery(addrResults); //gets the processed addressId

                                                var whereCol = metadataUpdate.Find(x => x.columnname.ToLower().Contains("_emailid")); //this shouold always be addr_addressid
                                                //var whereCol = metadataUpdate.Find(x => x.isprimarykey == true);
                                                whereClause = " WHERE " + whereCol.columnname + " = " + addressId;


                                                #endregion
                                            }

                                            if ((tb != "email") && (tb != "address") && (tb != "phone"))
                                            {
                                                #region ANY OTHER TABLE

                                                string footer = whereRes[1]; //building on the above it turns pers_personid turns to personid
                                                List<object> whereList = new List<object>();

                                                string sqlWhere = "_spAFWhereClause " + footer; //calls db to figure out what the where clause will be.
                                                whereList = con.Query(sqlWhere).ToList();

                                                string viewWhere;
                                                foreach (var i in whereList)
                                                {
                                                    string whereTableName = i.ToString(); //need to fix this so that we can get the actual field for the where clause
                                                    viewWhere = GetWhereTable(whereTableName);

                                                    //wherelist is the list of fields that could be the where clause
                                                    //if (whereTableName == uniqueTables)
                                                    if (viewWhere.ToLower() == t.ToLower())
                                                    {
                                                        //whereClause = " WHERE " + pKey + " = " + whereItems[1];
                                                        var whereCol = metadataUpdate.Find(x => x.columnname.ToLower().Contains(footer.ToLower()));
                                                        if (whereItems[1] == "NULL")
                                                        {
                                                            whereClause = " WHERE " + whereCol.columnname + " is " + whereItems[1];
                                                        }
                                                        if (whereItems[1] == "!NULL")
                                                        {
                                                            whereClause = " WHERE " + whereCol.columnname + " is not " + whereItems[1];
                                                        }
                                                        else
                                                        {
                                                            whereClause = " WHERE " + whereCol.columnname + " = " + whereItems[1];
                                                        }
                                                        //whereClause = " WHERE " + whereCol.columnname + " = " + whereItems[1];
                                                    }

                                                }


                                                #endregion //region - ANY OTHER TABLE
                                            }

                                            #endregion
                                        //}

                                        if (error == "")
                                        {
                                            try
                                            {
                                                //if (setQuery != "")
                                                //{
                                                    //[AF-117-DONE]

                                                    if((tableMetadata.updatedby == null) || (tableMetadata.updateddate == null))
                                                    {
                                                        int indx = setQuery.LastIndexOf(',');
                                                        setQuery = setQuery.Remove(indx, 1);
                                                        sql = headQuery + setQuery + whereClause;
                                                    }
                                                    else
                                                    {
                                                        sql = headQuery + setQuery + tableMetadata.updatedby + " = " + userId + ", " + tableMetadata.updateddate + " = @updatedate" + whereClause;
                                                    }
                                                    
                                                    //sql = headQuery + setQuery + tableMetadata.updatedby + " = " + userId + ", " + tableMetadata.updateddate + " = @updatedate" + whereClause;
                                                    con.Execute(sql, new { updatedate = DateTime.Now });
                                                    headQuery = "";
                                                    setQuery = "";
                                                    whereClause = "";
                                                //}
                                            }
                                            catch (Exception ex)
                                            {
                                                string response = "MSG: " + ex.Message;
                                                Response ret = Response.AsError(HttpStatusCode.ExpectationFailed, response);

                                                timer.Stop();
                                                TimeSpan timespan = timer.Elapsed;
                                                string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                                                string ms = timespan.TotalMilliseconds.ToString();
                                                string min = timespan.TotalMinutes.ToString();
                                                //Console.WriteLine("Elapsed Time in Seconds: " + timeElapsedSeconds);
                                                //Console.WriteLine("Elapsed Time in Milliseconds: " + ms);
                                                //Console.WriteLine("Elapsed Time in Minutes: " + min);
                                                string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | ENTITY: " + entityName + " | STATUS_CODE: " + ret.StatusCode.ToString() + " | " + response;
                                                log.Error(perfMsg);

                                                return ret;
                                            }
                                        }

                                    }

                                }

                                #endregion

                                #region ONE LEVEL DEEP

                                else //one level deep
                                {

                                    headQuery = "UPDATE " + table + " SET ";

                                    //set query
                                    List<ViewSchema> tableSchema;
                                    List<Metadata> metadataQuery; //used for the query

                                    GlobalVariables.Metadata.TryGetValue(table.ToLower(), out metadataQuery); //this can't deal with aliases
                                    GlobalVariables.ViewSchema.TryGetValue(entityName.ToLower(), out tableSchema);

                                    var metadataFieldsLvl1 = FindMetadataFields(metadataQuery); //only used for the updatedBy and updatedDate fields

                                    //List<ViewSchema> fieldsToUpdate = new List<ViewSchema>();
                                    List<Metadata> fieldsToUpdate = new List<Metadata>();

                                    foreach (var item in valueClasstype)
                                    {
                                        string colN = item.columnName.ToLower();
                                        string val = item.value;
                                        tableUpdate = item.tableName;
                                        var metaValue = metadataQuery.Find(a => a.columnname.ToLower().Contains(colN));

                                        if (metaValue != null)
                                        {
                                            metaValue.value = val;
                                            fieldsToUpdate.Add(metaValue); //has all the fields and the data to update
                                        }
                                    }

                                    foreach (var field in fieldsToUpdate)
                                    {
                                        string tableName = field.objectname;

                                        if (tableName.ToLower() == table.ToLower())
                                        {
                                            //set query

                                            string value = field.value;
                                            string column = field.columnname;

                                            if (value == "@NULL@")
                                            {
                                                setQuery = setQuery + column + " = NULL, ";
                                            }
                                            else if (value.Contains("'"))
                                            {
                                                value = value.Replace("'", "''");
                                                setQuery = setQuery + column + " = '" + value + "', ";
                                            }
                                            else
                                            {
                                                setQuery = setQuery + column + " = '" + value + "', ";
                                            }
                                            
                                        }
                                    }

                                    //where query
                                    //if (setQuery != "")
                                    //{
                                        #region WHERE CLAUSE

                                        string tb = table.ToLower();

                                        if (tb == "address")
                                        {
                                            #region DEALING WITH ADDRESSES

                                            string addressId;
                                            List<object> addrResults = new List<object>();
                                            string addrQuery = "SELECT Addr_addressid FROM " + entityName + " WHERE " + whereItems[0] + " = '" + whereItems[1] + "'";
                                            addrResults = con.Query(addrQuery).ToList();

                                            if (addrResults.Count > 1)
                                            {
                                                error = "There is too many Address IDs attached to the record of the view - please review the record. ";
                                                return Response.AsError(HttpStatusCode.NotFound, error);
                                            }

                                            addressId = GetFieldValFromQuery(addrResults); //gets the processed addressId

                                            //var whereCol = tableSchema.Find(x => x.columnname.ToLower().Contains("_AddressId")); //this shouold always be addr_addressid
                                            var whereCol = metadataQuery.Find(x => x.isprimarykey == true);
                                            whereClause = " WHERE " + whereCol.columnname + " = " + addressId;


                                            #endregion
                                        }

                                        if (tb == "email")
                                        {
                                            #region DEALING WITH EMAIL

                                            string id;
                                            List<object> res = new List<object>();
                                            string addrQuery = "SELECT Emai_emailid FROM " + entityName + " WHERE " + whereItems[0] + " = '" + whereItems[1] + "'"; //query the view to find out the ID
                                            res = con.Query(addrQuery).ToList();

                                            if (res.Count > 1)
                                            {
                                                error = "There is to many email IDs attached to the record of the view - please review the record. ";
                                                return Response.AsError(HttpStatusCode.NotFound, error);
                                            }

                                            id = GetFieldValFromQuery(res); //gets the processed addressId

                                            //var whereCol = tableSchema.Find(x => x.columnname.ToLower().Contains("_AddressId")); //this shouold always be addr_addressid
                                            var whereCol = metadataQuery.Find(x => x.isprimarykey == true);
                                            whereClause = " WHERE " + whereCol.columnname + " = " + id;


                                            #endregion
                                        }

                                        if (tb == "phone")
                                        {
                                            #region DEALING WITH PHONE

                                            string id;
                                            List<object> res = new List<object>();
                                            string addrQuery = "SELECT phon_phoneid FROM " + entityName + " WHERE " + whereItems[0] + " = '" + whereItems[1] + "'"; //query the view to find out the ID
                                            res = con.Query(addrQuery).ToList();

                                            if (res.Count > 1)
                                            {
                                                error = "There is too many phone IDs attached to the record of the view - please review the record. ";
                                                return Response.AsError(HttpStatusCode.NotFound, error);
                                            }

                                            id = GetFieldValFromQuery(res); //gets the processed addressId

                                            //var whereCol = tableSchema.Find(x => x.columnname.ToLower().Contains("_AddressId")); //this shouold always be addr_addressid
                                            var whereCol = metadataQuery.Find(x => x.isprimarykey == true);
                                            whereClause = " WHERE " + whereCol.columnname + " = " + id;


                                            #endregion
                                        }

                                        if ((tb != "email") && (tb != "address") && (tb != "phone"))
                                        {
                                            #region ANY OTHER TABLE


                                            string footer = whereRes[1]; //building on the above it turns pers_personid turns to personid
                                            List<object> whereList = new List<object>(); //this is screwing the person table up...

                                            string sqlWhere = "_spAFWhereClause " + footer; //calls db to figure out what the where clause will be.
                                            whereList = con.Query(sqlWhere).ToList();
                                            foreach (var i in whereList)
                                            {
                                                string whereTableName = i.ToString(); //need to fix this so that we can get the actual field for the where clause
                                                string whereName = GetWhereTable(whereTableName);
                                                //wherelist is the list of fields that could be the where clause
                                                //if (whereTableName == uniqueTables)
                                                if (table.ToLower() == whereName.ToLower())
                                                {
                                                    var whereCol = metadataQuery.Find(x => x.columnname.ToLower().Contains(footer.ToLower()));
                                                    if (whereItems[1] == "NULL")
                                                    {
                                                        whereClause = " WHERE " + whereCol.columnname + " is " + whereItems[1];
                                                    }
                                                    else if (whereItems[1] == "!NULL")
                                                    {
                                                        whereClause = " WHERE " + whereCol.columnname + " is not " + whereItems[1];
                                                    }
                                                    else
                                                    {
                                                        whereClause = " WHERE " + whereCol.columnname + " = " + whereItems[1];
                                                    }
                                                    //whereClause = " WHERE " + whereCol.columnname + " = " + whereItems[1];
                                                }

                                            }

                                            #endregion //region - ANY OTHER TABLE
                                        }

                                        #endregion //region - WHERE  CLAUSE
                                    //}

                                    //sql formation
                                    //[AF-117-DONE]

                                    if ((metadataFieldsLvl1.updatedby == null) || (metadataFieldsLvl1.updateddate == null))
                                    {
                                        int indx = setQuery.LastIndexOf(',');
                                        setQuery = setQuery.Remove(indx, 1);
                                        sql = headQuery + setQuery + whereClause;
                                    }
                                    else
                                    {
                                        sql = headQuery + setQuery + metadataFieldsLvl1.updatedby + " = " + userId + ", " + metadataFieldsLvl1.updateddate + " = @updatedate " + whereClause;
                                    }

                                    //sql = headQuery + setQuery + metadataFieldsLvl1.updatedby + " = " + userId + ", " + metadataFieldsLvl1.updateddate + " = @updatedate " + whereClause;

                                    if (error == "")
                                    {
                                        try
                                        {
                                            if ((setQuery != "") || (whereClause != ""))
                                            {
                                                con.Execute(sql, new { updatedate = DateTime.Now });
                                                headQuery = "";
                                                setQuery = "";
                                                whereClause = "";
                                            }

                                        }
                                        catch (Exception ex)
                                        {
                                            string response = "MSG: " + ex.Message;
                                            Response ret = Response.AsError(HttpStatusCode.NotFound, response);

                                            timer.Stop();
                                            TimeSpan timespan = timer.Elapsed;
                                            string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                                            string ms = timespan.TotalMilliseconds.ToString();
                                            string min = timespan.TotalMinutes.ToString();
                                            //Console.WriteLine("Elapsed Time in Seconds: " + timeElapsedSeconds);
                                            //Console.WriteLine("Elapsed Time in Milliseconds: " + ms);
                                            //Console.WriteLine("Elapsed Time in Minutes: " + min);
                                            string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | ENTITY: " + entityName + " | STATUS_CODE: " + ret.StatusCode.ToString() + " | " + response;
                                            log.Error(perfMsg);

                                            return ret;
                                        }
                                    }
                                    else
                                    {
                                        return Response.AsJson(error); // this might kickout later
                                    }




                                }

                                #endregion
                            }
                        }

                        #endregion

                        #region HANDLER FOR DIRECT TABLE UPDATES

                        else //this assumes that if its a table update that all the fields can be concatenated together in the one setquery
                        {
                            //sort out the fields first

                            setQuery = ""; ;

                            foreach (var val in valueClasstype)
                            {
                                string field = val.columnName;
                                string value = val.value;

                                // added 9-25-2014 becuase someone decided to enter in 'NULL' as a string value not as an actuall NULL value
                                //SET @NULL@ to actual null values
                                //SET NULL to string values
                                if (value == "@NULL@")
                                {
                                    setQuery = setQuery + field + " = NULL, ";
                                }
                                else if(value.Contains("'"))
                                {
                                    value = value.Replace("'", "''");
                                    setQuery = setQuery + field + " = '" + value + "', ";
                                }
                                else
                                {
                                    setQuery = setQuery + field + " = '" + value + "', ";
                                }
                            }
                            //then the where clause

                            //whereClause = " WHERE " + whereItems[0] + " = '" + whereItems[1] + "'";
                            whereClause = " WHERE " + whereReturn; //uses where return because its to a direct table

                            //then put it all together with the UPDATE + entityName with metadata

                            headQuery = "UPDATE " + entityName + " SET ";

                            //[AF-117-DONE]
                            
                            if((metadataFields.updateddate == null) || (metadataFields.updatedby == null))
                            {
                                int indx = setQuery.LastIndexOf(',');
                                setQuery = setQuery.Remove(indx, 1);
                                sql = headQuery + setQuery + whereClause;
                            }
                            else
                            {
                                sql = headQuery + setQuery + metadataFields.updatedby + " = " + userId + ", " + metadataFields.updateddate + " = @updatedate" + whereClause; //need to add the metadata updates with this string
                            }

                            //sql = headQuery + setQuery + metadataFields.updatedby + " = " + userId + ", " + metadataFields.updateddate + " = @updatedate" + whereClause; //need to add the metadata updates with this string

                            returnSql = "SELECT * FROM " + entityName + whereClause;

                            //execute the query
                            try
                            {
                                if (setQuery != "")
                                {
                                    con.Execute(sql, new { updatedate = DateTime.Now });
                                    headQuery = "";
                                    setQuery = "";
                                    whereClause = "";
                                }
                            }

                            catch (Exception ex)
                            {
                                string response = "MSG: " + ex.Message;
                                Response ret = Response.AsError(HttpStatusCode.NotFound, response);

                                timer.Stop();
                                TimeSpan timespan = timer.Elapsed;
                                string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                                string ms = timespan.TotalMilliseconds.ToString();
                                string min = timespan.TotalMinutes.ToString();
                                string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | ENTITY: " + entityName + " | STATUS_CODE: " + ret.StatusCode.ToString() + " | " + response;
                                log.Error(perfMsg);

                                return ret;
                            }

                        }

                        #endregion

                        #region RETURN SQL

                        //return query here
                        returnSql = "SELECT * FROM " + entityName + " WHERE " + whereReturn;

                        List<object> entityResults = con.Query(returnSql).ToList();

                        payloadList = FormatPayload(metadata, entityResults);

                        try
                        {
                            //var payload = new { userMetadata = userMetadata, _url = Request.Url, payload = payloadList, items = entityResults, _metadata = metadata };

                            var payload = new { userMetadata = userMetadata, _url = Request.Url, payload = payloadList };
                            Response ret = Response.AsJson(payload);

                            //[PERF-BENCH]
                            #region PERF-BENCH

                            timer.Stop();
                            TimeSpan timespan = timer.Elapsed;
                            string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                            string ms = timespan.TotalMilliseconds.ToString();
                            string min = timespan.TotalMinutes.ToString();
                            //Console.WriteLine("Elapsed Time in Seconds: " + timeElapsedSeconds);
                            //Console.WriteLine("Elapsed Time in Milliseconds: " + ms);
                            //Console.WriteLine("Elapsed Time in Minutes: " + min);
                            string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | TOTAL_RECORDS: " + payloadList.Count() + " | USER: " + userMetadata.userFullName + " | USERID: " + userMetadata.userId + " | ENTITY: " + entityName + " | STATUS_CODE: " + ret.StatusCode.ToString();
                            log.Info(perfMsg);

                            //INSERT INTO table _tAFSearchLogs

                            //AFLog logRecord = new AFLog();

                            //logRecord.CreatedDate = DateTime.Now.ToString();
                            //logRecord.fullName = userMetadata.userFullName;
                            //logRecord.HostName = Request.Url.HostName;
                            //logRecord.Path = Request.Url.Path;
                            //logRecord.Request = userMetadata.userRequest;
                            //logRecord.TotalRecs = payloadList.Count();
                            //logRecord.userId = userMetadata.userId;

                            //string sqlLog = "INSERT INTO _tAFLogs (Path, Request, TotalRecords, UserId, UserfullName, Hostname, CreatedDate, LoadTime) VALUES ('" + Request.Url.Path + "', '" + userMetadata.userRequest + "', '" + payloadList.Count() + "', '" + userMetadata.userId + "', '" + userMetadata.userFullName + "', '" + Request.Url.HostName + "', '" + DateTime.Now.ToString() + "', '" + timespan.TotalSeconds + "')";
                            //con.Execute(sqlLog);


                            #endregion

                            return ret;
                        }
                        catch (Exception ex)
                        {
                            string response = "MSG: " + ex.Message;
                            Response ret = Response.AsError(HttpStatusCode.NotFound, response);

                            timer.Stop();
                            TimeSpan timespan = timer.Elapsed;
                            string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                            string ms = timespan.TotalMilliseconds.ToString();
                            string min = timespan.TotalMinutes.ToString();
                            //Console.WriteLine("Elapsed Time in Seconds: " + timeElapsedSeconds);
                            //Console.WriteLine("Elapsed Time in Milliseconds: " + ms);
                            //Console.WriteLine("Elapsed Time in Minutes: " + min);
                            string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | ENTITY: " + entityName + " | STATUS_CODE: " + ret.StatusCode.ToString() + " | " + response;
                            log.Error(perfMsg);

                            return ret;
                        }

                        #endregion

                    }
                }
                else
                {
                    string response = "MSG :" + entityName + " doesn't exist in metadata please check that the entity name is spelt correctly.";
                    Response ret = Response.AsError(HttpStatusCode.NotFound, response);

                    timer.Stop();
                    TimeSpan timespan = timer.Elapsed;
                    string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                    string ms = timespan.TotalMilliseconds.ToString();
                    string min = timespan.TotalMinutes.ToString();
                    //Console.WriteLine("Elapsed Time in Seconds: " + timeElapsedSeconds);
                    //Console.WriteLine("Elapsed Time in Milliseconds: " + ms);
                    //Console.WriteLine("Elapsed Time in Minutes: " + min);
                    string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | ENTITY: " + entityName + " | STATUS_CODE: " + ret.StatusCode.ToString() + " | " + response;
                    log.Info(perfMsg);

                    return ret;
                }

            };


            #endregion

            #region DELETES
            //[START]
            Delete["/delete/{entity}/{where}"] = parameters =>
            {
                //[PERF-BENCH]
                Stopwatch timer = Stopwatch.StartNew();

                this.EnableCors();

                #region Delete Vars

                string sid = "";
                string userId = "";

                DynamicModelBinder dm = new DynamicModelBinder();
                NancyContext context = this.Context;

                string uiData = dm.ProcessJson2(context);

                Dictionary<string, string> uiVals = new Dictionary<string, string>();

                try
                {
                    uiVals = JsonConvert.DeserializeObject<Dictionary<string, string>>(uiData);
                }
                catch(Exception ex)
                {
                    var response = "Error reading payload: " + ex.Message + "See frameworkLog.txt for details";
                    log.Error(response + System.Environment.NewLine + "STACK TRACE: " + ex.StackTrace.ToString());
                    return Response.AsError(HttpStatusCode.ExpectationFailed, response);
                }
                
                foreach(var item in uiVals)
                {
                    if (item.Key.ToLower() == "sid")
                    {
                        sid = item.Value;
                    }
                }
                
                
                //List<string> payloadUI = dm.ProcessJson(context);
                UserMetadata userMetadata = new UserMetadata();

                //sid = GetSID(payloadUI); //Contains the SID from the payload

                // cleanup the params as it comes in
                var entityName = ((string)parameters.entity).Trim().ToLower();
                var where = ((string)parameters.where);
                string[] wherParts = where.Split('_');

                string header = wherParts[0];

                #endregion

                string whereClause = FilterQuery(where);
                List<Metadata> metadata;
                if (GlobalVariables.Metadata.TryGetValue(entityName, out metadata))
                {
                    using (var con = connectionFactory.GetOpenConnection())
                    {
                        #region GETTING USERIDS FROM SID
                        //sid = sid.Replace("'", "");
                        List<Activity> userData; //= new List<Activity>();
                        //List<object> sidIDList = new List<object>();
                        List<object> userDetails = new List<object>();


                        string sqlSID = "SELECT Acty_UserID FROM Activity where acty_logout is null and acty_logoutMethod is null and acty_SID = '" + sid + "'";
                        // Query that gets the user Id from the Activity table
                        //userMetadata.userSID = sid;

                        try
                        {
                            int count = 0;
                        RetryLogin:
                            GlobalVariables.Activity.TryGetValue(sid, out userData); //userData will always have one value in it. It can never have the same SID twice
                            //bool sidValid = false;

                            if (userData != null)
                            {
                                #region Process SID

                                foreach (var userDetail in userData)
                                {
                                    if (userDetail.sid.TrimEnd() == sid) //initial check of the SID - this should be where you should check and refresh..
                                    {
                                        if (userDetail.id != null)
                                        {
                                            userMetadata.userFirstName = userDetail.userFirstName;
                                            userMetadata.userLastName = userDetail.userLastName;
                                            userMetadata.userSID = userDetail.sid;
                                            userMetadata.userRequest = this.Request.Method.ToString();
                                            userMetadata.userId = userDetail.id;
                                            userMetadata.userFullName = userDetail.userFullName;
                                            userMetadata.userLoginTime = userDetail.loginTime;
                                            userMetadata.userLogon = userDetail.userLogon;
                                            userMetadata.userDept = userDetail.userDepartment;
                                        }
                                        else
                                        {
                                            var response = "Invalid User ID! Please Ensure ID is not null. ";
                                            return Response.AsError(HttpStatusCode.Unauthorized, response);
                                        }

                                    }
                                    else
                                    {
                                        var response = "Invalid SID! Please ensure that the user is still in session. ";
                                        return Response.AsError(HttpStatusCode.Unauthorized, response);
                                    }
                                }

                                #endregion

                            }
                            else if (count == 0) //1 attempt max - for testing 
                            {
                                count++;
                                GlobalVariables.GetUserMetadata();
                                goto RetryLogin;
                            }
                            else
                            {
                                var response = "Could not retrieve user Id! Please ensure that the user is logged in";
                                return Response.AsError(HttpStatusCode.ExpectationFailed, response);
                            }

                        }
                        catch
                        {
                            var response = "Could not retrieve user Id! Please ensure there is a connection to the database";
                            return Response.AsError(HttpStatusCode.ExpectationFailed, response);
                        }

                        userId = userMetadata.userId;

                        #endregion

                        var metadataFields = FindMetadataFields(metadata);
                        string sql;

                        #region REBUILT DELETES

                        //check if the record has been deleted or not

                        string checkDelSQL = "SELECT * FROM " + entityName + " WHERE " + whereClause + "and " + header + "_deleted is null";

                        List<object> checkResults = con.Query(checkDelSQL).ToList();

                        List<Dictionary<string, PayloadTemplate>> payloadList = new List<Dictionary<string, PayloadTemplate>>();
                        if (checkResults.Count != 0) //if there is something to delete
                        {
                            //[AF-117]
                            if (entityName.StartsWith("v") || (entityName.StartsWith("_v")))
                            {
                                sql = "UPDATE " + entityName + " SET " + header + "_deleted = 1, " + header + "_updatedby = " + userId + ", " + header + "_updateddate = @updatedate WHERE " + whereClause;
                            }
                            else
                            {
                                sql = "UPDATE " + entityName + " SET " + metadataFields.deleted + " = 1, " + metadataFields.updatedby + " = " + userId + ", " + metadataFields.updateddate + " = @updatedate WHERE " + whereClause;
                            }

                            var testVar = con.Execute(sql, new { updatedate = DateTime.Now }); //update the record

                            //construct returnSQL

                            string returnSQL = "SELECT * FROM " + entityName + " WHERE " + whereClause;

                            #region RETURN SQL

                            //List<Dictionary<string, PayloadTemplate>> payloadList = new List<Dictionary<string, PayloadTemplate>>();

                            //string returnSql = "SELECT * FROM " + entityName + " WHERE " + whereClause;

                            List<object> entityResults = con.Query(returnSQL).ToList();

                            payloadList = FormatPayload(metadata, entityResults);

                            try
                            {
                                //var payload = new { userMetadata = userMetadata, _url = Request.Url, payload = payloadList, items = entityResults, _metadata = metadata };

                                var payload = new { userMetadata = userMetadata, _url = Request.Url, payload = payloadList };
                                Response ret = Response.AsJson(payload);

                                //[PERF-BENCH]
                                #region PERF-BENCH

                                timer.Stop();
                                TimeSpan timespan = timer.Elapsed;
                                string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                                string ms = timespan.TotalMilliseconds.ToString();
                                string min = timespan.TotalMinutes.ToString();
                                //Console.WriteLine("Elapsed Time in Seconds: " + timeElapsedSeconds);
                                //Console.WriteLine("Elapsed Time in Milliseconds: " + ms);
                                //Console.WriteLine("Elapsed Time in Minutes: " + min);
                                string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | TOTAL_RECORDS: " + payloadList.Count() + " | USER: " + userMetadata.userFullName + " | USERID: " + userMetadata.userId + " | ENTITY: " + entityName + " | STATUS_CODE: " + ret.StatusCode.ToString();
                                log.Info(perfMsg);

                                //INSERT INTO table _tAFSearchLogs

                                //AFLog logRecord = new AFLog();

                                //logRecord.CreatedDate = DateTime.Now.ToString();
                                //logRecord.fullName = userMetadata.userFullName;
                                //logRecord.HostName = Request.Url.HostName;
                                //logRecord.Path = Request.Url.Path;
                                //logRecord.Request = userMetadata.userRequest;
                                //logRecord.TotalRecs = payloadList.Count();
                                //logRecord.userId = userMetadata.userId;

                                //string sqlLog = "INSERT INTO _tAFLogs (Path, Request, TotalRecords, UserId, UserfullName, Hostname, CreatedDate, LoadTime) VALUES ('" + Request.Url.Path + "', '" + userMetadata.userRequest + "', '" + payloadList.Count() + "', '" + userMetadata.userId + "', '" + userMetadata.userFullName + "', '" + Request.Url.HostName + "', '" + DateTime.Now.ToString() + "', '" + timespan.TotalSeconds + "')";
                                //con.Execute(sqlLog);


                                #endregion

                                return ret;
                            }
                            catch (Exception ex)
                            {
                                return Response.AsError(HttpStatusCode.NotFound, ex.Message.ToString());
                            }

                            #endregion
                        }
                        else //otherwise prep for empty payload
                        {
                            try
                            {
                                //var payload = new { userMetadata = userMetadata, _url = Request.Url, payload = payloadList, items = entityResults, _metadata = metadata };

                                var payload = new { userMetadata = userMetadata, _url = Request.Url, payload = payloadList }; //payloadList SHOULD ALWAYS BE EMPTY HERE
                                Response ret = Response.AsJson(payload);

                                //[PERF-BENCH]
                                #region PERF-BENCH

                                timer.Stop();
                                TimeSpan timespan = timer.Elapsed;
                                string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                                string ms = timespan.TotalMilliseconds.ToString();
                                string min = timespan.TotalMinutes.ToString();
                                //Console.WriteLine("Elapsed Time in Seconds: " + timeElapsedSeconds);
                                //Console.WriteLine("Elapsed Time in Milliseconds: " + ms);
                                //Console.WriteLine("Elapsed Time in Minutes: " + min);
                                string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | TOTAL_RECORDS: " + payloadList.Count() + " | USER: " + userMetadata.userFullName + " | USERID: " + userMetadata.userId + " | ENTITY: " + entityName + " | STATUS_CODE: " + ret.StatusCode.ToString();
                                log.Info(perfMsg);

                                //INSERT INTO table _tAFSearchLogs

                                //AFLog logRecord = new AFLog();

                                //logRecord.CreatedDate = DateTime.Now.ToString();
                                //logRecord.fullName = userMetadata.userFullName;
                                //logRecord.HostName = Request.Url.HostName;
                                //logRecord.Path = Request.Url.Path;
                                //logRecord.Request = userMetadata.userRequest;
                                //logRecord.TotalRecs = payloadList.Count();
                                //logRecord.userId = userMetadata.userId;

                                //string sqlLog = "INSERT INTO _tAFLogs (Path, Request, TotalRecords, UserId, UserfullName, Hostname, CreatedDate, LoadTime) VALUES ('" + Request.Url.Path + "', '" + userMetadata.userRequest + "', '" + payloadList.Count() + "', '" + userMetadata.userId + "', '" + userMetadata.userFullName + "', '" + Request.Url.HostName + "', '" + DateTime.Now.ToString() + "', '" + timespan.TotalSeconds + "')";
                                //con.Execute(sqlLog);


                                #endregion

                                return ret;
                            }
                            catch (Exception ex)
                            {
                                return Response.AsError(HttpStatusCode.NotFound, ex.Message.ToString());
                            }
                        }

                        #endregion

                        //return HttpStatusCode.OK;
                    }
                }
                else
                {
                    string response = "MSG :" + entityName + " doesn't exist in metadata please check that the entity name is spelt correctly.";
                    Response ret = Response.AsError(HttpStatusCode.NotFound, response);

                    timer.Stop();
                    TimeSpan timespan = timer.Elapsed;
                    string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                    string ms = timespan.TotalMilliseconds.ToString();
                    string min = timespan.TotalMinutes.ToString();
                    //Console.WriteLine("Elapsed Time in Seconds: " + timeElapsedSeconds);
                    //Console.WriteLine("Elapsed Time in Milliseconds: " + ms);
                    //Console.WriteLine("Elapsed Time in Minutes: " + min);
                    string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | ENTITY: " + entityName + " | STATUS_CODE: " + ret.StatusCode.ToString() + " | " + response;
                    log.Info(perfMsg);

                    return ret;
                }
                
            };

            #endregion

            #region CREATE V2

            Post["/new/{entity}"] = parameters =>
            {
                //[PERF-BENCH]
                Stopwatch timer = Stopwatch.StartNew();

                this.EnableCors();

                #region CREATES VARIABLES

                var entityName = ((string)parameters.entity).Trim().ToLower();

                string sqlId = "";
                string id = "";
                string sid = "";
                string userId = "";
                string insertSql = "";
                string returnSql = "";

                string headQuery, setQuery;
                string columns = "";
                string vals = "";
                DynamicModelBinder dm = new DynamicModelBinder();
                NancyContext context = this.Context;

                #region USING JSON LIBRARY
                
                string testData = dm.ProcessJson2(context);
                List<ViewSchema> payloadProcessed = new List<ViewSchema>();
                Dictionary<string, string> uiVals = new Dictionary<string, string>();
                try
                {
                    uiVals = JsonConvert.DeserializeObject<Dictionary<string, string>>(testData);
                }
                catch(Exception ex)
                {
                    var response = "Error reading payload: " + ex.Message + "See frameworkLog.txt for details";
                    log.Error(response + System.Environment.NewLine + "STACK TRACE: " + ex.StackTrace.ToString());
                    return Response.AsError(HttpStatusCode.ExpectationFailed, response);
                }
                
                
                foreach (var item in uiVals)
                {
                    if(item.Key.ToLower() == "sid")
                    {
                        sid = item.Value;
                    }                    
                }
                
                payloadProcessed = ProcessPayload(uiVals);



                //List<string> payloadUI = dm.ProcessJson(context);
                //List<ViewSchema> payloadProcessed = GetTableValues2(payloadUI);
                //sid = GetSID(payloadUI); //Contains the SID from the payload
                #endregion

                List<Metadata> metadata;

                List<string> fields = new List<string>();
                List<string> values = new List<string>();

                

                //_spINFCreateRecord vard
                string crPrimaryKey = "";

                #endregion

                if (GlobalVariables.Metadata.TryGetValue(entityName, out metadata))
                {

                    List<Dictionary<string, PayloadTemplate>> payloadList = new List<Dictionary<string, PayloadTemplate>>();
                    UserMetadata userMetadata = new UserMetadata();

                    string chk = GlobalVariables.CheckCreateVersion();

                    using (var con = connectionFactory.GetOpenConnection())
                    {
                        #region GETTING USERIDS FROM SID
                        //sid = sid.Replace("'", "");
                        List<Activity> userData; //= new List<Activity>();
                        //List<object> sidIDList = new List<object>();
                        List<object> userDetails = new List<object>();


                        string sqlSID = "SELECT Acty_UserID FROM Activity where acty_logout is null and acty_logoutMethod is null and acty_SID = '" + sid + "'";
                        // Query that gets the user Id from the Activity table
                        //userMetadata.userSID = sid;

                        try
                        {
                            int count = 0;
                        RetryLogin:
                            GlobalVariables.Activity.TryGetValue(sid, out userData); //userData will always have one value in it. It can never have the same SID twice
                            //bool sidValid = false;

                            if (userData != null)
                            {
                                #region Process SID

                                foreach (var userDetail in userData)
                                {
                                    if (userDetail.sid.TrimEnd() == sid) //initial check of the SID - this should be where you should check and refresh..
                                    {
                                        if (userDetail.id != null)
                                        {
                                            userMetadata.userFirstName = userDetail.userFirstName;
                                            userMetadata.userLastName = userDetail.userLastName;
                                            userMetadata.userSID = userDetail.sid;
                                            userMetadata.userRequest = this.Request.Method.ToString();
                                            userMetadata.userId = userDetail.id;
                                            userMetadata.userFullName = userDetail.userFullName;
                                            userMetadata.userLoginTime = userDetail.loginTime;
                                            userMetadata.userLogon = userDetail.userLogon;
                                            userMetadata.userDept = userDetail.userDepartment;
                                        }
                                        else
                                        {
                                            var response = "Invalid User ID! Please Ensure ID is not null. ";
                                            return Response.AsError(HttpStatusCode.Unauthorized, response);
                                        }

                                    }
                                    else
                                    {
                                        var response = "Invalid SID! Please ensure that the user is still in session. ";
                                        return Response.AsError(HttpStatusCode.Unauthorized, response);
                                    }
                                }

                                #endregion

                            }
                            else if (count == 0) //1 attempt max - for testing 
                            {
                                count++;
                                GlobalVariables.GetUserMetadata();
                                goto RetryLogin;
                            }
                            else
                            {
                                var response = "Could not retrieve user Id! Please ensure that the user is logged in";
                                return Response.AsError(HttpStatusCode.ExpectationFailed, response);
                            }

                        }
                        catch
                        {
                            var response = "Could not retrieve user Id! Please ensure there is a connection to the database";
                            return Response.AsError(HttpStatusCode.ExpectationFailed, response);
                        }

                        userId = userMetadata.userId;

                        #endregion

                        #region CREATE ON VIEWS

                        if ((entityName.StartsWith("v")) || (entityName.StartsWith("_v")))
                        {
                            string error = "CREATING RECORDS IN VIEWS IS NOT ALLOWED!!";
                            return Response.AsError(HttpStatusCode.BadRequest, error);
                        }

                        #endregion

                        #region CREATE ON TABLES
                        else
                        {
                            if ((chk == "SP") && (entityName.StartsWith("_taf"))) //used to allow custom table inserts on upgraded 7.2 systems
                            {
                                #region 7.2 and later

                                List<object> entityResults = new List<object>();
                                string crDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                foreach (var item in payloadProcessed) /* payload processed is from the UI and was put in the ViewSchema Class */
                                {
                                    string field = item.columnName;
                                    string value = item.value;
                                    columns = columns + field + ", "; //concatenates the fields to be inserted in the table

                                    if (value == "@NULL@")
                                    {
                                        value = "NULL";
                                        vals += value + ", "; //concatenates teh values to be insterted in the table
                                    }
                                    else if (value.Contains("'"))
                                    {
                                        value = value.Replace("'", "''");
                                        vals += "'" + value + "', "; //concatenates teh values to be insterted in the table
                                    }
                                    else
                                    {
                                        vals += "'" + value + "', "; //concatenates teh values to be insterted in the table
                                    }
                                }

                                //[AF-117-DONE]
                                //construct head query
                                var metadataFields = FindMetadataFields(metadata);
                                headQuery = "INSERT INTO " + entityName + " (" + columns + metadataFields.createddate + ", " + metadataFields.createdby + ", " + metadataFields.updateddate + ", " + metadataFields.updatedby + ")";

                                //construct set query
                                string dateTime = DateTime.Now.ToString();

                                if ((metadataFields.createddate == null) || (metadataFields.createdby == null) || (metadataFields.updateddate == null) || (metadataFields.updatedby == null))
                                {
                                    int colindx = columns.LastIndexOf(',');
                                    int valindx = vals.LastIndexOf(',');

                                    columns = columns.Remove(colindx, 1);
                                    vals = vals.Remove(valindx, 1);

                                    headQuery = "INSERT INTO " + entityName + " (" + columns + ")";
                                    setQuery = " VALUES (" + vals + ")";
                                }
                                else
                                {
                                    setQuery = " VALUES (" + vals + "'" + crDate + "'," + userId + ", " + "'" + crDate + "'," + userId + ")";
                                }


                                string newId = ""; //variable to put in the new ID generated
                                string primaryKey = "";

                                #region CHANNEL_LINK PRIMARY KEY HACK [HACK:PK-CHANLINK]

                                /* This got brought up because, of the change in the metadata to identify the primary keys better
                                 */

                                if (entityName == "channel_link")
                                {
                                    crPrimaryKey = "OUTPUT Inserted.chli_channel_id";
                                    primaryKey = "chli_channel_id";
                                }
                                else
                                {
                                    crPrimaryKey = "OUTPUT Inserted." + metadataFields.primarykey;
                                    primaryKey = metadataFields.primarykey;
                                }

                                #endregion


                                string keyTableSQL = "DECLARE @keyTable table (primaryKey varchar(30)) ";
                                string intoVarTableSQL = " into @keyTable ";
                                string returnKeySQL = " select * from @keyTable ";


                                //construct insert SQL [START]
                                insertSql = keyTableSQL + headQuery + crPrimaryKey + intoVarTableSQL + setQuery + returnKeySQL;

                                try
                                {
                                    entityResults = con.Query(insertSql).ToList(); //this will contain the primary key
                                    //need to format entityResults so that I can get my select statement ready
                                    //afterwards format the payload
                                    newId = GetIds(entityResults);

                                    returnSql = "SELECT * FROM " + entityName + " WHERE " + primaryKey + " = " + newId;
                                    List<object> recordResults = con.Query(returnSql).ToList(); //return query

                                    payloadList = FormatPayload(metadata, recordResults);
                                }
                                catch (Exception ex)
                                {
                                    string response1 = "MSG: " + ex.Message;
                                    Response ret = Response.AsError(HttpStatusCode.NotFound, response1);

                                    timer.Stop();
                                    TimeSpan timespan = timer.Elapsed;
                                    string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                                    string ms = timespan.TotalMilliseconds.ToString();
                                    string min = timespan.TotalMinutes.ToString();
                                    //Console.WriteLine("Elapsed Time in Seconds: " + timeElapsedSeconds);
                                    //Console.WriteLine("Elapsed Time in Milliseconds: " + ms);
                                    //Console.WriteLine("Elapsed Time in Minutes: " + min);
                                    string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | ENTITY: " + entityName + " | STATUS_CODE: " + ret.StatusCode.ToString() + " | " + response1;
                                    log.Error(perfMsg);

                                    return ret;
                                }

                                //var response = new { _url = Request.Url, items = entityResults, _translationValues = translationObj2, _metadata = metadata }; //if the record has been marked deleted it will return 0 results
                                var response = new { userMetadata = userMetadata, _url = Request.Url, payload = payloadList };
                                Response retSuccess = Response.AsJson(response);

                                //[PERF-BENCH]
                                #region PERF-BENCH

                                timer.Stop();
                                TimeSpan timespanSuccess = timer.Elapsed;
                                string timeElapsedSecondsSuccess = timespanSuccess.TotalSeconds.ToString();
                                string msSuccess = timespanSuccess.TotalMilliseconds.ToString();
                                string minSuccess = timespanSuccess.TotalMinutes.ToString();
                                //Console.WriteLine("Elapsed Time in Seconds: " + timeElapsedSeconds);
                                //Console.WriteLine("Elapsed Time in Milliseconds: " + ms);
                                //Console.WriteLine("Elapsed Time in Minutes: " + min);
                                string perfMsgSuccess = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSecondsSuccess + " | TOTAL_RECORDS: " + payloadList.Count() + " | USER: " + userMetadata.userFullName + " | USERID: " + userMetadata.userId + " | ENTITY: " + entityName + " | STATUS_CODE: " + retSuccess.StatusCode.ToString();
                                log.Info(perfMsgSuccess);

                                //INSERT INTO table _tAFLogs

                                //AFLog logRecord = new AFLog();

                                //logRecord.CreatedDate = DateTime.Now.ToString();
                                //logRecord.fullName = userMetadata.userFullName;
                                //logRecord.HostName = Request.Url.HostName;
                                //logRecord.Path = Request.Url.Path;
                                //logRecord.Request = userMetadata.userRequest;
                                //logRecord.TotalRecs = payloadList.Count();
                                //logRecord.userId = userMetadata.userId;

                                //string sqlLog = "INSERT INTO _tAFLogs (Path, Request, TotalRecords, UserId, UserfullName, Hostname, CreatedDate, LoadTime) VALUES ('" + Request.Url.Path + "', '" + userMetadata.userRequest + "', '" + payloadList.Count() + "', '" + userMetadata.userId + "', '" + userMetadata.userFullName + "', '" + Request.Url.HostName + "', '" + DateTime.Now.ToString() + "', '" + timespan.TotalSeconds + "')";
                                //con.Execute(sqlLog);


                                #endregion

                                return retSuccess;

                                #endregion
                            }
                            else if (chk == "SP") //from an upgrade
                            {
                                #region 7.2 upgrage and earlier

                                #region CREATE PRIMARY KEY

                                List<object> idList = new List<object>();
                                try
                                {
                                    sqlId = "_spAFgetNewId '" + entityName + "'"; //call sp to get id

                                    idList = con.Query(sqlId).ToList();

                                    id = GetIds(idList); //id to be used when inserting a new record
                                }
                                catch(Exception ex)
                                {
                                    return Response.AsError(HttpStatusCode.ExpectationFailed, ex.Message.ToString());
                                }


                                #endregion

                                //sort out the columns and the fields

                                List<object> entityResults = new List<object>();

                                foreach (var item in payloadProcessed) /* payload processed is from the UI and was put in the ViewSchema Class */
                                {
                                    string field = item.columnName;
                                    string value = item.value;

                                    if (value == "@NULL@")
                                    {
                                        value = "NULL";
                                        vals += value + ", "; //concatenates teh values to be insterted in the table
                                    }
                                    else if (value.Contains("'"))
                                    {
                                        value = value.Replace("'", "''");
                                        vals += "'" + value + "', "; //concatenates teh values to be insterted in the table
                                    }
                                    else
                                    {
                                        vals += "'" + value + "', "; //concatenates teh values to be insterted in the table
                                    }


                                    columns = columns + field + ", "; //concatenates the fields to be inserted in the table
                                    //vals = vals + "'" + value + "', "; //concatenates teh values to be insterted in the table
                                }

                                //NEED TO TAKE INTO ACCOUNT CRM EMAIL PHONE DATA HAVING ITS SHITTY STRUCTURE THAT TRIES TO BE 3RD NORMAL FORM BUT IS MAKING LIFE HELL FOR ME.


                                //construct head query
                                var metadataFields = FindMetadataFields(metadata);

                                //[AF-117-DONE]
                                #region [HACK:PK-CHANLINK] shitty db architecture with no fuckng consistency
                                
                                if (entityName == "channel_link")
                                {
                                    headQuery = "INSERT INTO " + entityName + " (" + columns + " ChLi_ChannelLinkId " + ", " + metadataFields.createddate + ", " + metadataFields.createdby + ", " + metadataFields.updateddate + ", " + metadataFields.updatedby + ")";
                                    returnSql = "SELECT * FROM " + entityName + " WHERE ChLi_ChannelLinkId =" + id;
                                }
                                else
                                {
                                    headQuery = "INSERT INTO " + entityName + " (" + columns + metadataFields.primarykey + ", " + metadataFields.createddate + ", " + metadataFields.createdby + ", " + metadataFields.updateddate + ", " + metadataFields.updatedby + ")";
                                    returnSql = "SELECT * FROM " + entityName + " WHERE " + metadataFields.primarykey + " = " + id;
                                }
                                #endregion

                                //headQuery = "INSERT INTO " + entityName + " (" + columns + metadataFields.primarykey + ", " + metadataFields.createddate + ", " + metadataFields.createdby + ", " + metadataFields.updateddate + ", " + metadataFields.updatedby + ")";

                                //construct set query

                                if ((metadataFields.createddate == null) || (metadataFields.createdby == null) || (metadataFields.updateddate == null) || (metadataFields.updatedby == null))
                                {
                                    headQuery = "INSERT INTO " + entityName + " (" + columns + metadataFields.primarykey + ")";
                                    setQuery = " VALUES (" + vals + id + ", " + " @createdDate, " + userId + ", " + " @updateDate, " + userId + ")";
                                }
                                else
                                {
                                    setQuery = " VALUES (" + vals + id + ", " + " @createdDate, " + userId + ", " + " @updateDate, " + userId + ")";
                                }
                                

                                //construct insert SQL
                                insertSql = headQuery + setQuery;
                                //returnSql = "SELECT * FROM " + entityName + " WHERE " + metadataFields.primarykey + " = " + id; //moved to hack for channel_link


                                //var payload = new { userMetadata = userMetadata, _url = Request.Url, payload = payloadList };

                                try
                                {
                                    con.Execute(insertSql, new { createdDate = DateTime.Now, updateDate = DateTime.Now });
                                    entityResults = con.Query(returnSql).ToList();

                                    payloadList = FormatPayload(metadata, entityResults);

                                }
                                catch (Exception ex)
                                {
                                    string response1 = "MSG: " + ex.Message;
                                    Response ret = Response.AsError(HttpStatusCode.NotFound, response1);

                                    timer.Stop();
                                    TimeSpan timespan = timer.Elapsed;
                                    string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                                    string ms = timespan.TotalMilliseconds.ToString();
                                    string min = timespan.TotalMinutes.ToString();
                                    string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | ENTITY: " + entityName + " | STATUS_CODE: " + ret.StatusCode.ToString() + " | " + response1;
                                    log.Error(perfMsg);

                                    return ret;
                                }


                                //var response = new { _url = Request.Url, items = entityResults, _translationValues = translationObj2, _metadata = metadata }; //if the record has been marked deleted it will return 0 results
                                var response = new { userMetadata = userMetadata, _url = Request.Url, payload = payloadList };
                                Response retSuccess = Response.AsJson(response);

                                //[PERF-BENCH]
                                #region PERF-BENCH

                                timer.Stop();
                                TimeSpan timespanSuccess = timer.Elapsed;
                                string timeElapsedSecondsSuccess = timespanSuccess.TotalSeconds.ToString();
                                string msSuccess = timespanSuccess.TotalMilliseconds.ToString();
                                string minSuccess = timespanSuccess.TotalMinutes.ToString();
                                //Console.WriteLine("Elapsed Time in Seconds: " + timeElapsedSeconds);
                                //Console.WriteLine("Elapsed Time in Milliseconds: " + ms);
                                //Console.WriteLine("Elapsed Time in Minutes: " + min);
                                string perfMsgSuccess = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSecondsSuccess + " | TOTAL_RECORDS: " + payloadList.Count() + " | USER: " + userMetadata.userFullName + " | USERID: " + userMetadata.userId + " | ENTITY: " + entityName + " | STATUS_CODE: " + retSuccess.StatusCode.ToString();
                                log.Info(perfMsgSuccess);

                                //INSERT INTO table _tAFLogs

                                //AFLog logRecord = new AFLog();

                                //logRecord.CreatedDate = DateTime.Now.ToString();
                                //logRecord.fullName = userMetadata.userFullName;
                                //logRecord.HostName = Request.Url.HostName;
                                //logRecord.Path = Request.Url.Path;
                                //logRecord.Request = userMetadata.userRequest;
                                //logRecord.TotalRecs = payloadList.Count();
                                //logRecord.userId = userMetadata.userId;

                                //string sqlLog = "INSERT INTO _tAFLogs (Path, Request, TotalRecords, UserId, UserfullName, Hostname, CreatedDate, LoadTime) VALUES ('" + Request.Url.Path + "', '" + userMetadata.userRequest + "', '" + payloadList.Count() + "', '" + userMetadata.userId + "', '" + userMetadata.userFullName + "', '" + Request.Url.HostName + "', '" + DateTime.Now.ToString() + "', '" + timespan.TotalSeconds + "')";
                                //con.Execute(sqlLog);


                                #endregion

                                return retSuccess;


                                #endregion
                            }
                            else if (chk == "PK") //vanilla install of 7.2
                            {
                                #region 7.2 and later

                                List<object> entityResults = new List<object>();
                                string crDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                foreach (var item in payloadProcessed) /* payload processed is from the UI and was put in the ViewSchema Class */
                                {
                                    string field = item.columnName;
                                    string value = item.value;
                                    columns = columns + field + ", "; //concatenates the fields to be inserted in the table

                                    if(value == "@NULL@")
                                    {
                                        value = "NULL";
                                        vals += value + ", "; //concatenates teh values to be insterted in the table
                                    }
                                    else if (value.Contains("'"))
                                    {
                                        value = value.Replace("'", "''");
                                        vals += "'" + value + "', "; //concatenates teh values to be insterted in the table
                                    }
                                    else
                                    {
                                        vals += "'" + value + "', "; //concatenates teh values to be insterted in the table
                                    }


                                }

                                //[AF-117-DONE]
                                //construct head query
                                var metadataFields = FindMetadataFields(metadata);
                                headQuery = "INSERT INTO " + entityName + " (" + columns + metadataFields.createddate + ", " + metadataFields.createdby + ", " + metadataFields.updateddate + ", " + metadataFields.updatedby + ")";

                                //construct set query
                                string dateTime = DateTime.Now.ToString();

                                if ((metadataFields.createddate == null) || (metadataFields.createdby == null) || (metadataFields.updateddate == null) || (metadataFields.updatedby == null))
                                {
                                    int colindx = columns.LastIndexOf(',');
                                    int valindx = vals.LastIndexOf(',');

                                    columns = columns.Remove(colindx, 1);
                                    vals = vals.Remove(valindx, 1);

                                    headQuery = "INSERT INTO " + entityName + " (" + columns + ")";
                                    setQuery = " VALUES (" + vals + ")";
                                }
                                else
                                {
                                    setQuery = " VALUES (" + vals + "'" + crDate + "'," + userId + ", " + "'" + crDate + "'," + userId + ")";
                                }

                                

                                string newId = ""; //variable to put in the new ID generated
                                string primaryKey = "";

                                #region CHANNEL_LINK PRIMARY KEY HACK [HACK:PK-CHANLINK]

                                /* This got brought up because, of the change in the metadata to identify the primary keys better
                                 */

                                if (entityName == "channel_link")
                                {
                                    crPrimaryKey = "OUTPUT Inserted.chli_channel_id";
                                    primaryKey = "chli_channel_id";
                                }
                                else
                                {
                                    crPrimaryKey = "OUTPUT Inserted." + metadataFields.primarykey;
                                    primaryKey = metadataFields.primarykey;
                                }

                                #endregion


                                string keyTableSQL = "DECLARE @keyTable table (primaryKey varchar(30)) ";
                                string intoVarTableSQL = " into @keyTable ";
                                string returnKeySQL = " select * from @keyTable ";


                                //construct insert SQL [START]
                                insertSql = keyTableSQL + headQuery + crPrimaryKey + intoVarTableSQL + setQuery + returnKeySQL;

                                try
                                {
                                    entityResults = con.Query(insertSql).ToList(); //this will contain the primary key
                                    //need to format entityResults so that I can get my select statement ready
                                    //afterwards format the payload
                                    newId = GetIds(entityResults);

                                    returnSql = "SELECT * FROM " + entityName + " WHERE " + primaryKey + " = " + newId;
                                    List<object> recordResults = con.Query(returnSql).ToList(); //return query

                                    payloadList = FormatPayload(metadata, recordResults);
                                }
                                catch (Exception ex)
                                {
                                    string response1 = "MSG: " + ex.Message;
                                    Response ret = Response.AsError(HttpStatusCode.NotFound, response1);

                                    timer.Stop();
                                    TimeSpan timespan = timer.Elapsed;
                                    string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                                    string ms = timespan.TotalMilliseconds.ToString();
                                    string min = timespan.TotalMinutes.ToString();
                                    //Console.WriteLine("Elapsed Time in Seconds: " + timeElapsedSeconds);
                                    //Console.WriteLine("Elapsed Time in Milliseconds: " + ms);
                                    //Console.WriteLine("Elapsed Time in Minutes: " + min);
                                    string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | ENTITY: " + entityName + " | STATUS_CODE: " + ret.StatusCode.ToString() + " | " + response1;
                                    log.Error(perfMsg);

                                    return ret;
                                }

                                //var response = new { _url = Request.Url, items = entityResults, _translationValues = translationObj2, _metadata = metadata }; //if the record has been marked deleted it will return 0 results
                                var response = new { userMetadata = userMetadata, _url = Request.Url, payload = payloadList };
                                Response retSuccess = Response.AsJson(response);

                                //[PERF-BENCH]
                                #region PERF-BENCH

                                timer.Stop();
                                TimeSpan timespanSuccess = timer.Elapsed;
                                string timeElapsedSecondsSuccess = timespanSuccess.TotalSeconds.ToString();
                                string msSuccess = timespanSuccess.TotalMilliseconds.ToString();
                                string minSuccess = timespanSuccess.TotalMinutes.ToString();
                                //Console.WriteLine("Elapsed Time in Seconds: " + timeElapsedSeconds);
                                //Console.WriteLine("Elapsed Time in Milliseconds: " + ms);
                                //Console.WriteLine("Elapsed Time in Minutes: " + min);
                                string perfMsgSuccess = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSecondsSuccess + " | TOTAL_RECORDS: " + payloadList.Count() + " | USER: " + userMetadata.userFullName + " | USERID: " + userMetadata.userId + " | ENTITY: " + entityName + " | STATUS_CODE: " + retSuccess.StatusCode.ToString();
                                log.Info(perfMsgSuccess);

                                //INSERT INTO table _tAFLogs

                                //AFLog logRecord = new AFLog();

                                //logRecord.CreatedDate = DateTime.Now.ToString();
                                //logRecord.fullName = userMetadata.userFullName;
                                //logRecord.HostName = Request.Url.HostName;
                                //logRecord.Path = Request.Url.Path;
                                //logRecord.Request = userMetadata.userRequest;
                                //logRecord.TotalRecs = payloadList.Count();
                                //logRecord.userId = userMetadata.userId;

                                //string sqlLog = "INSERT INTO _tAFLogs (Path, Request, TotalRecords, UserId, UserfullName, Hostname, CreatedDate, LoadTime) VALUES ('" + Request.Url.Path + "', '" + userMetadata.userRequest + "', '" + payloadList.Count() + "', '" + userMetadata.userId + "', '" + userMetadata.userFullName + "', '" + Request.Url.HostName + "', '" + DateTime.Now.ToString() + "', '" + timespan.TotalSeconds + "')";
                                //con.Execute(sqlLog);


                                #endregion

                                return retSuccess;

                                #endregion
                            }


                        }

                        #endregion
                    }
                }
                else
                {
                    string response = "MSG :" + entityName + " doesn't exist in metadata please check that the entity name is spelt correctly.";
                    Response ret = Response.AsError(HttpStatusCode.NotFound, response);

                    timer.Stop();
                    TimeSpan timespan = timer.Elapsed;
                    string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                    string ms = timespan.TotalMilliseconds.ToString();
                    string min = timespan.TotalMinutes.ToString();
                    //Console.WriteLine("Elapsed Time in Seconds: " + timeElapsedSeconds);
                    //Console.WriteLine("Elapsed Time in Milliseconds: " + ms);
                    //Console.WriteLine("Elapsed Time in Minutes: " + min);
                    string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | ENTITY: " + entityName + " | STATUS_CODE: " + ret.StatusCode.ToString() + " | " + response;
                    log.Info(perfMsg);

                    return ret;
                }


                return HttpStatusCode.NotFound;

            };

            #endregion

            #region STORED PROCS

            /* STORED PROC ROUTE NO SID REQUIRED */

            Post["/procedure/{storedProc}/"] = parameters =>
            {
                //[PERF-BENCH]
                Stopwatch timer = Stopwatch.StartNew();

                this.EnableCors();

                var storedProc = parameters.storedProc; //don't clean up as it will have spaces
                DynamicModelBinder dm = new DynamicModelBinder();
                NancyContext context = this.Context;

                string testData = dm.ProcessJson2(context);
                Dictionary<string, string> uiVals = new Dictionary<string, string>();

                try
                {
                    uiVals = JsonConvert.DeserializeObject<Dictionary<string, string>>(testData);
                }
                catch(Exception ex)
                {
                    string errMsg = "ERROR PROCESSING PAYLOAD - ERROR MSG: " + ex.Message + "See framewroklog.txt for details.";
                    log.Error(errMsg + "STACK: " + ex.StackTrace);
                    return Response.AsError(HttpStatusCode.NotFound, errMsg);

                }

                List<string> payload = dm.ProcessJson(context);

                List<string> vals = GetValues(payload); //this is the vars that the stored proc will need

                string sql = "";
                List<object> entityResults;

                try
                {
                    using (var con = connectionFactory.GetOpenConnection())
                    {
                        //foreach (var value in vals)
                        foreach (var values in uiVals)
                        {
                            string value = values.Value;
                            if (value.Contains("'"))
                            {
                                value = value.Replace("'", "''");
                            }
                            
                            sql += "'" + value + "', ";
                                                     
                        }

                        sql = storedProc + " " + sql;

                        if (sql.EndsWith(", "))
                        {
                            int remove = sql.LastIndexOf(',');

                            sql = sql.Remove(remove);
                        }
     

                        try
                        {
                            entityResults = con.Query(sql).ToList();                            
                        }
                        catch (Exception ex)
                        {
                            string response = "MSG: " + ex.Message;
                            Response ret = Response.AsError(HttpStatusCode.NotFound, response);

                            timer.Stop();
                            TimeSpan timespan = timer.Elapsed;
                            string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                            string ms = timespan.TotalMilliseconds.ToString();
                            string min = timespan.TotalMinutes.ToString();
                            string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | ENTITY: " + storedProc + " | STATUS_CODE: " + ret.StatusCode.ToString() + " | " + response;
                            log.Error(perfMsg);

                            return ret;
                        }

                        Response retSuccess;
                        
                        var responseSuccess = new { _url = Request.Url, payload = entityResults }; //if the record has been marked deleted it will return 0 results

                        retSuccess = Response.AsJson(responseSuccess);

                        //[PERF-BENCH]
                        #region PERF-BENCH

                        timer.Stop();
                        TimeSpan timespanSuccess = timer.Elapsed;
                        string timeElapsedSecondsSuccess = timespanSuccess.TotalSeconds.ToString();
                        string msSuccess = timespanSuccess.TotalMilliseconds.ToString();
                        string minSuccess = timespanSuccess.TotalMinutes.ToString();

                        string perfMsgSuccess = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSecondsSuccess + " | TOTAL_RECORDS: " + entityResults.Count() + " | USER: " + " | ENTITY: " + storedProc + " | STATUS_CODE: " + retSuccess.StatusCode.ToString();
                        log.Info(perfMsgSuccess);



                        #endregion

                        return retSuccess;

                    }
                }

                catch
                {
                    return HttpStatusCode.NotFound;
                }

            };

            /* GET generic Stored Procedure */

            Get["/procedure/{storedProc}/{inputs}"] = parameters =>
            {
                //[PERF-BENCH]
                Stopwatch timer = Stopwatch.StartNew();

                this.EnableCors();

                // cleanup the params as it comes in
                var spName = ((string)parameters.storedProc).Trim().ToLower();
                var inputParams = ((string)parameters.inputs).Trim();

                string inputsClause, sql;
                inputsClause = inputParams.Replace("&", ", ");  // Assuming that if you want to have ampersands in the input parameter, you will submit them as URL encoded (e.g. %26).
                // TODO: URL decode the input parameters

                try
                {
                    List<object> entityResults = new List<object>();

                    using (var con = connectionFactory.GetOpenConnection())
                    {
                        sql = " exec " + spName + " " + inputsClause;
                        try
                        {
                            entityResults = con.Query(sql).ToList();
                        }
                        catch (Exception ex)
                        {
                            string response = "MSG: " + ex.Message;
                            Response ret = Response.AsError(HttpStatusCode.NotFound, response);

                            timer.Stop();
                            TimeSpan timespan = timer.Elapsed;
                            string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                            string ms = timespan.TotalMilliseconds.ToString();
                            string min = timespan.TotalMinutes.ToString();
                            string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | ENTITY: " + spName + " | STATUS_CODE: " + ret.StatusCode.ToString() + " | " + response;
                            log.Error(perfMsg);

                            return ret;
                        }
                        
                    }

                    var payload = new { _url = Request.Url, payload = entityResults };

                    Response retSuccess;

                    var responseSuccess = new { _url = Request.Url, payload = entityResults }; //if the record has been marked deleted it will return 0 results

                    retSuccess = Response.AsJson(responseSuccess);

                    //[PERF-BENCH]
                    #region PERF-BENCH

                    timer.Stop();
                    TimeSpan timespanSuccess = timer.Elapsed;
                    string timeElapsedSecondsSuccess = timespanSuccess.TotalSeconds.ToString();
                    string msSuccess = timespanSuccess.TotalMilliseconds.ToString();
                    string minSuccess = timespanSuccess.TotalMinutes.ToString();

                    string perfMsgSuccess = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSecondsSuccess + " | TOTAL_RECORDS: " + entityResults.Count() + " | USER: " + " | ENTITY: " + spName + " | STATUS_CODE: " + retSuccess.StatusCode.ToString();
                    log.Info(perfMsgSuccess);



                    #endregion

                    return retSuccess;

                }
                catch (Exception ex)
                {
                    return Response.AsError(HttpStatusCode.NotFound, ex.ToString());
                }

            };

            #endregion

            #region POST-GET
            //use this to do a select statement but have funny characters like forward slashes in the parameters. also usefull for requests with a lot of data

            Post["/postget/{entity}"] = parameters =>
            {
                //[PERF-BENCH]
                Stopwatch timer = Stopwatch.StartNew();

                this.EnableCors();

                #region UPDATE V2 vars

                var entityName = ((string)parameters.entity).Trim().ToLower();
                //string sqlId = "";
                //string id = "";
                string sid = "";
                string userId = "";

                DynamicModelBinder dm = new DynamicModelBinder();
                NancyContext context = this.Context;
                List<ViewSchema> payloadObj = new List<ViewSchema>();

                #region USING JSON LIBRARY

                string testData = dm.ProcessJson2(context);
                Dictionary<string, string> uiVals = new Dictionary<string, string>();
                try
                {
                    uiVals = JsonConvert.DeserializeObject<Dictionary<string, string>>(testData);
                }
                catch (Exception ex)
                {
                    var response = "Error reading payload: " + ex.Message + "See frameworkLog.txt for details";
                    log.Error(response + System.Environment.NewLine + "STACK TRACE: " + ex.StackTrace.ToString());
                    return Response.AsError(HttpStatusCode.ExpectationFailed, response);
                }


                foreach (var item in uiVals)
                {
                    if (item.Key.ToLower() == "sid")
                    {
                        sid = item.Value;
                    }
                }

                #endregion
                payloadObj = ProcessPayload(uiVals); // will have the values from the json packet

                //string columns = "";
                //string values = "";

                //vars for dealing with views
                List<object> dudList = new List<object>(); //un processed tables are kept here
                HashSet<string> tablesToInsert = new HashSet<string>(); //this will contain the tables that needs to be updated

                List<Metadata> metadata;
                List<Metadata> metadataUpdate = new List<Metadata>();
                //List<ViewSchema> viewSchema;

                //string setQuery = "";
                //string whereClause = "";
                //string headQuery = "";
                string sql = "";
                //string returnSql = "";



                //generic errors
                //string error = "";


                #endregion

                if (GlobalVariables.Metadata.TryGetValue(entityName, out metadata))
                {
                    List<Dictionary<string, PayloadTemplate>> payloadList = new List<Dictionary<string, PayloadTemplate>>();
                    UserMetadata userMetadata = new UserMetadata();

                    using (var con = connectionFactory.GetOpenConnection())
                    {
                        var metadataFields = FindMetadataFields(metadata);

                        #region GETTING USERIDS FROM SID
                        sid = sid.Replace("'", "");
                        List<Activity> userData; //= new List<Activity>();
                        //List<object> sidIDList = new List<object>();
                        List<object> userDetails = new List<object>();


                        string sqlSID = "SELECT Acty_UserID FROM Activity where acty_logout is null and acty_logoutMethod is null and acty_SID = '" + sid + "'";
                        // Query that gets the user Id from the Activity table
                        //userMetadata.userSID = sid;

                        try
                        {
                            int count = 0;
                        RetryLogin:
                            GlobalVariables.Activity.TryGetValue(sid, out userData); //userData will always have one value in it. It can never have the same SID twice
                            //bool sidValid = false;

                            if (userData != null)
                            {
                                #region Process SID

                                foreach (var userDetail in userData)
                                {
                                    if (userDetail.sid.TrimEnd() == sid) //initial check of the SID - this should be where you should check and refresh..
                                    {
                                        if (userDetail.id != null)
                                        {
                                            userMetadata.userFirstName = userDetail.userFirstName;
                                            userMetadata.userLastName = userDetail.userLastName;
                                            userMetadata.userSID = userDetail.sid;
                                            userMetadata.userRequest = this.Request.Method.ToString();
                                            userMetadata.userId = userDetail.id;
                                            userMetadata.userFullName = userDetail.userFullName;
                                            userMetadata.userLoginTime = userDetail.loginTime;
                                            userMetadata.userLogon = userDetail.userLogon;
                                            userMetadata.userDept = userDetail.userDepartment;
                                        }
                                        else
                                        {
                                            var response = "Invalid User ID! Please Ensure ID is not null. ";
                                            return Response.AsError(HttpStatusCode.Unauthorized, response);
                                        }

                                    }
                                    else
                                    {
                                        var response = "Invalid SID! Please ensure that the user is still in session. ";
                                        return Response.AsError(HttpStatusCode.Unauthorized, response);
                                    }
                                }

                                #endregion

                            }
                            else if (count == 0) //1 attempt max - for testing 
                            {
                                count++;
                                GlobalVariables.GetUserMetadata();
                                goto RetryLogin;
                            }
                            else
                            {
                                var response = "Could not retrieve user Id! Please ensure that the user is logged in";
                                return Response.AsError(HttpStatusCode.ExpectationFailed, response);
                            }

                        }
                        catch
                        {
                            var response = "Could not retrieve user Id! Please ensure there is a connection to the database";
                            return Response.AsError(HttpStatusCode.ExpectationFailed, response);
                        }

                        userId = userMetadata.userId;

                        #endregion

                        sql = "SELECT * FROM " + entityName + " WHERE ";

                        //get the values from json packet to be put in the where clause

                        foreach(var val in payloadObj)
                        {
                            sql += val.columnName + " = '" + val.value + "' AND ";
                        }

                        //clear out the AND 
                        if(sql.EndsWith("AND "))
                        {
                            int indx = sql.LastIndexOf("AND");
                            sql = sql.Remove(indx);
                        }


                        //if (updateQuery.EndsWith("AND "))
                        //{
                        //    string remove = "AND";

                        //    result = updateQuery.Substring(0, updateQuery.LastIndexOf(remove));
                        //}


                        //if (line.EndsWith("}"))
                        //{
                        //    int indx = line.LastIndexOf("}");
                        //    line = line.Remove(indx);
                        //}


                        #region RETURN SQL

                        //return query here
                        //returnSql = "SELECT * FROM " + entityName + " WHERE " + whereReturn;

                        List<object> entityResults = con.Query(sql).ToList();

                        payloadList = FormatPayload(metadata, entityResults);

                        try
                        {
                            //var payload = new { userMetadata = userMetadata, _url = Request.Url, payload = payloadList, items = entityResults, _metadata = metadata };

                            var payload = new { userMetadata = userMetadata, _url = Request.Url, payload = payloadList };
                            Response ret = Response.AsJson(payload);

                            //[PERF-BENCH]
                            #region PERF-BENCH

                            timer.Stop();
                            TimeSpan timespan = timer.Elapsed;
                            string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                            string ms = timespan.TotalMilliseconds.ToString();
                            string min = timespan.TotalMinutes.ToString();
                            //Console.WriteLine("Elapsed Time in Seconds: " + timeElapsedSeconds);
                            //Console.WriteLine("Elapsed Time in Milliseconds: " + ms);
                            //Console.WriteLine("Elapsed Time in Minutes: " + min);
                            string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | TOTAL_RECORDS: " + payloadList.Count() + " | USER: " + userMetadata.userFullName + " | USERID: " + userMetadata.userId + " | ENTITY: " + entityName + " | STATUS_CODE: " + ret.StatusCode.ToString();
                            log.Info(perfMsg);




                            #endregion

                            return ret;
                        }
                        catch (Exception ex)
                        {
                            string response = "MSG: " + ex.Message;
                            Response ret = Response.AsError(HttpStatusCode.NotFound, response);

                            timer.Stop();
                            TimeSpan timespan = timer.Elapsed;
                            string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                            string ms = timespan.TotalMilliseconds.ToString();
                            string min = timespan.TotalMinutes.ToString();
                            string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | ENTITY: " + entityName + " | STATUS_CODE: " + ret.StatusCode.ToString() + " | " + response;
                            log.Error(perfMsg);

                            return ret;
                        }

                        #endregion

                    }
                }
                else
                {
                    string response = "MSG :" + entityName + " doesn't exist in metadata please check that the entity name is spelt correctly.";
                    Response ret = Response.AsError(HttpStatusCode.NotFound, response);

                    timer.Stop();
                    TimeSpan timespan = timer.Elapsed;
                    string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                    string ms = timespan.TotalMilliseconds.ToString();
                    string min = timespan.TotalMinutes.ToString();
                    //Console.WriteLine("Elapsed Time in Seconds: " + timeElapsedSeconds);
                    //Console.WriteLine("Elapsed Time in Milliseconds: " + ms);
                    //Console.WriteLine("Elapsed Time in Minutes: " + min);
                    string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | ENTITY: " + entityName + " | STATUS_CODE: " + ret.StatusCode.ToString() + " | " + response;
                    log.Info(perfMsg);

                    return ret;
                }

            };

            #endregion

            /* MAKE SURE TO UPDATE THE OPTIONS REGION EVERYTIME THERE IS A NEW POST ROUTE
             */

            #region OPTIONS

            Options["/metadata/refresh/{entity}"] = parameters =>
            {
                this.EnableCors();
                return HttpStatusCode.NoContent;
            };


            Options["/userMetadata"] = parameters =>
            {
                this.EnableCors();
                return HttpStatusCode.NoContent;
            };

            Options["procedure/{storedProc}"] = parameters =>
            {
                this.EnableCors();
                return HttpStatusCode.NoContent;
            };


            Options["/new/{entity}"] = parameters =>
            {
                this.EnableCors();
                return HttpStatusCode.NoContent;
            };

            Options["/delete/{entity}/{where}"] = parameters =>
            {
                this.EnableCors();
                return HttpStatusCode.NoContent;
            };

            Options["/update/{entity}/{where}"] = parameters =>
            {
                this.EnableCors();
                return HttpStatusCode.NoContent;
            };
            Options["/postget/{entity}"] = parameters =>
            {
                this.EnableCors();
                return HttpStatusCode.NoContent;
            };

            Options["updateviews"] = parameters =>
            {
                this.EnableCors();
                return HttpStatusCode.NoContent;
            };







            #endregion

            #region ADMIN FUNCTIONS

            #region REFRESH METADATA: ENTITY

            Post["/metadata/refresh/{entity}"] = parameters =>
            {
                this.EnableCors();
                UserMetadata userMetadata = new UserMetadata();

                string entityName = ((string)parameters.entity).Trim().ToLower();

                #region USING JSON LIBRARY
                string sid = "";
                DynamicModelBinder dm = new DynamicModelBinder();
                NancyContext context = this.Context;
                string testData = dm.ProcessJson2(context);
                List<ViewSchema> payloadProcessed = new List<ViewSchema>();
                Dictionary<string, string> uiVals = new Dictionary<string, string>();
                try
                {
                    uiVals = JsonConvert.DeserializeObject<Dictionary<string, string>>(testData);
                }
                catch (Exception ex)
                {
                    var response = "Error reading payload: " + ex.Message + "See frameworkLog.txt for details";
                    log.Error(response + System.Environment.NewLine + "STACK TRACE: " + ex.StackTrace.ToString());
                    return Response.AsError(HttpStatusCode.ExpectationFailed, response);
                }


                foreach (var item in uiVals)
                {
                    if (item.Key.ToLower() == "sid")
                    {
                        sid = item.Value;
                    }
                }
                #endregion

                try
                {
                    #region GETTING USERIDS FROM SID
                    sid = sid.Replace("'", "");
                    List<Activity> userData; //= new List<Activity>();
                    //List<object> sidIDList = new List<object>();
                    List<object> userDetails = new List<object>();

                    try
                    {
                        int count = 0;
                    RetryLogin:
                        GlobalVariables.Activity.TryGetValue(sid, out userData); //userData will always have one value in it. It can never have the same SID twice
                        //bool sidValid = false;

                        if (userData != null)
                        {
                            #region Process SID

                            foreach (var userDetail in userData)
                            {
                                if (userDetail.sid.TrimEnd() == sid) //initial check of the SID - this should be where you should check and refresh..
                                {
                                    if (userDetail.id != null)
                                    {
                                        userMetadata.userFirstName = userDetail.userFirstName;
                                        userMetadata.userLastName = userDetail.userLastName;
                                        userMetadata.userSID = userDetail.sid;
                                        userMetadata.userRequest = this.Request.Method.ToString();
                                        userMetadata.userId = userDetail.id;
                                        userMetadata.userFullName = userDetail.userFullName;
                                        userMetadata.userLoginTime = userDetail.loginTime;
                                        userMetadata.userLogon = userDetail.userLogon;
                                        userMetadata.userDept = userDetail.userDepartment;
                                        userMetadata.userAdmin = userDetail.userPerAdmin;
                                    }
                                    else
                                    {
                                        var response = "Invalid User ID! Please Ensure ID is not null. ";
                                        return Response.AsError(HttpStatusCode.Unauthorized, response);
                                    }

                                }
                                else
                                {
                                    var response = "Invalid SID! Please ensure that the user is still in session. ";
                                    return Response.AsError(HttpStatusCode.Unauthorized, response);
                                }
                            }

                            #endregion

                        }
                        else if (count == 0) //1 attempt max - for testing 
                        {
                            count++;
                            GlobalVariables.GetUserMetadata();
                            goto RetryLogin;
                        }
                        else
                        {
                            var response = "Could not retrieve user Id! Please ensure that the user is logged in";
                            return Response.AsError(HttpStatusCode.ExpectationFailed, response);
                        }

                    }
                    catch
                    {
                        var response = "Could not retrieve user Id! Please ensure there is a connection to the database";
                        return Response.AsError(HttpStatusCode.ExpectationFailed, response);
                    }



                    #endregion

                    if (userMetadata.userAdmin == "3") //3 = admin, 2 = infoAdmin, 1 = normal user -- the user_peradmin 
                    {
                        GlobalVariables.metadataVersion = DateTime.Now.ToString();
                        #region Reload Metadata
                        
                        string msg = GlobalVariables.ReloadMetadata(entityName);



                        if (msg.Contains("Reload Metadata Failed"))
                        {
                            return Response.AsError(HttpStatusCode.ExpectationFailed, msg);
                        }
                        else
                        {
                            List<string> responseMsg = new List<string>();

                            responseMsg.Add(msg);

                            return Response.AsJson(msg);
                        }

                        #endregion
                    }
                    else
                    {
                        log.Warn("Unauthorised Access to Metadata Refresh USER_FULLNAME:" + userMetadata.userFullName);
                        return Response.AsError(HttpStatusCode.Unauthorized, "Unauthorised Access to Metadata Refresh");
                    }



                }
                catch(Exception ex)
                {
                    string msg = ex.Message + "ENTITY: " + entityName;
                    log.Error(msg);
                    return Response.AsError(HttpStatusCode.ExpectationFailed, msg);
                }
              

                //return HttpStatusCode.NotFound; 
            };

            #endregion

            #region REFRESH METADATA: BULK
            Post["metadata/refresh/all/{cmd}"] = parameters =>
            {
                this.EnableCors();
                UserMetadata userMetadata = new UserMetadata();
                string cmd = ((string)parameters.cmd).Trim().ToLower();

                #region USING JSON LIBRARY
                string sid = "";
                DynamicModelBinder dm = new DynamicModelBinder();
                NancyContext context = this.Context;
                string testData = dm.ProcessJson2(context);
                List<ViewSchema> payloadProcessed = new List<ViewSchema>();
                Dictionary<string, string> uiVals = new Dictionary<string, string>();
                try
                {
                    uiVals = JsonConvert.DeserializeObject<Dictionary<string, string>>(testData);
                }
                catch (Exception ex)
                {
                    var response = "Error reading payload: " + ex.Message + "See frameworkLog.txt for details";
                    log.Error(response + System.Environment.NewLine + "STACK TRACE: " + ex.StackTrace.ToString());
                    return Response.AsError(HttpStatusCode.ExpectationFailed, response);
                }


                foreach (var item in uiVals)
                {
                    if (item.Key.ToLower() == "sid")
                    {
                        sid = item.Value;
                    }
                }
                #endregion

                try
                {
                    #region GETTING USERIDS FROM SID
                    sid = sid.Replace("'", "");
                    List<Activity> userData; //= new List<Activity>();
                    //List<object> sidIDList = new List<object>();
                    List<object> userDetails = new List<object>();

                    try
                    {
                        int count = 0;
                    RetryLogin:
                        GlobalVariables.Activity.TryGetValue(sid, out userData); //userData will always have one value in it. It can never have the same SID twice
                        //bool sidValid = false;

                        if (userData != null)
                        {
                            #region Process SID

                            foreach (var userDetail in userData)
                            {
                                if (userDetail.sid.TrimEnd() == sid) //initial check of the SID - this should be where you should check and refresh..
                                {
                                    if (userDetail.id != null)
                                    {
                                        userMetadata.userFirstName = userDetail.userFirstName;
                                        userMetadata.userLastName = userDetail.userLastName;
                                        userMetadata.userSID = userDetail.sid;
                                        userMetadata.userRequest = this.Request.Method.ToString();
                                        userMetadata.userId = userDetail.id;
                                        userMetadata.userFullName = userDetail.userFullName;
                                        userMetadata.userLoginTime = userDetail.loginTime;
                                        userMetadata.userLogon = userDetail.userLogon;
                                        userMetadata.userDept = userDetail.userDepartment;
                                        userMetadata.userAdmin = userDetail.userPerAdmin;
                                    }
                                    else
                                    {
                                        var response = "Invalid User ID! Please Ensure ID is not null. ";
                                        return Response.AsError(HttpStatusCode.Unauthorized, response);
                                    }

                                }
                                else
                                {
                                    var response = "Invalid SID! Please ensure that the user is still in session. ";
                                    return Response.AsError(HttpStatusCode.Unauthorized, response);
                                }
                            }

                            #endregion

                        }
                        else if (count == 0) //1 attempt max - for testing 
                        {
                            count++;
                            GlobalVariables.GetUserMetadata();
                            goto RetryLogin;
                        }
                        else
                        {
                            var response = "Could not retrieve user Id! Please ensure that the user is logged in";
                            return Response.AsError(HttpStatusCode.ExpectationFailed, response);
                        }

                    }
                    catch
                    {
                        var response = "Could not retrieve user Id! Please ensure there is a connection to the database";
                        return Response.AsError(HttpStatusCode.ExpectationFailed, response);
                    }



                    #endregion

                    if(userMetadata.userAdmin == "3")
                    {
                        GlobalVariables.metadataVersion = DateTime.Now.ToString();
                        Dictionary<String, List<Metadata>> newMetadata = GlobalVariables.RefreshMetadata(cmd);
                        string msg = GlobalVariables.ReloadMetadata(newMetadata);
                        newMetadata.Clear();
                        //newMetadata = null;
                        return HttpStatusCode.OK;
                    }
                    else
                    {
                        log.Warn("Unauthorised Access to Metadata Refresh USER_FULLNAME:" + userMetadata.userFullName);
                        return Response.AsError(HttpStatusCode.Unauthorized, "Unauthorised Access to Metadata Refresh");
                    }

                }
                catch (Exception ex)
                {
                    string msg = ex.Message + " REFRESH CMD: " + cmd;
                    log.Error(msg);
                    return Response.AsError(HttpStatusCode.ExpectationFailed, msg);
                }

                //return HttpStatusCode.NotFound; 

            };



            #endregion

            #region REFRESH VIEW SCHEMA

            Post["/updateviews/{cmd}"] = parameters =>
            {
                this.EnableCors();

                #region updateviews variables

                Response resultResponse = "";
                string sid = "";
                string userId = "";
                DynamicModelBinder dm = new DynamicModelBinder();
                NancyContext context = this.Context;
                List<string> payloadUI = dm.ProcessJson(context);
                sid = GetSID(payloadUI); //Contains the SID from the payload
                UserMetadata userMetadata = new UserMetadata();
                bool admin = false;
                var cmd = ((string)parameters.cmd).Trim().ToLower();
                string logMsg = "";

                #endregion

                try
                {

                    #region GETTING USERIDS FROM SID
                    sid = sid.Replace("'", "");
                    List<Activity> userData; //= new List<Activity>();
                    //List<object> sidIDList = new List<object>();
                    List<object> userDetails = new List<object>();

                    try
                    {
                        int count = 0;
                    RetryLogin:
                        GlobalVariables.Activity.TryGetValue(sid, out userData); //userData will always have one value in it. It can never have the same SID twice
                        //bool sidValid = false;

                        if (userData != null)
                        {
                            #region Process SID

                            foreach (var userDetail in userData)
                            {
                                if (userDetail.sid.TrimEnd() == sid) //initial check of the SID - this should be where you should check and refresh..
                                {
                                    if (userDetail.id != null)
                                    {
                                        userMetadata.userFirstName = userDetail.userFirstName;
                                        userMetadata.userLastName = userDetail.userLastName;
                                        userMetadata.userSID = userDetail.sid;
                                        userMetadata.userRequest = this.Request.Method.ToString();
                                        userMetadata.userId = userDetail.id;
                                        userMetadata.userFullName = userDetail.userFullName;
                                        userMetadata.userLoginTime = userDetail.loginTime;
                                        userMetadata.userLogon = userDetail.userLogon;
                                        userMetadata.userDept = userDetail.userDepartment;
                                        userMetadata.userAdmin = userDetail.userPerAdmin;
                                    }
                                    else
                                    {
                                        var response = "Invalid User ID! Please Ensure ID is not null. ";
                                        return Response.AsError(HttpStatusCode.Unauthorized, response);
                                    }

                                }
                                else
                                {
                                    var response = "Invalid SID! Please ensure that the user is still in session. ";
                                    return Response.AsError(HttpStatusCode.Unauthorized, response);
                                }
                            }

                            #endregion

                        }
                        else if (count == 0) //1 attempt max - for testing 
                        {
                            count++;
                            GlobalVariables.GetUserMetadata();
                            goto RetryLogin;
                        }
                        else
                        {
                            var response = "Could not retrieve user Id! Please ensure that the user is logged in";
                            return Response.AsError(HttpStatusCode.ExpectationFailed, response);
                        }

                    }
                    catch
                    {
                        var response = "Could not retrieve user Id! Please ensure there is a connection to the database";
                        return Response.AsError(HttpStatusCode.ExpectationFailed, response);
                    }



                    #endregion

                    #region UPDATE THE VIEW SCHEMA

                    userId = userMetadata.userId;
                    string adminNumber = "3"; //this method is untested
                    string cmdResult = "";
                    if(userMetadata.userAdmin == adminNumber) //3 is the magic number for Admins
                    {
                        admin = true;
                    }

                    if (admin == true)
                    {
                        switch(cmd)
                        {
                            case "all":
                                logMsg = "REFRESHING ALL SQL VIEWS";
                                log.Info(logMsg);
                                break;
                            
                            case "core":
                                logMsg = "REFRESHING CORE SQL VIEWS";
                                log.Info(logMsg);
                                cmdResult = SqlScripts.LoadCoreScripts();
                                /* [START HERE]
                                 * REFRESH THE UNDERLYING VIEWS OF THE API - NEXT STEP LOGICALLY
                                 * RE-DO THE METADATA REFRESH EXECUTION - REAL NEXT STEP 
                                 * 
                                 */



                                resultResponse = RefreshViewCmdResult(cmdResult);
                                break;

                            case "secondary":
                                logMsg = "REFRESHING SECONDARY SQL VIEWS";
                                log.Info(logMsg);
                                break;

                            default:
                                logMsg = "INVALID COMMAND INPUT!! 'updateviews/" + cmd + "'";
                                log.Warn(logMsg);
                                break;
                        }                        
                    }
                    else
                    {
                        return HttpStatusCode.Unauthorized;
                    }


                    #endregion

                    return resultResponse;
                }
                catch
                {
                    return HttpStatusCode.Unauthorized;
                }
                

            };


            #endregion

            #region POST USER METADATA

            /* Returns Metadata about the user
             * PARAMS: SessionID (SID)
             */


            Post["/userMetadata"] = parameters =>
            {

                this.EnableCors();

                NancyContext context = this.Context;
                List<string> payload = new List<string>();
                DynamicModelBinder dm = new DynamicModelBinder();
                payload = dm.ProcessJson(context);

                UserData userData = new UserData();
                Dictionary<string, string> userDetails = new Dictionary<string, string>();

                UserMetadata userMetadata = new UserMetadata();
                List<Activity> userDataList; //= new List<Activity>();


                foreach (var item in payload)
                {
                    string line = item;
                    line = line.Trim();

                    line = line.Replace("\"", "");
                    line = line.ToLower();
                    userData.sid = line.Replace("sid:", "");

                }

                try
                {
                    //logonresult CRMLogon = crm.logon(userData.userName, userData.password);
                    //userData.sid = CRMLogon.sessionid.ToString();
                    //crm.SessionHeaderValue.sessionId = userData.sid;
                    GlobalVariables.Activity.TryGetValue(userData.sid, out userDataList); //userData will always have one value in it. It can never have the same SID twice
                    //bool sidValid = false;

                    if (userDataList != null)
                    {
                        #region Process SID

                        foreach (var userDetail in userDataList)
                        {
                            if (userDetail.sid.TrimEnd() == userData.sid) //initial check of the SID - this should be where you should check and refresh..
                            {
                                if (userDetail.id != null)
                                {
                                    userMetadata.userFirstName = userDetail.userFirstName;
                                    userMetadata.userLastName = userDetail.userLastName;
                                    userMetadata.userSID = userDetail.sid;
                                    userMetadata.userRequest = this.Request.Method.ToString();
                                    userMetadata.userId = userDetail.id;
                                    userMetadata.userFullName = userDetail.userFullName;
                                    userMetadata.userLoginTime = userDetail.loginTime;
                                    userMetadata.userLogon = userDetail.userLogon;
                                    userMetadata.userDept = userDetail.userDepartment;
                                }
                                else
                                {
                                    var response = "Invalid User ID! Please Ensure ID is not null. ";
                                    return Response.AsError(HttpStatusCode.Unauthorized, response);
                                }

                            }
                            else
                            {
                                var response = "Invalid SID! Please ensure that the user is still in session. ";
                                return Response.AsError(HttpStatusCode.Unauthorized, response);
                            }
                        }

                        #endregion

                    }


                    var userPayload = new { userMetadata = userMetadata };

                    return Response.AsJson(userPayload);

                }
                catch (Exception ex)
                {
                    return Response.AsError(HttpStatusCode.NotFound, ex.Message.ToString());
                }

                //var response1 = new { date = DateTime.Now };

                //return Response.AsJson(response1);


            };
            
            
            #endregion

            #endregion

        }


        #region CORE METHODS

        #region FORMAT PAYLOAD


        private List<Dictionary<string, PayloadTemplate>> FormatPayload(List<Metadata> metadata, List<object> entityResults)
        {
            List<Dictionary<string, PayloadTemplate>> payloadList = new List<Dictionary<string, PayloadTemplate>>();

            foreach (var tblResult in entityResults) //entity results has ALL the results from query which wil always be one object.
            {

                /* in here each tblResult or record gets processed into a string */
                string pattern = @"(?!/s)*(?<='),(?=\s[A-Za-z])";
                //string pattern2 = @"\s+([^=]+)\s+=\s+('.+?'|NULL)[,\}]";
                string testRes = tblResult.ToString();

                testRes = testRes.Replace("NULL", "'NULL'");

                //Match[] matches = Regex.Matches(testRes, pattern2).Cast<Match>().ToArray();

                string[] resTest = Regex.Split(testRes, pattern);

                Dictionary<string, PayloadTemplate> record = new Dictionary<string, PayloadTemplate>();
                foreach (string item in resTest) //go through each of the results one at a time, getting its translations
                {
                    /* in here the record gets broken up into columns to be translated
                     * AFTER translating in theory they should be just the one record e.g. tblResult
                     */
                    string line = item;
                    if (line.Length > 1)
                    {
                        PayloadTemplate payloadTemp = new PayloadTemplate();

                        if (line.Contains("{DapperRow"))
                        {
                            line = line.Replace("{DapperRow, ", "");
                        }

                        if (line.EndsWith("}"))
                        {
                            int indx = line.LastIndexOf("}");
                            line = line.Remove(indx);
                        }

                        
                        #region Pre-process

                        string kvp = line;
                        string patternEq = @"(?<=[a-zA-Z0-9])\s=\s(?=')";
                        string[] kvpArr = Regex.Split(kvp, patternEq); //splits the key pair value of field name and value 

                        //payloadTemp.fieldName = kvpArr[0].ToString(); //field name of the result
                        string fieldName = kvpArr[0].ToString().Trim(); //field name of the result
                        payloadTemp.value = kvpArr[1].ToString(); //field val of the result
                        //payloadTemp.value = payloadTemp.value.Replace("'", ""); //THIS IS CAUSING ISSUES IF THERE IS MORE THAN ONE FIELD AND IT HAS THE " ' " 
                        payloadTemp.value = payloadTemp.value.Trim();

                        payloadTemp.value = payloadTemp.value.Trim();

                        Regex rgx = new Regex("[^a-zA-Z0-9 _]");
                        fieldName = rgx.Replace(fieldName, "");
                        fieldName = fieldName.Trim();
                        fieldName = fieldName.ToLower();

                        #endregion

                        #region Translation Values

                        var elts = from elt in metadata
                                   where elt.columnname.ToLower().Equals(fieldName)
                                   && elt.captionCode != null
                                   && elt.captionCode.ToLower().Equals(payloadTemp.value.Trim().ToLower())
                                   select new
                                   {
                                       elt.translation,
                                       elt.translationValue,
                                       elt.datatype,
                                       elt.characterlength
                                   };

                        if (!elts.Any()) //if no translations can be found for the field
                        {
                            var fieldTrans = from ftrans in metadata
                                             where ftrans.columnname.ToLower().Contains(fieldName.Trim().ToLower())
                                             select new { ftrans.translation, ftrans.datatype, ftrans.columnname, ftrans.translationValue, ftrans.captionCode, ftrans.characterlength };

                            foreach (var trans in fieldTrans)
                            {
                                string translatedCol = trans.columnname.ToLower().Trim();
                                string translatedVal = trans.translationValue; //don't change the data in this one because it will alter the actual value to be sent to the UI
                                string transCapCode = trans.captionCode;
                                string transCharLength = trans.characterlength.ToString();

                                if ((payloadTemp.translatedField == null) || (payloadTemp.translatedValue == null)) //will ensure that it will always take the first translation 2014-10-14 changed it from an '&&' to '||' to make sure it always had a translated field and value
                                {
                                    if (translatedCol == fieldName.Trim()) //this clause clears up issues when there is multiple translations
                                    {
                                        #region SET DEFAULTS [AF-122]
                                        payloadTemp.translatedField = trans.translation;
                                        //payloadTemp.translatedValue = payloadTemp.value;
                                        payloadTemp.datatype = trans.datatype;
                                        payloadTemp.characterLength = transCharLength;                                        
                                        #endregion

                                        if (payloadTemp.value != "'NULL'")
                                        {
                                            if (trans.datatype.ToLower() == "datetime")
                                            {
                                                string convTime = payloadTemp.value.Replace("'", "");
                                                DateTime time = Convert.ToDateTime(convTime);
                                                
                                                var sqlDate = time.ToString("yyyy-MM-dd HH:mm:ss");



                                                string timeSpan = time.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds.ToString();
                                                
                                                // OLD WAYS
                                                //payloadTemp.value = "/Date(" + timeSpan + "-0000)/";
                                                //payloadTemp.translatedValue = payloadTemp.value;                                                

                                                // Proper dates with Translation
                                                payloadTemp.value = sqlDate;
                                                payloadTemp.translatedValue = time.ToString();
                                                
                                                payloadTemp.translatedField = trans.translation;
                                                payloadTemp.datatype = trans.datatype;
                                                payloadTemp.characterLength = transCharLength;
                                            }
                                            else if (trans.datatype.ToLower() != "datetime")
                                            {
                                                //check for the translatedValue here
                                                if (transCapCode != null)
                                                {
                                                    string altVal = payloadTemp.value.Trim();

                                                    #region alter the value so that it takes out the trailing spaces. This is part of the translation of Values
                                                    /* The whole point of this excersie is to take out the single quotes then trim it. Reason being that I can't compare the value otherwise and it will miss everything
									                 */
                                                    int ix = altVal.LastIndexOf('\'');
                                                    altVal = altVal.Remove(ix); //takes out the last "'"
                                                    altVal = altVal.Remove(0, 1); //takes out hte first "'"
                                                    altVal = altVal.TrimEnd();

                                                    #endregion

                                                    string testVal = transCapCode.Trim();
                                                    string testVal2 = altVal.Trim();

                                                    if (testVal == testVal2) //this assumes that there is a translated value
                                                    {
                                                        payloadTemp.translatedField = trans.translation;
                                                        payloadTemp.translatedValue = trans.translationValue;
                                                        payloadTemp.datatype = trans.datatype;
                                                        payloadTemp.characterLength = transCharLength;
                                                    }
                                                }
                                                else //no translations available set translatedVal to actual Val
                                                {
                                                    payloadTemp.translatedField = trans.translation;
                                                    payloadTemp.translatedValue = payloadTemp.value;
                                                    payloadTemp.datatype = trans.datatype;
                                                    payloadTemp.characterLength = transCharLength;
                                                }
                                            }

                                        }

                                        else //this is the payload for NULL values
                                        {
                                            payloadTemp.translatedField = trans.translation;
                                            payloadTemp.translatedValue = "";
                                            payloadTemp.datatype = trans.datatype;
                                            payloadTemp.characterLength = transCharLength;
                                            //payloadTemp.value = ""; //this should be done outside!!
                                        }


                                    }//translatedCol IF statement
                                } //IF transaltions == NULL will ensure that it will pick up the first translation

                            }
                        }
                        else //goes here if there is a translation 
                        {
                            Console.WriteLine("Does it even go here??");
                            foreach (var translationData in elts)
                            {
                                payloadTemp.translatedField = translationData.translation;
                                payloadTemp.translatedValue = translationData.translationValue;
                                payloadTemp.datatype = translationData.datatype;
                                payloadTemp.characterLength = translationData.characterlength.ToString();
                            }
                        }

                        #endregion

                        #region Payload Cleanup

                        if (payloadTemp.value != null)
                        {
                            //gets rid of the single quote at the start
                            if (payloadTemp.value.StartsWith("'"))
                            {
                                int indx = payloadTemp.value.IndexOf("'");
                                payloadTemp.value = payloadTemp.value.Remove(indx, 1);
                            }

                            //gets rid of the single quote at the end
                            if (payloadTemp.value.EndsWith("'"))
                            {
                                int indx = payloadTemp.value.LastIndexOf("'");
                                payloadTemp.value = payloadTemp.value.Remove(indx, 1);
                            }

                        }

                        if (payloadTemp.translatedValue != null)
                        {
                            if (payloadTemp.translatedValue.StartsWith("'"))
                            {
                                int indx = payloadTemp.translatedValue.IndexOf("'");
                                payloadTemp.translatedValue = payloadTemp.translatedValue.Remove(indx, 1);
                            }

                            if (payloadTemp.translatedValue.EndsWith("'"))
                            {
                                int indx = payloadTemp.translatedValue.LastIndexOf("'");
                                payloadTemp.translatedValue = payloadTemp.translatedValue.Remove(indx, 1);
                            }
                        }

                        /* Changes returned values if its null - currently it returns NULL on both strings and int types
                            * This should only apply for int types and not for string values
                            */
                        //if it is of type string and NULL
                        //if (payloadTemp.datatype.ToLower() == "string")
                        //{
                        if (payloadTemp.value == "NULL")
                        {
                            payloadTemp.value = "";
                            payloadTemp.translatedValue = "";
                        }
                        if(payloadTemp.value == "'NULL'")
                        {
                            payloadTemp.value = "NULL";
                            payloadTemp.translatedValue = "NULL";
                        }
                        //}

                        #endregion



                        if ((payloadTemp.datatype != null) && (payloadTemp.datatype.ToLower() == "string"))
                        {
                            payloadTemp.value = payloadTemp.value.TrimEnd();
                            if (payloadTemp.translatedValue != null)
                            {
                                payloadTemp.translatedValue = payloadTemp.translatedValue.TrimEnd();
                            }
                            
                        }

                        if(payloadTemp.translatedValue == null)
                        {
                            payloadTemp.translatedValue = payloadTemp.value;
                        }

                        record.Add(fieldName, payloadTemp);

                    } // <-- end of if(item.Length > 1)


                } // <-- end of foreach(string item in res2)

                payloadList.Add(record);
                
            }// <-- end of foreach(var tblResult in entityResults)




            return payloadList;
        }
        

        #endregion

        #region GET USER ID 

        /// <summary>
        /// Returns the User Id based on the SID. Currently Not Implemented
        /// </summary>
        /// <param name="sid"></param>
        public static void GetUserId(string sid)
        {

        }

        #endregion

        #endregion


        #region Utility Methods


        private static List<ViewSchema> ProcessPayload(Dictionary<string, string> uiVals)
        {
            List<ViewSchema> values = new List<ViewSchema>();
            List<ViewSchema> schemaObj = new List<ViewSchema>();

            foreach(var item in uiVals)
            {
                ViewSchema field = new ViewSchema();

                if(item.Key.ToLower() != "sid")
                {
                    field.columnName = item.Key;
                    field.value = item.Value;
                    schemaObj.Add(field);
                }

                
            }


            return schemaObj;
        }
        

        private Response RefreshViewCmdResult(string result)
        {
            if (result == "true")
            {
                return HttpStatusCode.OK;
                

            }
            else
            {
                var response = "An error has occured in running SQL scripts please check the 'sqlLogs.txt' ";
                return Response.AsError(HttpStatusCode.ExpectationFailed, response);
                //break;
            }

            
        }

        private static UserMetadata GetUserMetadata(List<object> userDetails, string sid, string userId)
        {
            UserMetadata userMetadata = new UserMetadata();

            string userData = "";

            foreach (object item in userDetails)
            {
                userData = item.ToString();
                userData = userData.Replace("{DapperRow, ", " "); //need to separate the field name and teh value
                userData = userData.Replace("user_firstname = ", "");
                userData = userData.Replace("user_lastname = ", "");
                userData = userData.Replace("}", " ");
                userData = userData.Replace("'", " ");
                userData = userData.Trim();
            }

            string[] userDetail = userData.Split(',');

            userMetadata.userFirstName = userDetail[0].ToString();
            userMetadata.userLastName = userDetail[1].ToString();
            userMetadata.userId = userId;
            userMetadata.userSID = sid;
            userMetadata.userFullName = userMetadata.userFirstName + userMetadata.userLastName;

            return userMetadata;
        }

        private static string GetFieldValFromQuery(List<object> queryResult)
        {
            string result = "";

            foreach (var item in queryResult)
            {
                string i = item.ToString();

                string[] arr = i.Split('=');

                result = arr[1].Replace('}', ' ');

                result = result.Trim();
            }



            return result;
        }


        private static List<ViewSchema> GetTableValues2(List<string> payload) //do not cull: sends back the values for the tables .. ONLY FOR PAYLOAD
        {
            List<ViewSchema> values = new List<ViewSchema>();
            List<ViewSchema> schemaObj = new List<ViewSchema>();

            //string p = @"\\/Date\((\d+)\+\d+\)\\/"; //this can be used when starting to do the regex of the date time values that come in  with the weird JSON Date Time format.

            /* SID GETS REMOVED BY ANOTEHR METHOD
             */

            foreach (var line in payload)
            {

                ViewSchema field = new ViewSchema();
                int indx;

                string item = line.ToString();
                item = item.ToString();

                if (item.EndsWith(","))
                {
                    indx = item.LastIndexOf(",");
                    item = item.Remove(indx);
                }

                if (!item.Contains("SID"))
                {
                    if(!item.Contains("sid"))
                    {
                        string pattern = @"(.+?)\:(.+)";
                        string[] res = Regex.Split(item, pattern);

                        string pattern1 = "^\"|\"$"; //used to take out the (") from the start and teh end

                        string[] colArr = Regex.Split(res[1].Trim(), pattern1); //contains column name
                        string[] valArr = Regex.Split(res[2], pattern1); //contains value 


                        field.columnName = colArr[1]; //actual column name
                        field.value = valArr[1]; //actual value


                        schemaObj.Add(field);

                    }

                }
            }

            #region OLD CODE
            /*
            foreach (var item in payload)
            {

                ViewSchema field = new ViewSchema();


                if (item.Length != 1)
                {
                    if (!item.Contains("SID"))
                    {
                        //OLD CODE
                        int indx;

                        x = item.Replace(replace, "=").Trim(); //what if there is a (=) in the line?? -- cuts off the rest of the string
                        x = x.Replace('"', ' ').Trim(); //what if there is (") in the line?? -- replaces the bloodything.

                        if ((x.EndsWith(",") == true))
                        {
                            indx = x.LastIndexOf(",");
                            x = x.Remove(indx);
                        }

                        rawFields = x.Split('=');
                        colName = rawFields[0].Trim();

                        //process this field
                        value = rawFields[1].Trim();

                        if (value.Contains('\'')) //this ensures that the ' gets fixed.
                        {
                            value = value.Replace("'", "''");
                        }

                        if (value.Contains("Date"))
                        {


                            value = value.Replace("Date", " ");
                            value = value.Replace("/", " ");
                            //value = value.Trim();
                            //string[] arr1 = value.Split('-');
                            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                            //double tim = Convert.ToDouble(arr1[0]);
                            int tim = Convert.ToInt32(value);

                            epoch = epoch.AddSeconds(tim);
                            dat = epoch;

                            ////    long ticks = Convert.ToInt64(arr1[0]);

                            ////        //Convert.ToInt32(value);
                            ////    DateTime dt = new DateTime(ticks);
                            ////    string time = dt.ToString("MMMM dd, yyyy");
                        }

                        field.columnName = colName;
                        field.value = value;

                        //values.Add(field);

                    }

                    if (field.columnName != null)
                    {
                        values.Add(field);
                    }



                }

            }
            */

            //return values;
            #endregion

            return schemaObj;
        }


        private static List<ViewSchema> DrillDown(string tableName)
        {
            List<ViewSchema> orign2 = new List<ViewSchema>();

            if ((tableName.StartsWith("v")) || (tableName.StartsWith("_v"))) //more than one level deep
            {
                //look for the actual origin table

                string vName = tableName.ToLower();
                GlobalVariables.ViewSchema.TryGetValue(vName, out orign2); //for the case of vCompany: L1 - vCompanyPE, L2 - Company, CRMEmailPhoneData 

                foreach (var item in orign2)
                {
                    string viewName = item.tableName;

                    if ((viewName.StartsWith("v")) || (viewName.StartsWith("_v"))) //more than one level deep
                    {
                        DrillDown(viewName);
                    }
                    else
                    {
                        return orign2;
                    }
                }

            }

            return orign2;
        }

        private static string GetWhereTable(string whereTableName)
        {
            string rem1 = "{DapperRow, TableName = '";
            string rem2 = "', fieldName ";
            string result = whereTableName.Replace(rem1, " ");

            result = result.Replace(rem2, " ");

            string[] arr = result.Split('=');
            string finalResult = arr[0];

            finalResult = finalResult.Trim();

            return finalResult;
        }

        private static string GetUserID(List<object> sidIds)
        {
            string userId = "";
            string rawId = "";

            foreach (object item in sidIds)
            {
                rawId = item.ToString();
                rawId = rawId.Replace("{DapperRow, Acty_UserID = ", " "); //need to separate the field name and teh value
                rawId = rawId.Replace("}", " ");
                rawId = rawId.Replace("'", " ");
                rawId = rawId.Trim();
            }

            userId = rawId;
            return userId;
        }

        private static string GetSID(List<string> payload)
        {
            string sid = "";
            string item;
            string[] arr;
            string val;

            foreach (string value in payload)
            {
                if (value.Length != 1)
                {
                    arr = value.Split(':');
                    val = arr[0].ToLower();
                    item = arr[1].Trim().ToLower();

                    if (val.Contains("sid"))
                    {
                        item = item.Replace('"', ' ');
                        item = item.Replace(',', ' ');
                        item = item.Trim();
                        sid = "'" + item + "'";
                    }
                }
            }

            return sid;
        }


        private static string SortHandler(string sortString)
        {
            //make the order by string here

            string[] sortItems = sortString.Split('&');
            string[] sortOrder;
            string result = "";

            foreach (var item in sortItems)
            {
                sortOrder = item.Split('=');

                result = result + sortOrder[0] + " " + sortOrder[1] + ", ";
            }

            int remove = result.LastIndexOf(", ");

            result = result.Remove(remove);
            result = "ORDER BY " + result;

            return result;
        }

        private static List<string> FilterTranslations(string fields)
        {
            List<string> result = new List<string>();
            string[] splitFields = fields.Split('&');

            foreach (string field in splitFields)
            {
                result.Add(field);
            }

            return result;
        }

        private static List<string> GetValues(List<string> payload)
        {
            List<string> tables = new List<string>();
            string x;
            //string[] items;
            string[] rawFields;
            int indx;
            string replace = "\":\"";

            foreach (var item in payload)
            {
                if (item.Length != 1)
                {
                    if (!item.Contains("SID"))
                    {
                        x = item.Replace(replace, "=").Trim();
                        x = x.Replace('"', ' ').Trim();



                        if ((x.EndsWith(",") == true))
                        {
                            indx = x.LastIndexOf(",");
                            x = x.Remove(indx);
                        }

                        rawFields = x.Split('=');
                        tables.Add(rawFields[1].Trim());
                    }

                }
            }

            return tables;
        }

        /* Good Method for manipulating the payload
         */
        private static string GetIds(List<object> resultsList)
        {
            string rawId = "";

            string id = "";

            foreach (var item in resultsList)
            {
                rawId = item.ToString();
                rawId = rawId.Replace("{DapperRow, ", " "); //need to separate the field name and teh value
                rawId = rawId.Replace("}", " ");
                rawId = rawId.Replace("'", " ");
                rawId = rawId.Trim();
            }
            string[] idArr = rawId.Split('=');

            id = idArr[1].Trim();

            return id;
        }

        private static string UpdateTables(List<object> tables)
        {
            string uniqueTableName = "";
            HashSet<string> uniqueTableNames = new HashSet<string>(); //used to keep items unique

            foreach (var rowName in tables)
            {
                string tableName = rowName.ToString();

                if (!tableName.Contains("OpportunityHistory")) //bug in the OpportunityHistory Table...for some reason it has the comp_name in the bloody table
                {
                    if (!tableName.Contains("Progress"))
                    {
                        tableName = tableName.Replace("{DapperRow, name = ", " ");
                        tableName = tableName.Replace("}", " ");
                        tableName = tableName.Replace("'", " ");
                        tableName = tableName.Trim();

                        //put it in a hashset so that I can only have unique Items in it.
                        uniqueTableNames.Add(tableName); //this now contains all the tables thats needed to update the view.
                    }
                }

            }

            foreach (var item in uniqueTableNames)
            {
                uniqueTableName = uniqueTableName + item;
            }

            return uniqueTableName;
        }

        private static List<string> GetFields(List<string> payload) //do not cull: sends back the tables needed and takes away the values itself.. GENERIC
        {
            List<string> tables = new List<string>();
            string x, y;
            string[] items;
            string[] rawFields;

            foreach (var item in payload)
            {
                if (item.Length != 1)
                {
                    if (!item.Contains("SID"))
                    {
                        items = item.Split(',');
                        x = items[0].Replace('"', ' ').Trim();

                        x = x.Replace(':', '=');

                        rawFields = x.Split('='); //this is the important part

                        tables.Add(rawFields[0].Trim());


                        y = rawFields[0].Trim() + " = '" + rawFields[1].Trim() + "'";

                    }
                }
            }

            return tables;
        }

        private static List<string> GetTableValues(List<string> payload) //do not cull: sends back the tables needed and takes away the values itself.. ONLY FOR PAYLOAD
        {
            List<string> values = new List<string>();
            string x, y;
            //string[] items;
            string[] rawFields;
            string replace = "\":\"";
            foreach (var item in payload)
            {
                if (item.Length != 1)
                {
                    if (!item.Contains("SID"))
                    {

                        int indx;

                        x = item.Replace(replace, "=").Trim(); //is this the same as y??? - Yes
                        x = x.Replace('"', ' ').Trim();

                        if ((x.EndsWith(",") == true))
                        {
                            indx = x.LastIndexOf(",");
                            x = x.Remove(indx);
                        }

                        rawFields = x.Split('='); //this is the important part

                        y = rawFields[0].Trim() + " = '" + rawFields[1].Trim() + "'";


                        values.Add(y);
                    }


                }
            }

            return values;
        }

        private static string GetColumn(List<string> payload) //important for now...
        {
            string[] items;
            string x, colName;
            string[] y;

            colName = "";

            foreach (var item in payload)
            {
                if (item.Length != 1)
                {
                    items = item.Split(',');
                    x = items[0];
                    y = x.Split(':');
                    colName = y[0];
                    colName = colName.Replace('"', '\'').ToLower().Trim();

                }
            }

            return colName;
        }

        //this is semi important...but could be replaced
        private static string GetHeaders(List<string> payload)
        {
            string[] items;
            string x;
            string y = "";
            string[] headers;

            foreach (var item in payload)
            {
                if (item.Length != 1)
                {
                    items = item.Split(',');
                    x = items[0];
                    headers = x.Split('_');
                    y = headers[0].Replace('"', ' ').ToLower().Trim();

                }
            }

            return y + "_";
        }

        private static string FormatPayload(List<string> payload)
        {
            string rawQuery = "";
            string x, y;
            string[] items;
            string[] rawFields;

            foreach (var item in payload)
            {
                if (item.Length != 1)
                {
                    items = item.Split(',');
                    x = items[0].Replace('"', ' ').Trim();

                    x = x.Replace(':', '=');

                    rawFields = x.Split('='); //this is the important part

                    y = rawFields[0].Trim() + " = '" + rawFields[1].Trim() + "'";

                    rawQuery = rawQuery + y + ", ";

                }
            }

            return rawQuery;
        }

        private static string UpdateQuery(string query)
        {
            string[] items = query.Split('&');
            string result = "";
            string updateQuery;

            updateQuery = "";

            foreach (string field in items)
            {
                updateQuery = updateQuery + field + " AND ";
            }

            if (updateQuery.EndsWith("AND "))
            {
                string remove = "AND";

                result = updateQuery.Substring(0, updateQuery.LastIndexOf(remove));
            }

            return result;
        }


        private static string FilterQuery(string query)
        {
            string[] items = query.Split('&');
            string result = "";
            string searchQuery, whereQuery;

            searchQuery = "SELECT ";
            whereQuery = "";
            char wildcard = '%';

            // HANDLES WHERE QUERY
            if (query.Contains("="))
            {
                foreach (string item in items)
                {
                    string[] fields = item.Split('=');

                    string field = fields[0];
                    string val = fields[1];
                    string value = val; // "'" + val + "'"; //check this one
                    string trueClause = "";
                    string fullquery = "";
                    string trueFullClause = "";

                    if (value.Contains("$"))
                    {
                        string[] likeClause = val.Split('$');

                        if (val.StartsWith("$"))
                        {
                            trueClause = wildcard + likeClause[1];
                        }

                        if (val.EndsWith("$"))
                        {
                            trueClause = likeClause[0] + wildcard;
                        }


                        if ((val.StartsWith("$")) && (val.EndsWith("$")))
                        {
                            trueClause = wildcard + likeClause[1] + wildcard;
                        }

                        if (trueClause.Contains('\''))
                        {
                            trueClause = trueClause.Replace("'", "''");
                        }

                        trueFullClause = trueFullClause + " LIKE '" + trueClause + "'";

                        fullquery = field + trueFullClause; //dont put ' = ' because it will screw up the damn thing
                    }
                    else if(value == "NULL")
                    {
                        fullquery = field + " is NULL";
                    }
                    else if (value == "!NULL")
                    {
                        fullquery = field + " is not NULL";
                    }

                    else //proceed as normal query
                    {
                        if (value.Contains('\''))
                        {
                            value = value.Replace("'", "''");
                        }


                        fullquery = field + " = '" + value + "'";


                        
                    }

                    whereQuery = whereQuery + fullquery + " AND ";

                }

                if (whereQuery.EndsWith("AND "))
                {
                    string remove = "AND";

                    result = whereQuery.Substring(0, whereQuery.LastIndexOf(remove));

                }

            }
            //HANDLES SELECT QUERY
            else
            {
                foreach (string field in items)
                {
                    searchQuery = searchQuery + field + ", ";
                    //result = searchQuery;
                }

                if (searchQuery.EndsWith(", "))
                {
                    string remove = ", ";

                    result = searchQuery.Substring(0, searchQuery.LastIndexOf(remove));

                }

            }



            return result;
        }


        #endregion

        private ColumnMetadataFields FindMetadataFields(List<Metadata> metadata)
        {
            var fields = new ColumnMetadataFields(); //instantiate fields

            var primarykey = metadata.Find(i => i.isprimarykey == true);
            var deleted = metadata.Find(i => i.columnname.ToLower().Contains("_deleted"));
            var updatedby = metadata.Find(i => i.columnname.ToLower().Contains("_updatedby"));
            var updateddate = metadata.Find(i => i.columnname.ToLower().Contains("_updateddate"));

            var createddate = metadata.Find(i => i.columnname.ToLower().Contains("_createddate"));
            var createdby = metadata.Find(i => i.columnname.ToLower().Contains("_createdby"));

            if (primarykey != null)
            {
                fields.primarykey = primarykey.columnname;
            }

            if (deleted != null)
            {
                fields.deleted = deleted.columnname;
            }

            if (updatedby != null)
            {
                fields.updatedby = updatedby.columnname;
            }

            if (updateddate != null)
            {
                fields.updateddate = updateddate.columnname;
            }

            if (createddate != null)
            {
                fields.createddate = createddate.columnname;
            }

            if (createdby != null)
            {
                fields.createdby = createdby.columnname;
            }


            return fields;

        }

    }
}
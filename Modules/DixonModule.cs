﻿using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using accsys.crmframework.api.infrastructure;
using Dapper;
using Nancy;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Nancy.ModelBinding;
using System.Timers;
using System.IO;
using System.Web.Script.Serialization;
using ServiceStack.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Collections.Specialized;

namespace accsys.crmframework.api
{
    public class DixonModule : NancyModule
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("FrameworkLogger");
        public DixonModule(ConnectionFactory connectionFactory)
        {
            #region CREATE

            Post["/dixon/new/{entity}"] = parameters =>
            {
                Stopwatch timer = Stopwatch.StartNew();
                this.EnableCors();

                #region CREATES VARIABLES

                var entityName = ((string)parameters.entity).Trim().ToLower();

                string sid = "";
                string insertSql = "";
                string returnSql = "";

                string headQuery, setQuery;
                string columns = "";
                string vals = "";

                string newId = ""; //variable to put in the new ID generated
                string primaryKey = "";
                // string prefix = ""; // was goign to be used to get the prefix of the table
                // string createdDateField = "_createddate"; //was going to be used for the metadata fields
                // string updatedDateField = "_updateddate"; // was going to be used for the metadata fields


                string createdDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                DynamicModelBinder dm = new DynamicModelBinder();
                NancyContext context = this.Context;

                #region USING JSON LIBRARY

                string testData = dm.ProcessJson2(context);
                List<ViewSchema> payloadProcessed = new List<ViewSchema>();
                Dictionary<string, string> uiVals = new Dictionary<string, string>();

                try
                {
                    uiVals = JsonConvert.DeserializeObject<Dictionary<string, string>>(testData);
                }
                catch (Exception ex)
                {
                    var response = "Error reading payload: " + ex.Message + "See frameworkLog.txt for details";
                    log.Error(response + System.Environment.NewLine + "STACK TRACE: " + ex.StackTrace.ToString());
                    return Response.AsError(HttpStatusCode.ExpectationFailed, response);
                }

                foreach (var item in uiVals)
                {
                    if (item.Key.ToLower() == "sid")
                    {
                        sid = item.Value;
                    }
                }

                payloadProcessed = ProcessPayload(uiVals);

                #endregion

                //List<Metadata> metadata;

                List<string> fields = new List<string>();
                List<string> values = new List<string>();

                // _spINFCreateRecord vard
                // string crPrimaryKey = "";

                #endregion

                List<Dictionary<string, DixonPayload>> payloadList = new List<Dictionary<string, DixonPayload>>();
                using (var con = connectionFactory.GetOpenConnection())
                {
                    List<object> entityResults = new List<object>();
                    List<object> pkObj = new List<object>();
                    string crDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    foreach (var item in payloadProcessed) /* payload processed is from the UI and was put in the ViewSchema Class */
                    {
                        string field = item.columnName;
                        string value = item.value;
                        columns = columns + field + ", "; //concatenates the fields to be inserted in the table

                        if (value == "@NULL@")
                        {
                            value = "NULL";
                            vals += value + ", "; //concatenates teh values to be insterted in the table
                        }
                        else if (value.Contains("'"))
                        {
                            value = value.Replace("'", "''");
                            vals += "'" + value + "', "; //concatenates teh values to be insterted in the table
                        }
                        else
                        {
                            vals += "'" + value + "', "; //concatenates teh values to be insterted in the table
                        }
                    }

                    //need something here to strip out the last comma
                    int colindx = columns.LastIndexOf(',');
                    int valindx = vals.LastIndexOf(',');

                    columns = columns.Remove(colindx, 1);
                    vals = vals.Remove(valindx, 1);

                    string sqlGetId = @"SELECT Col.Column_Name as columnName from 
                                        INFORMATION_SCHEMA.TABLE_CONSTRAINTS Tab, 
                                        INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE Col 
                                        WHERE 
                                        Col.Constraint_Name = Tab.Constraint_Name
                                        AND Col.Table_Name = Tab.Table_Name
                                        AND Constraint_Type = 'PRIMARY KEY'
                                        AND Col.Table_Name = '" + entityName + "'";

                    // need to do get the primary key here first 
                    try
                    {
                        pkObj = con.Query(sqlGetId).ToList(); //gets the primary key of the table

                        primaryKey = GetIds(pkObj);

                        // need to do the getPrefix here

                        //prefix = GetPrefix(primaryKey); //this was going to be used for the 'metadata fields' but it would be easier to do it in angular instead
                    }
                    catch (Exception ex)
                    {
                        string response1 = "MSG: " + ex.Message;
                        Response ret = Response.AsError(HttpStatusCode.NotFound, response1);
                        return ret;
                    }

                    // concat the prefix to the updated date and the createddate
                    // createdDateField = prefix + createdDateField;
                    // updatedDateField = prefix + updatedDateField;

                    // then add it into the headQuery
                    



                    //headQuery = "INSERT INTO " + entityName + " (" + columns + ", " + createdDateField + ", " + updatedDateField + ")"; //need to add the created date and the updated date fields
                    headQuery = "INSERT INTO " + entityName + " (" + columns +  ")";
                    //construct set query
                    string dateTime = DateTime.Now.ToString();

                    //setQuery = " VALUES (" + vals + ", '" + createdDate + "', '" + createdDate + "')";
                    setQuery = " VALUES (" + vals + ")";



                    // Insert into externalprojectsclients (client_firstname, client_lastname) values ('john', 'Smith') select scope_identity() as lastID //query that will actually work

                    //construct insert SQL
                    //insertSql = keyTableSQL + headQuery + intoVarTableSQL + setQuery + returnKeySQL;
                    insertSql = headQuery + setQuery + " select scope_identity() as lastId";

                    try
                    {
                        entityResults = con.Query(insertSql).ToList(); //will do the actual insert
                        //need to format entityResults so that I can get my select statement ready
                        //afterwards format the payload
                        newId = GetIds(entityResults);

                        returnSql = "SELECT * FROM " + entityName + " WHERE " + primaryKey + " = " + newId;
                        List<object> recordResults = con.Query(returnSql).ToList(); //returns the inserted record

                        payloadList = FormatPayload(recordResults);
                    }
                    catch (Exception ex)
                    {
                        string response1 = "MSG: " + ex.Message;
                        Response ret = Response.AsError(HttpStatusCode.NotFound, response1);

                        timer.Stop();
                        TimeSpan timespan = timer.Elapsed;
                        string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                        string ms = timespan.TotalMilliseconds.ToString();
                        string min = timespan.TotalMinutes.ToString();
                        string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | ENTITY: " + entityName + " | STATUS_CODE: " + ret.StatusCode.ToString() + " | " + response1;
                        log.Error(perfMsg);

                        return ret;
                    }
                }

                var result = new { _url = Request.Url, payload = payloadList };
                Response retSuccess = Response.AsJson(result);

                timer.Stop();
                TimeSpan timespanSuccess = timer.Elapsed;
                string timeElapsedSecondsSuccess = timespanSuccess.TotalSeconds.ToString();
                string msSuccess = timespanSuccess.TotalMilliseconds.ToString();
                string minSuccess = timespanSuccess.TotalMinutes.ToString();
                string perfMsgSuccess = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSecondsSuccess + " | TOTAL_RECORDS: " + payloadList.Count() + " | USER: N/A | USERID: N/A | ENTITY: " + entityName + " | STATUS_CODE: " + retSuccess.StatusCode.ToString();
                log.Info(perfMsgSuccess);

                return retSuccess;
            };

            #endregion

            #region RETRIEVE

            // test route for authentication

            Get["dixon/authenticate"] = parameters =>
            {
                //Response ret = Response.AsJson(HttpStatusCode.OK);

                //return ret;
                string msg = "Test Dixon Authenticate";
                NancyContext context = this.Context;
                var userName = Environment.UserName;


                var returnMsg = new { status = HttpStatusCode.OK, msg = msg, userName = userName };


                return Response.AsJson(returnMsg);
            };


            Get["dixon/{entity}/{fields}/{where}"] = parameters =>
            {
                Stopwatch timer = Stopwatch.StartNew();
                this.EnableCors();

                #region SET-UP

                var entityName = ((string)parameters.entity).Trim().ToLower();
                var fields = ((string)parameters.fields).Trim();
                var where = ((string)parameters.where).Trim();
                string selectClause, whereClause, sql;
                selectClause = FilterQuery(fields);
                whereClause = FilterQuery(where);

                #endregion

                List<Metadata> metadata; //DOESNT DO ANYTHING TAKE OUT LATER [DEL]
                if (GlobalVariables.Metadata.TryGetValue(entityName, out metadata)) //[DEL]
                {
                    List<object> entityResults = new List<object>();
                    List<Dictionary<string, DixonPayload>> payloadList = new List<Dictionary<string, DixonPayload>>();

                    using (var con = connectionFactory.GetOpenConnection())
                    {
                        //sql = "SELECT * FROM [dbo].[" + entityName + "]";

                        sql = selectClause + " FROM " + entityName + " WHERE " + whereClause;
                        
                        try
                        {
                            entityResults = con.Query(sql).ToList();

                            if (entityResults.Count != 0)
                            {
                                payloadList = FormatPayload(entityResults);
                                Console.WriteLine("Data Processed, attempting to return");
                            }
                        }
                        catch (Exception ex)
                        {
                            string response = "MSG: " + ex.Message;
                            Response ret = Response.AsError(HttpStatusCode.NotFound, response);

                            timer.Stop();
                            TimeSpan timespan = timer.Elapsed;
                            string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                            string ms = timespan.TotalMilliseconds.ToString();
                            string min = timespan.TotalMinutes.ToString();
                            string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | ENTITY: " + entityName + " | STATUS_CODE: " + ret.StatusCode.ToString() + " | " + response;
                            log.Error(perfMsg);

                            return ret;
                        }
                    }

                    var payload = new { _url = Request.Url, payload = payloadList };

                    return Response.AsJson(payload);
                }
                else
                {
                    string response = "MSG :" + entityName + " doesn't exist in metadata please check that the entity name is spelt correctly.";
                    Response ret = Response.AsError(HttpStatusCode.NotFound, response);

                    timer.Stop();
                    TimeSpan timespan = timer.Elapsed;
                    string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                    string ms = timespan.TotalMilliseconds.ToString();
                    string min = timespan.TotalMinutes.ToString();
                    string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | ENTITY: " + entityName + " | STATUS_CODE: " + ret.StatusCode.ToString() + " | " + response;
                    log.Info(perfMsg);

                    return ret;
                }

            };

            #endregion

            // deletes are always going to be a soft delete i.e. isdeleted = 1
            // so there's no reason to add another route for deletes

            #region UPDATE/DELETE 

            Post["/dixon/update/{entity}/{where}"] = parameters =>
            {
                Stopwatch timer = Stopwatch.StartNew();
                this.EnableCors();

                //get the variables from the screen and the json packet
                #region UPDATE VARIABLES

                var entityName = ((string)parameters.entity).Trim().ToLower();
                var where = ((string)parameters.where).Trim();

                string sid = "";

                string setQuery = "";
                string whereClause = "";
                string headQuery = "";
                string sql = "";
                string returnSql = "";
                string whereReturn = FilterQuery(where);
                string[] whereItems = where.Split('=');
                string[] whereRes = whereItems[0].Split('_'); //contains the field in the where statement

                DynamicModelBinder dm = new DynamicModelBinder();
                NancyContext context = this.Context;
                string testData = dm.ProcessJson2(context);
                List<ViewSchema> payloadProcessed = new List<ViewSchema>();
                Dictionary<string, string> uiVals = new Dictionary<string, string>();

                try
                {
                    uiVals = JsonConvert.DeserializeObject<Dictionary<string, string>>(testData);
                }
                catch (Exception ex)
                {
                    var response = "Error reading payload: " + ex.Message + "See frameworkLog.txt for details";
                    log.Error(response + System.Environment.NewLine + "STACK TRACE: " + ex.StackTrace.ToString());
                    return Response.AsError(HttpStatusCode.ExpectationFailed, response);
                }

                foreach (var item in uiVals)
                {
                    if (item.Key.ToLower() == "sid")
                    {
                        sid = item.Value;
                    }
                }

                payloadProcessed = ProcessPayload(uiVals);


                #endregion

                List<Dictionary<string, DixonPayload>> payloadList = new List<Dictionary<string, DixonPayload>>();
                using (var con = connectionFactory.GetOpenConnection())
                {
                    //updates will be done directly to TABLES only

                    setQuery = "";
                    foreach (var val in payloadProcessed)
                    {
                        string field = val.columnName;
                        string value = val.value;

                        //SET @NULL@ to actual null values
                        //SET NULL to string values
                        // might change this later on but for now its working
                        if (value == "@NULL@")
                        {
                            setQuery = setQuery + field + " = NULL, ";
                        }
                        else if (value.Contains("'"))
                        {
                            value = value.Replace("'", "''");
                            setQuery = setQuery + field + " = '" + value + "', ";
                        }
                        else
                        {
                            setQuery = setQuery + field + " = '" + value + "', ";
                        }

                    }

                    whereClause = " WHERE " + whereReturn; //uses where return because its to a direct table
                    headQuery = "UPDATE " + entityName + " SET ";


                    int indx = setQuery.LastIndexOf(',');
                    setQuery = setQuery.Remove(indx, 1);
                    sql = headQuery + setQuery + whereClause;

                    returnSql = "SELECT * FROM " + entityName + " WHERE " + whereReturn;

                    try
                    {
                        con.Execute(sql); //executes the update itself  which doesn't return anything
                    }
                    catch (Exception ex)
                    {
                        string response = "MSG: " + ex.Message;
                        Response ret = Response.AsError(HttpStatusCode.NotFound, response);

                        timer.Stop();
                        TimeSpan timespan = timer.Elapsed;
                        string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                        string ms = timespan.TotalMilliseconds.ToString();
                        string min = timespan.TotalMinutes.ToString();
                        string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | ENTITY: " + entityName + " | STATUS_CODE: " + ret.StatusCode.ToString() + " | " + response;
                        log.Error(perfMsg);

                        return ret;
                    }


                    // need to return the record that has been updated
                    #region RETURN QUERY
                    
                    try
                    {
                        List<object> entityResults = con.Query(returnSql).ToList(); //executes the return query
                        payloadList = FormatPayload(entityResults);// format return query

                        var payload = new { _url = Request.Url, payload = payloadList };
                        Response ret = Response.AsJson(payload);

                        //stop the timer once its returned the payload to the UI
                        timer.Stop();
                        TimeSpan timespan = timer.Elapsed;
                        string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                        string ms = timespan.TotalMilliseconds.ToString();
                        string min = timespan.TotalMinutes.ToString();

                        string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | TOTAL_RECORDS: " + payloadList.Count() + " | USER: N/A | USERID: N/A | ENTITY: " + entityName + " | STATUS_CODE: " + ret.StatusCode.ToString();
                        log.Info(perfMsg);

                        return ret;

                    }
                    catch (Exception ex)
                    {
                        string response = "MSG: " + ex.Message;
                        Response ret = Response.AsError(HttpStatusCode.NotFound, response);

                        timer.Stop();
                        TimeSpan timespan = timer.Elapsed;
                        string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                        string ms = timespan.TotalMilliseconds.ToString();
                        string min = timespan.TotalMinutes.ToString();

                        string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | ENTITY: " + entityName + " | STATUS_CODE: " + ret.StatusCode.ToString() + " | " + response;
                        log.Error(perfMsg);

                        return ret;
                    }

                    #endregion



                };
                
            };


            #endregion

            #region STORED PROCEDURE POST

            Post["/dixon/procedure/{storedProc}/"] = parameters =>
            {
                //[PERF-BENCH]
                Stopwatch timer = Stopwatch.StartNew();

                this.EnableCors();

                var storedProc = parameters.storedProc; //don't clean up as it will have spaces
                DynamicModelBinder dm = new DynamicModelBinder();
                NancyContext context = this.Context;

                string testData = dm.ProcessJson2(context);
                Dictionary<string, string> uiVals = new Dictionary<string, string>();

                try
                {
                    uiVals = JsonConvert.DeserializeObject<Dictionary<string, string>>(testData);
                }
                catch (Exception ex)
                {
                    string errMsg = "ERROR PROCESSING PAYLOAD - ERROR MSG: " + ex.Message + "See framewroklog.txt for details.";
                    log.Error(errMsg + "STACK: " + ex.StackTrace);
                    return Response.AsError(HttpStatusCode.NotFound, errMsg);

                }

                List<string> payload = dm.ProcessJson(context);

                List<string> vals = GetValues(payload); //this is the vars that the stored proc will need

                string sql = "";
                List<object> entityResults;

                try
                {
                    using (var con = connectionFactory.GetOpenConnection())
                    {
                        //foreach (var value in vals)
                        foreach (var values in uiVals)
                        {
                            string value = values.Value;
                            if (value.Contains("'"))
                            {
                                value = value.Replace("'", "''");
                            }

                            sql += "'" + value + "', ";

                        }

                        sql = storedProc + " " + sql;

                        if (sql.EndsWith(", "))
                        {
                            int remove = sql.LastIndexOf(',');

                            sql = sql.Remove(remove);
                        }


                        try
                        {
                            entityResults = con.Query(sql).ToList();
                        }
                        catch (Exception ex)
                        {
                            string response = "MSG: " + ex.Message;
                            Response ret = Response.AsError(HttpStatusCode.NotFound, response);

                            timer.Stop();
                            TimeSpan timespan = timer.Elapsed;
                            string timeElapsedSeconds = timespan.TotalSeconds.ToString();
                            string ms = timespan.TotalMilliseconds.ToString();
                            string min = timespan.TotalMinutes.ToString();
                            string perfMsg = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSeconds + " | ENTITY: " + storedProc + " | STATUS_CODE: " + ret.StatusCode.ToString() + " | " + response;
                            log.Error(perfMsg);

                            return ret;
                        }

                        Response retSuccess;

                        var responseSuccess = new { _url = Request.Url, payload = entityResults }; //if the record has been marked deleted it will return 0 results

                        retSuccess = Response.AsJson(responseSuccess);

                        //[PERF-BENCH]
                        #region PERF-BENCH

                        timer.Stop();
                        TimeSpan timespanSuccess = timer.Elapsed;
                        string timeElapsedSecondsSuccess = timespanSuccess.TotalSeconds.ToString();
                        string msSuccess = timespanSuccess.TotalMilliseconds.ToString();
                        string minSuccess = timespanSuccess.TotalMinutes.ToString();

                        string perfMsgSuccess = "PATH: " + Request.Url.Path.ToString() + " | TIME_TAKEN: " + timeElapsedSecondsSuccess + " | TOTAL_RECORDS: " + entityResults.Count() + " | USER: " + " | ENTITY: " + storedProc + " | STATUS_CODE: " + retSuccess.StatusCode.ToString();
                        log.Info(perfMsgSuccess);



                        #endregion

                        return retSuccess;

                    }
                }

                catch
                {
                    return HttpStatusCode.NotFound;
                }

            };

            #endregion

            #region OPTIONS
            Options["/dixon/new/{entity}/"] = parameters =>
            {
                this.EnableCors();
                return HttpStatusCode.NoContent;
            };

            Options["dixon/{entity}/{fields}/{where}"] = parameters =>
            {
                this.EnableCors();
                return HttpStatusCode.NoContent;
            };

            Options["/dixon/update/{entity}/{where}"] = parameters =>
            {
                this.EnableCors();
                return HttpStatusCode.NoContent;
            };

            Options["/dixon/procedure/{storedProc}/"] = parameters =>
            {
                this.EnableCors();
                return HttpStatusCode.NoContent;
            };

            #endregion

        }

        #region FUNCTIONS
        
        
        private List<Dictionary<string, DixonPayload>> FormatPayload(List<object> entityResults)
        {
            List<Dictionary<string, DixonPayload>> payloadList = new List<Dictionary<string, DixonPayload>>();

            foreach (var tblResult in entityResults) //entity results has ALL the results from query which wil always be one object.
            {

                /* in here each tblResult or record gets processed into a string */
                string pattern = @"(?!/s)*(?<='),(?=\s[A-Za-z])";
                //string pattern2 = @"\s+([^=]+)\s+=\s+('.+?'|NULL)[,\}]";
                string testRes = tblResult.ToString();

                testRes = testRes.Replace("NULL", "'NULL'");

                //Match[] matches = Regex.Matches(testRes, pattern2).Cast<Match>().ToArray();

                string[] resTest = Regex.Split(testRes, pattern);

                Dictionary<string, DixonPayload> record = new Dictionary<string, DixonPayload>();
                foreach (string item in resTest) //go through each of the results one at a time, getting its translations
                {
                    /* in here the record gets broken up into columns to be translated
                     * AFTER translating in theory they should be just the one record e.g. tblResult
                     */
                    string line = item;
                    if (line.Length > 1)
                    {
                        DixonPayload payloadTemp = new DixonPayload();

                        if (line.Contains("{DapperRow"))
                        {
                            line = line.Replace("{DapperRow, ", "");
                        }

                        if (line.EndsWith("}"))
                        {
                            int indx = line.LastIndexOf("}");
                            line = line.Remove(indx);
                        }


                        #region Pre-process

                        string kvp = line;
                        string patternEq = @"(?<=[a-zA-Z0-9])\s=\s(?=')";
                        string[] kvpArr = Regex.Split(kvp, patternEq); //splits the key pair value of field name and value 

                        //payloadTemp.fieldName = kvpArr[0].ToString(); //field name of the result
                        string fieldName = kvpArr[0].ToString().Trim(); //field name of the result
                        payloadTemp.value = kvpArr[1].ToString(); //field val of the result
                        //payloadTemp.value = payloadTemp.value.Replace("'", ""); //THIS IS CAUSING ISSUES IF THERE IS MORE THAN ONE FIELD AND IT HAS THE " ' " 
                        payloadTemp.value = payloadTemp.value.Trim();

                        payloadTemp.value = payloadTemp.value.Trim();

                        Regex rgx = new Regex("[^a-zA-Z0-9 _]");
                        fieldName = rgx.Replace(fieldName, "");
                        fieldName = fieldName.Trim();
                        fieldName = fieldName.ToLower();

                        #endregion

                        #region Payload Cleanup

                        if (payloadTemp.value != null)
                        {
                            //gets rid of the single quote at the start
                            if (payloadTemp.value.StartsWith("'"))
                            {
                                int indx = payloadTemp.value.IndexOf("'");
                                payloadTemp.value = payloadTemp.value.Remove(indx, 1);
                            }

                            //gets rid of the single quote at the end
                            if (payloadTemp.value.EndsWith("'"))
                            {
                                int indx = payloadTemp.value.LastIndexOf("'");
                                payloadTemp.value = payloadTemp.value.Remove(indx, 1);
                            }

                        }

                        /* Changes returned values if its null - currently it returns NULL on both strings and int types
                            * This should only apply for int types and not for string values
                            */

                        if (payloadTemp.value == "NULL")
                        {
                            payloadTemp.value = "";
                        }
                        if (payloadTemp.value == "'NULL'")
                        {
                            payloadTemp.value = "NULL";
                        }


                        #endregion

                        //if ((payloadTemp.datatype != null) && (payloadTemp.datatype.ToLower() == "string"))
                        //{
                            payloadTemp.value = payloadTemp.value.TrimEnd();
                        //}

                        record.Add(fieldName, payloadTemp);

                    } // <-- end of if(item.Length > 1)


                } // <-- end of foreach(string item in res2)

                payloadList.Add(record);

            }// <-- end of foreach(var tblResult in entityResults)




            return payloadList;
        }

        private static List<ViewSchema> ProcessPayload(Dictionary<string, string> uiVals)
        {
            List<ViewSchema> values = new List<ViewSchema>();
            List<ViewSchema> schemaObj = new List<ViewSchema>();

            foreach (var item in uiVals)
            {
                ViewSchema field = new ViewSchema();

                if (item.Key.ToLower() != "sid")
                {
                    field.columnName = item.Key;
                    field.value = item.Value;
                    schemaObj.Add(field);
                }


            }


            return schemaObj;
        }

        private static string GetPrefix(string primaryKey)
        {
            //split the primaryKey into 2 on the "_"
            string prefix = "";
            string[] strArr = primaryKey.Split('_');

            prefix = strArr[0].Trim();

            return prefix;
        }

        private static string GetIds(List<object> resultsList)
        {
            string rawId = "";

            string id = "";

            foreach (var item in resultsList)
            {
                rawId = item.ToString();
                rawId = rawId.Replace("{DapperRow, ", " "); //need to separate the field name and teh value
                rawId = rawId.Replace("}", " ");
                rawId = rawId.Replace("'", " ");
                rawId = rawId.Trim();
            }
            string[] idArr = rawId.Split('=');

            id = idArr[1].Trim();

            return id;
        }

        private static string GetPrimaryKey(List<object> resultsList)
        {
            string rawId = "";

            string primaryKey = "";

            foreach (var item in resultsList)
            {
                rawId = item.ToString();
                rawId = rawId.Replace("{DapperRow, ", " "); //need to separate the field name and teh value
                rawId = rawId.Replace("}", " ");
                rawId = rawId.Replace("'", " ");
                rawId = rawId.Trim();
            }

            string[] idArr = rawId.Split('=');

            primaryKey = idArr[0].Trim();

            return primaryKey;
        }

        private static List<string> GetValues(List<string> payload)
        {
            List<string> tables = new List<string>();
            string x;
            //string[] items;
            string[] rawFields;
            int indx;
            string replace = "\":\"";

            foreach (var item in payload)
            {
                if (item.Length != 1)
                {
                    if (!item.Contains("SID"))
                    {
                        x = item.Replace(replace, "=").Trim();
                        x = x.Replace('"', ' ').Trim();



                        if ((x.EndsWith(",") == true))
                        {
                            indx = x.LastIndexOf(",");
                            x = x.Remove(indx);
                        }

                        rawFields = x.Split('=');
                        tables.Add(rawFields[1].Trim());
                    }

                }
            }

            return tables;
        }


        private static string FilterQuery(string query)
        {
            string[] items = query.Split('&');
            string result = "";
            string searchQuery, whereQuery;

            searchQuery = "SELECT ";
            whereQuery = "";
            char wildcard = '%';

            // HANDLES WHERE QUERY
            if (query.Contains("="))
            {
                foreach (string item in items)
                {
                    string[] fields = item.Split('=');

                    string field = fields[0];
                    string val = fields[1];
                    string value = val; // "'" + val + "'"; //check this one
                    string trueClause = "";
                    string fullquery = "";
                    string trueFullClause = "";

                    if (value.Contains("$"))
                    {
                        string[] likeClause = val.Split('$');

                        if (val.StartsWith("$"))
                        {
                            trueClause = wildcard + likeClause[1];
                        }

                        if (val.EndsWith("$"))
                        {
                            trueClause = likeClause[0] + wildcard;
                        }


                        if ((val.StartsWith("$")) && (val.EndsWith("$")))
                        {
                            trueClause = wildcard + likeClause[1] + wildcard;
                        }

                        if (trueClause.Contains('\''))
                        {
                            trueClause = trueClause.Replace("'", "''");
                        }

                        trueFullClause = trueFullClause + " LIKE '" + trueClause + "'";

                        fullquery = field + trueFullClause; //dont put ' = ' because it will screw up the damn thing
                    }
                    else if (value == "NULL")
                    {
                        fullquery = field + " is NULL";
                    }
                    else if (value == "!NULL")
                    {
                        fullquery = field + " is not NULL";
                    }

                    else //proceed as normal query
                    {
                        if (value.Contains('\''))
                        {
                            value = value.Replace("'", "''");
                        }


                        fullquery = field + " = '" + value + "'";



                    }

                    whereQuery = whereQuery + fullquery + " AND ";

                }

                if (whereQuery.EndsWith("AND "))
                {
                    string remove = "AND";

                    result = whereQuery.Substring(0, whereQuery.LastIndexOf(remove));

                }

            }
            //HANDLES SELECT QUERY
            else
            {
                foreach (string field in items)
                {
                    searchQuery = searchQuery + field + ", ";
                    //result = searchQuery;
                }

                if (searchQuery.EndsWith(", "))
                {
                    string remove = ", ";

                    result = searchQuery.Substring(0, searchQuery.LastIndexOf(remove));

                }

            }



            return result;
        }

        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using accsys.crmframework.api.infrastructure;
using Dapper;
using Nancy;
using System.IO;
using System.Data.Common;
using System.Data.SqlClient;
using System.Timers;
using System.Reflection;
using System.Configuration;

namespace accsys.crmframework.api
{
    public static class GlobalVariables
    {
        private static string _bar;
        private static Dictionary<String, List<Activity>> _activity;
        //private static Dictionary<String, List<Activity>> _activity2 = new Dictionary<string,List<Activity>>();
        private static Dictionary<String, List<Metadata>> _metadata;
        public static Dictionary<String, List<ViewSchema>> _viewSchema;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("GlobalVariables"); //THIS POINTS TO GV.txt
        public static string metadataVersion = DateTime.Now.ToString(); //metadataVersion 
        public static string appName = "";

        public static string Foo
        {
            get
            {
                return "foo";
            }
        }

        // read-write variable
        public static string Bar
        {
            get
            {
                return _bar;
            }
            set
            {
                _bar = value;
            }
        }

        //public static string MetadataVersion
        //{
        //    get
        //    {
        //        return DateTime.Now.ToString();
        //    }
        //    set
        //    {
        //        MetadataVersion = value;
        //    }
        //}


        public static Response AsError(this IResponseFormatter formatter, HttpStatusCode statusCode, string message)
        {
            return new Response
            {
                StatusCode = statusCode,
                ContentType = "text/plain",
                Contents = stream => (new StreamWriter(stream) { AutoFlush = true }).Write(message)
            };
        }

        public static void RefreshViews()
        {
            var connectionfactory = new ConnectionFactory();

            using (var con = connectionfactory.GetOpenConnection())
            {
                con.Execute("_spAFRefreshViews");
            }
        }

        public static string CheckCreateVersion()
        {
            var connectionFactory = new ConnectionFactory();
            string checkSQL = "select parm_value from custom_sysparams where parm_name = 'StillGotIdentityCols'";
            string primaryIdType = "";
            List<object> checkList = new List<object>();
            string res = "";

            using (var con = connectionFactory.GetOpenConnection())
            {
                checkList = con.Query(checkSQL).ToList();

                foreach (var val in checkList)
                {
                    primaryIdType = val.ToString();

                    primaryIdType = primaryIdType.Replace("{DapperRow, parm_value = ", " ");
                    res = primaryIdType.Replace("}", " ");
                    res = res.Replace("'", "");
                    res = res.Trim();
                }

                if (res == "Y") //vanilla
                {
                    return "PK"; //if it using the native primary keys of the table
                }
                else //from an upgrade
                {
                    return "SP"; //if it is using a stored procedure to create the IDs 
                }
            }


        }

        public static Dictionary<String, List<ViewSchema>> ViewSchema
        {
            get
            {
                if (_viewSchema == null)
                {
                    var connectionfactory = new ConnectionFactory();

                    using (var con = connectionfactory.GetOpenConnection())
                    {
                        string viewSchemaQuery = @"select 
                        distinct v.VIEW_NAME as [viewName],
                        v.TABLE_NAME as [tableName],
                        v.COLUMN_NAME as [columnName],
                        CASE
	                        WHEN ((k.ORDINAL_POSITION IS NULL) AND ([crmtable].Bord_IdField != v.COLUMN_NAME)) THEN 0
	                        WHEN ((k.ORDINAL_POSITION IS NOT NULL) OR ([crmtable].Bord_IdField = v.COLUMN_NAME)) THEN 1
	                        ELSE 0
	                        end
                        AS [isprimarykey]
                        from INFORMATION_SCHEMA.view_column_usage as v
                        left outer join INFORMATION_SCHEMA.KEY_COLUMN_USAGE as k on k.TABLE_SCHEMA = v.TABLE_SCHEMA and k.TABLE_CATALOG = v.TABLE_CATALOG and k.TABLE_NAME = v.TABLE_NAME and k.COLUMN_NAME = v.COLUMN_NAME
                        LEFT OUTER JOIN [dbo].[Custom_Tables] [crmtable] on [crmtable].[Bord_Name] = v.TABLE_NAME AND [Bord_Deleted] IS NULL";

                        var results = con.Query<ViewSchema>(viewSchemaQuery).GroupBy(b => b.viewName.ToLower()).ToDictionary(g => g.Key.ToLower(), g => g.ToList());

                        //.ToLookup(kvp => kvp.objectname, kvp => kvp.Value);;

                        _viewSchema = results;
                        return _viewSchema;
                    }

                }
                else
                {
                    return _viewSchema;
                }
            }
        }
        public static Dictionary<String, List<Activity>> Activity
        {
            get
            {

                if (_activity == null)
                {
                    var connectionfactory = new ConnectionFactory();

                    using (var con = connectionfactory.GetOpenConnection())
                    {

                        #region activityView

                        string activityView = @"SELECT
                                acty_sid AS [sid],
                                acty_userlogon AS [userLogon],
                                acty_login AS [loginTime],
                                user_fullname AS [userFullName],
                                user_department AS [userDepartment],
                                user_firstname AS [userFirstName],
                                user_lastname AS [userLastName],
                                user_userid AS [id],
                                user_per_admin AS [userPerAdmin]
                            FROM _vAFLoggedInUsers WHERE acty_sid IS NOT NULL";

                        #endregion

                        Dictionary<String, List<Activity>> listActivity = new Dictionary<string, List<Activity>>();

                        Console.WriteLine("LOADING USER ACTIVITY");
                        var userActivity = con.Query<Activity>(activityView).GroupBy(b => b.sid.ToLower().TrimEnd()).ToDictionary(g => g.Key.ToLower(), g => g.ToList());

                        listActivity = userActivity;

                        _activity = listActivity;
                    }
                }

                //_activity = GetUserMetadata(); //new way
                return _activity;
            }
        }
        public static Dictionary<String, List<Metadata>> Metadata
        {
            get
            {
                if (_metadata == null)
                {
                    var connectionfactory = new ConnectionFactory();


                    using (var con = connectionfactory.GetOpenConnection())
                    {
                        string getType = ConfigurationManager.AppSettings["MetadataGetType"];
                        string metadataQuery  = "";
                        if (getType == "v1")
                        {
                            //use the big query here 

                            #region ORIGINAL QUERY

                            metadataQuery = @"SELECT DISTINCT
                        --cu.TABLE_CATALOG as [database],
                        --cu.TABLE_SCHEMA as [schema],
                        cu.TABLE_NAME as [objectname],
                        c.COLUMN_NAME as [columnname],
                        CONVERT(int,c.ORDINAL_POSITION) as [position],
                        CASE 
                        WHEN c.Data_type IN ('char' , 'varchar', 'text', 'ntext','nvarchar', 'ntext', 'nchar') THEN 'String'
                        WHEN c.Data_type IN ('bit') THEN 'Boolean'
                        WHEN c.Data_type = 'tinyint' THEN 'int'
                        ELSE c.Data_type
                        END as [datatype],
                        CASE
	                        WHEN ((k.ORDINAL_POSITION IS NULL) AND ([crmtable].Bord_IdField != c.COLUMN_NAME)) THEN 0
	                        WHEN ((k.ORDINAL_POSITION IS NOT NULL) OR ([crmtable].Bord_IdField = c.COLUMN_NAME)) THEN 1
							ELSE 0
	                        end
                        AS [isprimarykey],

                        CASE 
	                        WHEN c.IS_NULLABLE = 'NO' THEN 0
	                        WHEN c.IS_NULLABLE = 'YES' THEN 1
	                        END AS [isnullable], 
                        CONVERT(int,c.CHARACTER_MAXIMUM_LENGTH) as [characterlength],
                        CONVERT(int,c.NUMERIC_PRECISION) as [numericprecision],
                        CONVERT(int,c.NUMERIC_SCALE) as [numericscale],
                        CONVERT(int,c.DATETIME_PRECISION) as [datetimeprecision],
						CAST(crmCaptions.Capt_US as nvarchar(max)) as [translation],
						CAST(crmCaptions2.capt_code as nvarchar(max)) as [captionCode],
						CAST(crmCaptions2.Capt_US as nvarchar(max)) as [translationValue]

                        FROM INFORMATION_SCHEMA.TABLES AS cu
                        INNER JOIN INFORMATION_SCHEMA.COLUMNS AS c ON c.TABLE_SCHEMA = cu.TABLE_SCHEMA AND c.TABLE_CATALOG = cu.TABLE_CATALOG AND c.TABLE_NAME = cu.TABLE_NAME
                        
						LEFT OUTER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS k on k.TABLE_SCHEMA = cu.TABLE_SCHEMA AND k.TABLE_CATALOG = cu.TABLE_CATALOG AND k.TABLE_NAME = cu.TABLE_NAME AND k.COLUMN_NAME = c.COLUMN_NAME
                        LEFT OUTER JOIN [dbo].[Custom_Tables] [crmtable] on [crmtable].[Bord_Name] = cu.TABLE_NAME AND [Bord_Deleted] IS NULL

						--LEFT OUTER JOIN [dbo].[Custom_Captions] [crmCaptions] on [crmCaptions].[Capt_Code] = c.COLUMN_NAME and [crmCaptions].[capt_family] != 'GenCaptions' 
                        LEFT OUTER JOIN [dbo].[Custom_Captions] [crmCaptions] on [crmCaptions].[Capt_Code] = c.COLUMN_NAME and [crmCaptions].[capt_family] = 'ColNames' and [crmCaptions].[Capt_FamilyType] = 'Tags' -- Added this to minimise the metadata load from 66590 to 51934 stock CRM db
						left outer join [dbo].[Custom_Captions] [crmCaptions2] on [crmCaptions2].[capt_family] = c.COLUMN_NAME 


						WHERE cu.TABLE_SCHEMA = 'dbo'
						--and cu.table_name NOT like 'v%'
						--and cu.table_name NOT like '_v%'
						--and cu.table_name like 'v%'
						--order by cu.table_name asc
						
						--and cu.table_name BETWEEN 'v1%' and 'vI%' --first part
						--and cu.table_name between 'vI%' and 'vQ%' --2nd lot
						--and cu.table_name between 'vQ%' and 'vZ%' --3rd set 



						
						--and cu.table_name like '_v%'";

                            #endregion

                            _metadata = con.Query<Metadata>(metadataQuery).GroupBy(b => b.objectname.ToLower()).ToDictionary(g => g.Key.ToLower(), g => g.ToList());
                        }
                        else if (getType == "dixon")
                        {
                            #region QUERY STRINGS

                            #region MQV1 - for Dixon

                                string metadataqueryView1 = @"SELECT DISTINCT
                            --cu.TABLE_CATALOG as [database],
                            --cu.TABLE_SCHEMA as [schema],
                            cu.TABLE_NAME as [objectname],
                            c.COLUMN_NAME as [columnname],
                            CONVERT(int,c.ORDINAL_POSITION) as [position],
                            CASE 
                            WHEN c.Data_type IN ('char' , 'varchar', 'text', 'ntext','nvarchar', 'ntext', 'nchar') THEN 'String'
                            WHEN c.Data_type IN ('bit') THEN 'Boolean'
                            WHEN c.Data_type = 'tinyint' THEN 'int'
                            ELSE c.Data_type
                            END as [datatype],
                            CASE
	                            WHEN ((k.ORDINAL_POSITION IS NULL) AND ([crmtable].Bord_IdField != c.COLUMN_NAME)) THEN 0
	                            WHEN ((k.ORDINAL_POSITION IS NOT NULL) OR ([crmtable].Bord_IdField = c.COLUMN_NAME)) THEN 1
							    ELSE 0
	                            end
                            AS [isprimarykey],

                            CASE 
	                            WHEN c.IS_NULLABLE = 'NO' THEN 0
	                            WHEN c.IS_NULLABLE = 'YES' THEN 1
	                            END AS [isnullable], 
                            CONVERT(int,c.CHARACTER_MAXIMUM_LENGTH) as [characterlength],
                            CONVERT(int,c.NUMERIC_PRECISION) as [numericprecision],
                            CONVERT(int,c.NUMERIC_SCALE) as [numericscale],
                            CONVERT(int,c.DATETIME_PRECISION) as [datetimeprecision],
						    CAST(crmCaptions.Capt_US as nvarchar(max)) as [translation],
						    CAST(crmCaptions2.capt_code as nvarchar(max)) as [captionCode],
						    CAST(crmCaptions2.Capt_US as nvarchar(max)) as [translationValue]

                            FROM INFORMATION_SCHEMA.TABLES AS cu
                            INNER JOIN INFORMATION_SCHEMA.COLUMNS AS c ON c.TABLE_SCHEMA = cu.TABLE_SCHEMA AND c.TABLE_CATALOG = cu.TABLE_CATALOG AND c.TABLE_NAME = cu.TABLE_NAME
                        
						    LEFT OUTER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS k on k.TABLE_SCHEMA = cu.TABLE_SCHEMA AND k.TABLE_CATALOG = cu.TABLE_CATALOG AND k.TABLE_NAME = cu.TABLE_NAME AND k.COLUMN_NAME = c.COLUMN_NAME
                            LEFT OUTER JOIN [dbo].[Custom_Tables] [crmtable] on [crmtable].[Bord_Name] = cu.TABLE_NAME AND [Bord_Deleted] IS NULL

						    LEFT OUTER JOIN [dbo].[Custom_Captions] [crmCaptions] on [crmCaptions].[Capt_Code] = c.COLUMN_NAME and [crmCaptions].[capt_family] != 'GenCaptions'
						    left outer join [dbo].[Custom_Captions] [crmCaptions2] on [crmCaptions2].[capt_family] = c.COLUMN_NAME 


						    WHERE cu.TABLE_SCHEMA = 'dbo'
                            and cu.table_name BETWEEN 'v1%' and 'vI%' --first part
                            ";
                            #endregion

                            #region MVQ2

                                string metadataqueryView2 = @"SELECT DISTINCT
                            --cu.TABLE_CATALOG as [database],
                            --cu.TABLE_SCHEMA as [schema],
                            cu.TABLE_NAME as [objectname],
                            c.COLUMN_NAME as [columnname],
                            CONVERT(int,c.ORDINAL_POSITION) as [position],
                            CASE 
                            WHEN c.Data_type IN ('char' , 'varchar', 'text', 'ntext','nvarchar', 'ntext', 'nchar') THEN 'String'
                            WHEN c.Data_type IN ('bit') THEN 'Boolean'
                            WHEN c.Data_type = 'tinyint' THEN 'int'
                            ELSE c.Data_type
                            END as [datatype],
                            CASE
	                            WHEN ((k.ORDINAL_POSITION IS NULL) AND ([crmtable].Bord_IdField != c.COLUMN_NAME)) THEN 0
	                            WHEN ((k.ORDINAL_POSITION IS NOT NULL) OR ([crmtable].Bord_IdField = c.COLUMN_NAME)) THEN 1
							    ELSE 0
	                            end
                            AS [isprimarykey],

                            CASE 
	                            WHEN c.IS_NULLABLE = 'NO' THEN 0
	                            WHEN c.IS_NULLABLE = 'YES' THEN 1
	                            END AS [isnullable], 
                            CONVERT(int,c.CHARACTER_MAXIMUM_LENGTH) as [characterlength],
                            CONVERT(int,c.NUMERIC_PRECISION) as [numericprecision],
                            CONVERT(int,c.NUMERIC_SCALE) as [numericscale],
                            CONVERT(int,c.DATETIME_PRECISION) as [datetimeprecision],
						    CAST(crmCaptions.Capt_US as nvarchar(max)) as [translation],
						    CAST(crmCaptions2.capt_code as nvarchar(max)) as [captionCode],
						    CAST(crmCaptions2.Capt_US as nvarchar(max)) as [translationValue]

                            FROM INFORMATION_SCHEMA.TABLES AS cu
                            INNER JOIN INFORMATION_SCHEMA.COLUMNS AS c ON c.TABLE_SCHEMA = cu.TABLE_SCHEMA AND c.TABLE_CATALOG = cu.TABLE_CATALOG AND c.TABLE_NAME = cu.TABLE_NAME
                        
						    LEFT OUTER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS k on k.TABLE_SCHEMA = cu.TABLE_SCHEMA AND k.TABLE_CATALOG = cu.TABLE_CATALOG AND k.TABLE_NAME = cu.TABLE_NAME AND k.COLUMN_NAME = c.COLUMN_NAME
                            LEFT OUTER JOIN [dbo].[Custom_Tables] [crmtable] on [crmtable].[Bord_Name] = cu.TABLE_NAME AND [Bord_Deleted] IS NULL

						    LEFT OUTER JOIN [dbo].[Custom_Captions] [crmCaptions] on [crmCaptions].[Capt_Code] = c.COLUMN_NAME and [crmCaptions].[capt_family] != 'GenCaptions'
						    left outer join [dbo].[Custom_Captions] [crmCaptions2] on [crmCaptions2].[capt_family] = c.COLUMN_NAME 


						    WHERE cu.TABLE_SCHEMA = 'dbo'
                            and cu.table_name BETWEEN 'vI%' and 'vQ%' --2nd part";

                            #endregion

                            #region MVQ3

                                string metadataqueryView3 = @"SELECT DISTINCT
                            --cu.TABLE_CATALOG as [database],
                            --cu.TABLE_SCHEMA as [schema],
                            cu.TABLE_NAME as [objectname],
                            c.COLUMN_NAME as [columnname],
                            CONVERT(int,c.ORDINAL_POSITION) as [position],
                            CASE 
                            WHEN c.Data_type IN ('char' , 'varchar', 'text', 'ntext','nvarchar', 'ntext', 'nchar') THEN 'String'
                            WHEN c.Data_type IN ('bit') THEN 'Boolean'
                            WHEN c.Data_type = 'tinyint' THEN 'int'
                            ELSE c.Data_type
                            END as [datatype],
                            CASE
	                            WHEN ((k.ORDINAL_POSITION IS NULL) AND ([crmtable].Bord_IdField != c.COLUMN_NAME)) THEN 0
	                            WHEN ((k.ORDINAL_POSITION IS NOT NULL) OR ([crmtable].Bord_IdField = c.COLUMN_NAME)) THEN 1
							    ELSE 0
	                            end
                            AS [isprimarykey],

                            CASE 
	                            WHEN c.IS_NULLABLE = 'NO' THEN 0
	                            WHEN c.IS_NULLABLE = 'YES' THEN 1
	                            END AS [isnullable], 
                            CONVERT(int,c.CHARACTER_MAXIMUM_LENGTH) as [characterlength],
                            CONVERT(int,c.NUMERIC_PRECISION) as [numericprecision],
                            CONVERT(int,c.NUMERIC_SCALE) as [numericscale],
                            CONVERT(int,c.DATETIME_PRECISION) as [datetimeprecision],
						    CAST(crmCaptions.Capt_US as nvarchar(max)) as [translation],
						    CAST(crmCaptions2.capt_code as nvarchar(max)) as [captionCode],
						    CAST(crmCaptions2.Capt_US as nvarchar(max)) as [translationValue]

                            FROM INFORMATION_SCHEMA.TABLES AS cu
                            INNER JOIN INFORMATION_SCHEMA.COLUMNS AS c ON c.TABLE_SCHEMA = cu.TABLE_SCHEMA AND c.TABLE_CATALOG = cu.TABLE_CATALOG AND c.TABLE_NAME = cu.TABLE_NAME
                        
						    LEFT OUTER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS k on k.TABLE_SCHEMA = cu.TABLE_SCHEMA AND k.TABLE_CATALOG = cu.TABLE_CATALOG AND k.TABLE_NAME = cu.TABLE_NAME AND k.COLUMN_NAME = c.COLUMN_NAME
                            LEFT OUTER JOIN [dbo].[Custom_Tables] [crmtable] on [crmtable].[Bord_Name] = cu.TABLE_NAME AND [Bord_Deleted] IS NULL

						    LEFT OUTER JOIN [dbo].[Custom_Captions] [crmCaptions] on [crmCaptions].[Capt_Code] = c.COLUMN_NAME and [crmCaptions].[capt_family] != 'GenCaptions'
						    left outer join [dbo].[Custom_Captions] [crmCaptions2] on [crmCaptions2].[capt_family] = c.COLUMN_NAME 


						    WHERE cu.TABLE_SCHEMA = 'dbo'
                            and cu.table_name BETWEEN 'vQ%' and 'vZ%' --2nd part";

                            #endregion

                            #region metadataVAF

                                string metadataVAF = @"SELECT DISTINCT
                            --cu.TABLE_CATALOG as [database],
                            --cu.TABLE_SCHEMA as [schema],
                            cu.TABLE_NAME as [objectname],
                            c.COLUMN_NAME as [columnname],
                            CONVERT(int,c.ORDINAL_POSITION) as [position],
                            CASE 
                            WHEN c.Data_type IN ('char' , 'varchar', 'text', 'ntext','nvarchar', 'ntext', 'nchar') THEN 'String'
                            WHEN c.Data_type IN ('bit') THEN 'Boolean'
                            WHEN c.Data_type = 'tinyint' THEN 'int'
                            ELSE c.Data_type
                            END as [datatype],
                            CASE
	                            WHEN ((k.ORDINAL_POSITION IS NULL) AND ([crmtable].Bord_IdField != c.COLUMN_NAME)) THEN 0
	                            WHEN ((k.ORDINAL_POSITION IS NOT NULL) OR ([crmtable].Bord_IdField = c.COLUMN_NAME)) THEN 1
							    ELSE 0
	                            end
                            AS [isprimarykey],

                            CASE 
	                            WHEN c.IS_NULLABLE = 'NO' THEN 0
	                            WHEN c.IS_NULLABLE = 'YES' THEN 1
	                            END AS [isnullable], 
                            CONVERT(int,c.CHARACTER_MAXIMUM_LENGTH) as [characterlength],
                            CONVERT(int,c.NUMERIC_PRECISION) as [numericprecision],
                            CONVERT(int,c.NUMERIC_SCALE) as [numericscale],
                            CONVERT(int,c.DATETIME_PRECISION) as [datetimeprecision],
						    CAST(crmCaptions.Capt_US as nvarchar(max)) as [translation],
						    CAST(crmCaptions2.capt_code as nvarchar(max)) as [captionCode],
						    CAST(crmCaptions2.Capt_US as nvarchar(max)) as [translationValue]

                            FROM INFORMATION_SCHEMA.TABLES AS cu
                            INNER JOIN INFORMATION_SCHEMA.COLUMNS AS c ON c.TABLE_SCHEMA = cu.TABLE_SCHEMA AND c.TABLE_CATALOG = cu.TABLE_CATALOG AND c.TABLE_NAME = cu.TABLE_NAME
                        
						    LEFT OUTER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS k on k.TABLE_SCHEMA = cu.TABLE_SCHEMA AND k.TABLE_CATALOG = cu.TABLE_CATALOG AND k.TABLE_NAME = cu.TABLE_NAME AND k.COLUMN_NAME = c.COLUMN_NAME
                            LEFT OUTER JOIN [dbo].[Custom_Tables] [crmtable] on [crmtable].[Bord_Name] = cu.TABLE_NAME AND [Bord_Deleted] IS NULL

						    LEFT OUTER JOIN [dbo].[Custom_Captions] [crmCaptions] on [crmCaptions].[Capt_Code] = c.COLUMN_NAME and [crmCaptions].[capt_family] != 'GenCaptions'
						    left outer join [dbo].[Custom_Captions] [crmCaptions2] on [crmCaptions2].[capt_family] = c.COLUMN_NAME 


						    WHERE cu.TABLE_SCHEMA = 'dbo'
						    and cu.table_name like '_vAF%'";

                            #endregion

                            #region metadataTables


                                string metadataTables = @"SELECT DISTINCT
                            --cu.TABLE_CATALOG as [database],
                            --cu.TABLE_SCHEMA as [schema],
                            cu.TABLE_NAME as [objectname],
                            c.COLUMN_NAME as [columnname],
                            CONVERT(int,c.ORDINAL_POSITION) as [position],
                            CASE 
                            WHEN c.Data_type IN ('char' , 'varchar', 'text', 'ntext','nvarchar', 'ntext', 'nchar') THEN 'String'
                            WHEN c.Data_type IN ('bit') THEN 'Boolean'
                            WHEN c.Data_type = 'tinyint' THEN 'int'
                            ELSE c.Data_type
                            END as [datatype],
                            CASE
	                            WHEN ((k.ORDINAL_POSITION IS NULL) AND ([crmtable].Bord_IdField != c.COLUMN_NAME)) THEN 0
	                            WHEN ((k.ORDINAL_POSITION IS NOT NULL) OR ([crmtable].Bord_IdField = c.COLUMN_NAME)) THEN 1
							    ELSE 0
	                            end
                            AS [isprimarykey],

                            CASE 
	                            WHEN c.IS_NULLABLE = 'NO' THEN 0
	                            WHEN c.IS_NULLABLE = 'YES' THEN 1
	                            END AS [isnullable], 
                            CONVERT(int,c.CHARACTER_MAXIMUM_LENGTH) as [characterlength],
                            CONVERT(int,c.NUMERIC_PRECISION) as [numericprecision],
                            CONVERT(int,c.NUMERIC_SCALE) as [numericscale],
                            CONVERT(int,c.DATETIME_PRECISION) as [datetimeprecision],
						    CAST(crmCaptions.Capt_US as nvarchar(max)) as [translation],
						    CAST(crmCaptions2.capt_code as nvarchar(max)) as [captionCode],
						    CAST(crmCaptions2.Capt_US as nvarchar(max)) as [translationValue]

                            FROM INFORMATION_SCHEMA.TABLES AS cu
                            INNER JOIN INFORMATION_SCHEMA.COLUMNS AS c ON c.TABLE_SCHEMA = cu.TABLE_SCHEMA AND c.TABLE_CATALOG = cu.TABLE_CATALOG AND c.TABLE_NAME = cu.TABLE_NAME
                        
						    LEFT OUTER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS k on k.TABLE_SCHEMA = cu.TABLE_SCHEMA AND k.TABLE_CATALOG = cu.TABLE_CATALOG AND k.TABLE_NAME = cu.TABLE_NAME AND k.COLUMN_NAME = c.COLUMN_NAME
                            LEFT OUTER JOIN [dbo].[Custom_Tables] [crmtable] on [crmtable].[Bord_Name] = cu.TABLE_NAME AND [Bord_Deleted] IS NULL

						    LEFT OUTER JOIN [dbo].[Custom_Captions] [crmCaptions] on [crmCaptions].[Capt_Code] = c.COLUMN_NAME and [crmCaptions].[capt_family] != 'GenCaptions'
						    left outer join [dbo].[Custom_Captions] [crmCaptions2] on [crmCaptions2].[capt_family] = c.COLUMN_NAME 


						    WHERE cu.TABLE_SCHEMA = 'dbo'
						    and cu.table_name NOT like 'v%'
						    and cu.table_name NOT like '_v%'";

                            #endregion

                            #endregion

                            #region EXECUTIONS - THIS AREA IS USED FOR DIXON BECAUSE OF THE SHEER SIZE OF THE DATABASE

                                Dictionary<String, List<Metadata>> listM = new Dictionary<string, List<Metadata>>();
                                //Dictionary<String, HashSet<Metadata>> listM = new Dictionary<string, List<Metadata>>();

                                Console.WriteLine("LOADING AF VIEWS");

                                var resAF = con.Query<Metadata>(metadataVAF).GroupBy(b => b.objectname.ToLower()).ToDictionary(g => g.Key.ToLower(), g => g.ToList());


                                //listM = con.Query<Metadata>(metadataVAF).GroupBy(b => b.objectname.ToLower()).ToDictionary(g => g.Key.ToLower(), g => g.ToList());

                                foreach (var valAF in resAF)
                                {
                                    listM.Add(valAF.Key, valAF.Value);
                                }

                                Console.WriteLine("LOADING SAGE CRM VIEWS");

                                var res1 = con.Query<Metadata>(metadataqueryView1).GroupBy(b => b.objectname.ToLower()).ToDictionary(g => g.Key.ToLower(), g => g.ToList());

                                foreach (var val1 in res1)
                                {
                                    listM.Add(val1.Key, val1.Value);
                                }

                                var res2 = con.Query<Metadata>(metadataqueryView2).GroupBy(b => b.objectname.ToLower()).ToDictionary(g => g.Key.ToLower(), g => g.ToList());

                                foreach (var val2 in res2)
                                {
                                    listM.Add(val2.Key, val2.Value);

                                }


                                var res3 = con.Query<Metadata>(metadataqueryView3).GroupBy(b => b.objectname.ToLower()).ToDictionary(g => g.Key.ToLower(), g => g.ToList());

                                foreach (var val3 in res3)
                                {
                                    listM.Add(val3.Key, val3.Value);
                                }

                                var res4 = con.Query<Metadata>(metadataTables).GroupBy(b => b.objectname.ToLower()).ToDictionary(g => g.Key.ToLower(), g => g.ToList());

                                Console.WriteLine("LOADING SAGE CRM TABLES");

                                foreach (var val4 in res4)
                                {
                                    listM.Add(val4.Key, val4.Value);
                                }
                        
                                _metadata = listM;

                            #endregion
                        }

                        else if (getType == "v2")
                        {
                            //assumptions - views are created with a prefix of a lowercase 'v' followed by an uppercase character e.g. vCompanyPE
                            //string alphabet = "ABCDEFGHIJKLMNOPQRSTUWXYZ"; //Took out 'V' because it started counting the views as well 
                            Dictionary<String, List<Metadata>> metadataList = new Dictionary<string, List<Metadata>>();

                            //_metadata = metadataList;

                            _metadata = RefreshMetadata("all");
                        }
                        else if (getType == "NonCRM")
                        {
                            #region NON CRM QUERY

                            metadataQuery = @"SELECT DISTINCT
                                                cu.TABLE_NAME as [objectname],
                                                c.COLUMN_NAME as [columnname],
                                                CONVERT(int,c.ORDINAL_POSITION) as [position],
                                                CASE 
                                                WHEN c.Data_type IN ('char' , 'varchar', 'text', 'ntext','nvarchar', 'ntext', 'nchar') THEN 'String'
                                                WHEN c.Data_type IN ('bit') THEN 'Boolean'
                                                WHEN c.Data_type = 'tinyint' THEN 'int'
                                                ELSE c.Data_type
                                                END as [datatype],
                                                CASE
	                                                WHEN ((k.ORDINAL_POSITION IS NULL)) THEN 0
	                                                WHEN ((k.ORDINAL_POSITION IS NOT NULL))  THEN 1
	                                                ELSE 0
	                                                end
                                                AS [isprimarykey],

                                                CASE 
	                                                WHEN c.IS_NULLABLE = 'NO' THEN 0
	                                                WHEN c.IS_NULLABLE = 'YES' THEN 1
	                                                END AS [isnullable], 
                                                CONVERT(int,c.CHARACTER_MAXIMUM_LENGTH) as [characterlength],
                                                CONVERT(int,c.NUMERIC_PRECISION) as [numericprecision],
                                                CONVERT(int,c.NUMERIC_SCALE) as [numericscale],
                                                CONVERT(int,c.DATETIME_PRECISION) as [datetimeprecision]
                                                FROM INFORMATION_SCHEMA.TABLES AS cu
                                                INNER JOIN INFORMATION_SCHEMA.COLUMNS AS c ON c.TABLE_SCHEMA = cu.TABLE_SCHEMA AND c.TABLE_CATALOG = cu.TABLE_CATALOG AND c.TABLE_NAME = cu.TABLE_NAME
                                                LEFT OUTER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS k on k.TABLE_SCHEMA = cu.TABLE_SCHEMA AND k.TABLE_CATALOG = cu.TABLE_CATALOG AND k.TABLE_NAME = cu.TABLE_NAME AND k.COLUMN_NAME = c.COLUMN_NAME
                                                WHERE cu.TABLE_SCHEMA = 'dbo'";



                            #endregion

                            _metadata = con.Query<Metadata>(metadataQuery).GroupBy(b => b.objectname.ToLower()).ToDictionary(g => g.Key.ToLower(), g => g.ToList());
                        }




                        /* ORIGINAL QUERY */

                        //var results = con.Query<Metadata>(metadataQuery).GroupBy(b => b.objectname.ToLower()).ToDictionary(g => g.Key.ToLower(), g => g.ToList());

                        //_metadata = results;

                        //_metadata = con.Query<Metadata>(metadataQuery).GroupBy(b => b.objectname.ToLower()).ToDictionary(g => g.Key.ToLower(), g => g.ToList());

                        return _metadata;
                    }
                }
                else
                {
                    return _metadata;

                }

            }
        }

        /* PURPOSE: This is to Reload the metadata given an entityname
         * INPUTS: entityname
         */

        public static string ReloadMetadata(Dictionary<String, List<Metadata>> newMetadata)
        {
            string finalMsg = "";
            try
            {
                foreach (var metadataItem in newMetadata)
                {
                    if (Metadata.ContainsKey(metadataItem.Key))
                    {
                        Metadata[metadataItem.Key] = metadataItem.Value;
                        finalMsg = "Updated Metadata Cache: " + metadataItem.Key;
                        log.Info(finalMsg);
                    }
                    else
                    {
                        Metadata.Add(metadataItem.Key, metadataItem.Value);
                        finalMsg = "Added New Metadata Cache: " + metadataItem.Key;
                        log.Info(finalMsg);
                    }
                }

                newMetadata.Clear(); //hopefully disposes the object -- to a degree it does [MEMLEAK]

            }
            catch (Exception ex)
            {
                string msg = ex.Message + " .Reload Metadata Failed. ENTITY: ";
                log.Error(finalMsg);
            }

            return finalMsg;
        }

        /* PURPOSE: This is to Reload the metadata given an entityname
         * INPUTS: entityname
         */

        public static string ReloadMetadata(string entityName)
        {
            #region RELOAD QUERY

            string metadataQuery = @"SELECT DISTINCT
                        --cu.TABLE_CATALOG as [database],
                        --cu.TABLE_SCHEMA as [schema],
                        cu.TABLE_NAME as [objectname],
                        c.COLUMN_NAME as [columnname],
                        CONVERT(int,c.ORDINAL_POSITION) as [position],
                        CASE 
                        WHEN c.Data_type IN ('char' , 'varchar', 'text', 'ntext','nvarchar', 'ntext', 'nchar') THEN 'String'
                        WHEN c.Data_type IN ('bit') THEN 'Boolean'
                        WHEN c.Data_type = 'tinyint' THEN 'int'
                        ELSE c.Data_type
                        END as [datatype],
                       CASE
                               WHEN ((k .ORDINAL_POSITION IS NULL) AND ([crmtable]. Bord_IdField != c. COLUMN_NAME)) THEN 0
                               --WHEN ((k.ORDINAL_POSITION IS NOT NULL) OR ([crmtable].Bord_IdField = c.COLUMN_NAME)) THEN 1 -- pete's original
                                                 WHEN ( (tc. CONSTRAINT_TYPE = 'PRIMARY KEY') AND ( tc.CONSTRAINT_NAME = k.CONSTRAINT_NAME )
                                                        --OR ([crmtable].Bord_IdField = c.COLUMN_NAME)
                                                        )THEN 1
                                                 ELSE 0
                               end
                        AS [isprimarykey] ,
                        CASE 
	                        WHEN c.IS_NULLABLE = 'NO' THEN 0
	                        WHEN c.IS_NULLABLE = 'YES' THEN 1
	                        END AS [isnullable], 
                        CONVERT(int,c.CHARACTER_MAXIMUM_LENGTH) as [characterlength],
                        CONVERT(int,c.NUMERIC_PRECISION) as [numericprecision],
                        CONVERT(int,c.NUMERIC_SCALE) as [numericscale],
                        CONVERT(int,c.DATETIME_PRECISION) as [datetimeprecision],
						CAST(crmCaptions.Capt_US as nvarchar(max)) as [translation],
						CAST(crmCaptions2.capt_code as nvarchar(max)) as [captionCode],
						CAST(crmCaptions2.Capt_US as nvarchar(max)) as [translationValue]

                        FROM INFORMATION_SCHEMA.TABLES AS cu
                        INNER JOIN INFORMATION_SCHEMA.COLUMNS AS c ON c.TABLE_SCHEMA = cu.TABLE_SCHEMA AND c.TABLE_CATALOG = cu.TABLE_CATALOG AND c.TABLE_NAME = cu.TABLE_NAME
                        
						LEFT OUTER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS k on k.TABLE_SCHEMA = cu.TABLE_SCHEMA AND k.TABLE_CATALOG = cu.TABLE_CATALOG AND k.TABLE_NAME = cu.TABLE_NAME AND k.COLUMN_NAME = c.COLUMN_NAME
                        LEFT OUTER JOIN [dbo].[Custom_Tables] [crmtable] on [crmtable].[Bord_Name] = cu.TABLE_NAME AND [Bord_Deleted] IS NULL

						--LEFT OUTER JOIN [dbo].[Custom_Captions] [crmCaptions] on [crmCaptions].[Capt_Code] = c.COLUMN_NAME and [crmCaptions].[capt_family] != 'GenCaptions' 
                        LEFT OUTER JOIN [dbo].[Custom_Captions] [crmCaptions] on [crmCaptions].[Capt_Code] = c.COLUMN_NAME and [crmCaptions].[capt_family] = 'ColNames' and [crmCaptions].[Capt_FamilyType] = 'Tags' -- Added this to minimise the metadata load from 66590 to 51934 stock CRM db
						left outer join [dbo].[Custom_Captions] [crmCaptions2] on [crmCaptions2].[capt_family] = c.COLUMN_NAME 


						WHERE cu.TABLE_SCHEMA = 'dbo'
						and crmCaptions2.Capt_Deleted is null
						and crmCaptions.Capt_Deleted is null
						and cu.table_name = '" + entityName + "'";
            #endregion

            Dictionary<String, List<Metadata>> newMetadata = new Dictionary<String, List<Metadata>>();
            var connectionFactory = new ConnectionFactory();
            string finalMsg = "";
            try
            {
                using (var con = connectionFactory.GetOpenConnection())
                {
                    //the code below is responsible of querying the database to get the new metadata
                    newMetadata = con.Query<Metadata>(metadataQuery).GroupBy(b => b.objectname.ToLower()).ToDictionary(g => g.Key.ToLower(), g => g.ToList());
                }

                foreach (var metadataItem in newMetadata)
                {
                    if (Metadata.ContainsKey(metadataItem.Key))
                    {
                        Metadata[metadataItem.Key] = metadataItem.Value;
                        finalMsg = "Updated Metadata Cache: " + metadataItem.Key;
                        log.Info(finalMsg);
                    }
                    else
                    {
                        Metadata.Add(metadataItem.Key, metadataItem.Value);
                        finalMsg = "Added New Metadata Cache: " + metadataItem.Key;
                        log.Info(finalMsg);
                    }
                }
            }
            catch(Exception ex)
            {
                string msg = ex.Message + " .Reload Metadata Failed. ENTITY: ";
                finalMsg = msg + entityName; 
                log.Error(finalMsg);
                //finalMsg = "fail";
            }

            return finalMsg;
        }

        /* PURPOSE: This is to Refresh metadata in chunks e.g. tables, views
         * INPUTS: cmd is either all, crmTables, crmViews, afTables, afViews
         */
        public static Dictionary<string, List<Metadata>> RefreshMetadata(string cmd)
        {
            Dictionary<String, List<Metadata>> metadataList = new Dictionary<String, List<Metadata>>();

            var connectionfactory = new ConnectionFactory();
            string alphabet = "ABCDEFGHIJKLMNOPQRSTUWXYZ"; //Took out 'V' because it started counting the views as well 
            string metadataQuery = "";
            using (var con = connectionfactory.GetOpenConnection())
            {
                switch(cmd)
                {
                    case "all":
                        {
                            #region SAGE CRM TABLES
                            Console.WriteLine("LOADING SAGE CRM TABLES...");
                            foreach (char c in alphabet)
                            {
                                #region Query

                                metadataQuery = @"SELECT DISTINCT
                            --cu.TABLE_CATALOG as [database],
                            --cu.TABLE_SCHEMA as [schema],
                            cu.TABLE_NAME as [objectname],
                            c.COLUMN_NAME as [columnname],
                            CONVERT(int,c.ORDINAL_POSITION) as [position],
                            CASE 
                            WHEN c.Data_type IN ('char' , 'varchar', 'text', 'ntext','nvarchar', 'ntext', 'nchar') THEN 'String'
                            WHEN c.Data_type IN ('bit') THEN 'Boolean'
                            WHEN c.Data_type = 'tinyint' THEN 'int'
                            ELSE c.Data_type
                            END as [datatype],
                            CASE
                               WHEN ((k .ORDINAL_POSITION IS NULL) AND ([crmtable]. Bord_IdField != c. COLUMN_NAME)) THEN 0
                               --WHEN ((k.ORDINAL_POSITION IS NOT NULL) OR ([crmtable].Bord_IdField = c.COLUMN_NAME)) THEN 1 -- pete's original
                                                 WHEN ( (tc. CONSTRAINT_TYPE = 'PRIMARY KEY') AND ( tc.CONSTRAINT_NAME = k.CONSTRAINT_NAME )
                                                        --OR ([crmtable].Bord_IdField = c.COLUMN_NAME)
                                                        )THEN 1
                                                 ELSE 0
                               end
                            AS [isprimarykey] ,



                            CASE 
	                            WHEN c.IS_NULLABLE = 'NO' THEN 0
	                            WHEN c.IS_NULLABLE = 'YES' THEN 1
	                            END AS [isnullable], 
                            CONVERT(int,c.CHARACTER_MAXIMUM_LENGTH) as [characterlength],
                            CONVERT(int,c.NUMERIC_PRECISION) as [numericprecision],
                            CONVERT(int,c.NUMERIC_SCALE) as [numericscale],
                            CONVERT(int,c.DATETIME_PRECISION) as [datetimeprecision],
						    CAST(crmCaptions.Capt_US as nvarchar(max)) as [translation],
						    CAST(crmCaptions2.capt_code as nvarchar(max)) as [captionCode],
						    CAST(crmCaptions2.Capt_US as nvarchar(max)) as [translationValue]

                            FROM INFORMATION_SCHEMA.TABLES AS cu
                            INNER JOIN INFORMATION_SCHEMA.COLUMNS AS c ON c.TABLE_SCHEMA = cu.TABLE_SCHEMA AND c.TABLE_CATALOG = cu.TABLE_CATALOG AND c.TABLE_NAME = cu.TABLE_NAME
                        
						    LEFT OUTER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS k on k.TABLE_SCHEMA = cu.TABLE_SCHEMA AND k.TABLE_CATALOG = cu.TABLE_CATALOG AND k.TABLE_NAME = cu.TABLE_NAME AND k.COLUMN_NAME = c.COLUMN_NAME
                            LEFT OUTER JOIN [dbo].[Custom_Tables] [crmtable] on [crmtable].[Bord_Name] = cu.TABLE_NAME AND [Bord_Deleted] IS NULL

                            --LEFT OUTER JOIN [dbo].[Custom_Captions] [crmCaptions] on [crmCaptions].[Capt_Code] = c.COLUMN_NAME and [crmCaptions].[capt_family] != 'GenCaptions' 
                            LEFT OUTER JOIN [dbo].[Custom_Captions] [crmCaptions] on [crmCaptions].[Capt_Code] = c.COLUMN_NAME and [crmCaptions].[capt_family] = 'ColNames' and [crmCaptions].[Capt_FamilyType] = 'Tags' -- Added this to minimise the metadata load from 66590 to 51934 stock CRM db
						    left outer join [dbo].[Custom_Captions] [crmCaptions2] on [crmCaptions2].[capt_family] = c.COLUMN_NAME 

                            LEFT OUTER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc on tc .CONSTRAINT_NAME = k .CONSTRAINT_NAME -- this should provide the correct primary keys
						    WHERE cu.TABLE_SCHEMA = 'dbo'
							and crmCaptions2.Capt_Deleted is null
							and crmCaptions.Capt_Deleted is null
                            and cu.table_name LIKE '" + c + "%'";

                                #endregion

                                var metadataQueryResults = con.Query<Metadata>(metadataQuery).GroupBy(b => b.objectname.ToLower()).ToDictionary(g => g.Key.ToLower(), g => g.ToList());

                                foreach (var result in metadataQueryResults)
                                {
                                    if (!metadataList.ContainsKey(result.Key))
                                    {
                                        metadataList.Add(result.Key, result.Value);
                                    }
                                }
                            }

                            #endregion

                            #region SAGE CRM VIEWS

                            Console.WriteLine("LOADING SAGE CRM VIEWS...");
                            foreach (char c in alphabet)
                            {
                                #region Query

                                metadataQuery = @"SELECT DISTINCT
                            --cu.TABLE_CATALOG as [database],
                            --cu.TABLE_SCHEMA as [schema],
                            cu.TABLE_NAME as [objectname],
                            c.COLUMN_NAME as [columnname],
                            CONVERT(int,c.ORDINAL_POSITION) as [position],
                            CASE 
                            WHEN c.Data_type IN ('char' , 'varchar', 'text', 'ntext','nvarchar', 'ntext', 'nchar') THEN 'String'
                            WHEN c.Data_type IN ('bit') THEN 'Boolean'
                            WHEN c.Data_type = 'tinyint' THEN 'int'
                            ELSE c.Data_type
                            END as [datatype],
                            CASE
                               WHEN ((k .ORDINAL_POSITION IS NULL) AND ([crmtable]. Bord_IdField != c. COLUMN_NAME)) THEN 0
                               --WHEN ((k.ORDINAL_POSITION IS NOT NULL) OR ([crmtable].Bord_IdField = c.COLUMN_NAME)) THEN 1 -- pete's original
                                                 WHEN ( (tc. CONSTRAINT_TYPE = 'PRIMARY KEY') AND ( tc.CONSTRAINT_NAME = k.CONSTRAINT_NAME )
                                                        --OR ([crmtable].Bord_IdField = c.COLUMN_NAME)
                                                        )THEN 1
                                                 ELSE 0
                               end
                            AS [isprimarykey] ,

                            CASE 
	                            WHEN c.IS_NULLABLE = 'NO' THEN 0
	                            WHEN c.IS_NULLABLE = 'YES' THEN 1
	                            END AS [isnullable], 
                            CONVERT(int,c.CHARACTER_MAXIMUM_LENGTH) as [characterlength],
                            CONVERT(int,c.NUMERIC_PRECISION) as [numericprecision],
                            CONVERT(int,c.NUMERIC_SCALE) as [numericscale],
                            CONVERT(int,c.DATETIME_PRECISION) as [datetimeprecision],
						    CAST(crmCaptions.Capt_US as nvarchar(max)) as [translation],
						    CAST(crmCaptions2.capt_code as nvarchar(max)) as [captionCode],
						    CAST(crmCaptions2.Capt_US as nvarchar(max)) as [translationValue]

                            FROM INFORMATION_SCHEMA.TABLES AS cu
                            INNER JOIN INFORMATION_SCHEMA.COLUMNS AS c ON c.TABLE_SCHEMA = cu.TABLE_SCHEMA AND c.TABLE_CATALOG = cu.TABLE_CATALOG AND c.TABLE_NAME = cu.TABLE_NAME
                        
						    LEFT OUTER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS k on k.TABLE_SCHEMA = cu.TABLE_SCHEMA AND k.TABLE_CATALOG = cu.TABLE_CATALOG AND k.TABLE_NAME = cu.TABLE_NAME AND k.COLUMN_NAME = c.COLUMN_NAME
                            LEFT OUTER JOIN [dbo].[Custom_Tables] [crmtable] on [crmtable].[Bord_Name] = cu.TABLE_NAME AND [Bord_Deleted] IS NULL

						    --LEFT OUTER JOIN [dbo].[Custom_Captions] [crmCaptions] on [crmCaptions].[Capt_Code] = c.COLUMN_NAME and [crmCaptions].[capt_family] != 'GenCaptions' 
                            LEFT OUTER JOIN [dbo].[Custom_Captions] [crmCaptions] on [crmCaptions].[Capt_Code] = c.COLUMN_NAME and [crmCaptions].[capt_family] = 'ColNames' and [crmCaptions].[Capt_FamilyType] = 'Tags' -- Added this to minimise the metadata load from 66590 to 51934 stock CRM db
						    left outer join [dbo].[Custom_Captions] [crmCaptions2] on [crmCaptions2].[capt_family] = c.COLUMN_NAME 

                            LEFT OUTER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc on tc .CONSTRAINT_NAME = k .CONSTRAINT_NAME -- this should provide the correct primary keys
						    WHERE cu.TABLE_SCHEMA = 'dbo'
							and crmCaptions2.Capt_Deleted is null
							and crmCaptions.Capt_Deleted is null
                            and cu.table_name LIKE 'v" + c + "%'";

                                #endregion

                                var metadataQueryResults = con.Query<Metadata>(metadataQuery).GroupBy(b => b.objectname.ToLower()).ToDictionary(g => g.Key.ToLower(), g => g.ToList());

                                foreach (var result in metadataQueryResults)
                                {
                                    if (!metadataList.ContainsKey(result.Key))
                                    {
                                        metadataList.Add(result.Key, result.Value);
                                    }
                                }
                            }

                            #endregion

                            #region AF TABLES
                            Console.WriteLine("LOADING AF TABLES...");
                            foreach (char c in alphabet)
                            {
                                #region Query

                                metadataQuery = @"SELECT DISTINCT
                            --cu.TABLE_CATALOG as [database],
                            --cu.TABLE_SCHEMA as [schema],
                            cu.TABLE_NAME as [objectname],
                            c.COLUMN_NAME as [columnname],
                            CONVERT(int,c.ORDINAL_POSITION) as [position],
                            CASE 
                            WHEN c.Data_type IN ('char' , 'varchar', 'text', 'ntext','nvarchar', 'ntext', 'nchar') THEN 'String'
                            WHEN c.Data_type IN ('bit') THEN 'Boolean'
                            WHEN c.Data_type = 'tinyint' THEN 'int'
                            ELSE c.Data_type
                            END as [datatype],
                            CASE
                               WHEN ((k .ORDINAL_POSITION IS NULL) AND ([crmtable]. Bord_IdField != c. COLUMN_NAME)) THEN 0
                               --WHEN ((k.ORDINAL_POSITION IS NOT NULL) OR ([crmtable].Bord_IdField = c.COLUMN_NAME)) THEN 1 -- pete's original
                                                 WHEN ( (tc. CONSTRAINT_TYPE = 'PRIMARY KEY') AND ( tc.CONSTRAINT_NAME = k.CONSTRAINT_NAME )
                                                        --OR ([crmtable].Bord_IdField = c.COLUMN_NAME)
                                                        )THEN 1
                                                 ELSE 0
                               end
                            AS [isprimarykey] ,

                            CASE 
	                            WHEN c.IS_NULLABLE = 'NO' THEN 0
	                            WHEN c.IS_NULLABLE = 'YES' THEN 1
	                            END AS [isnullable], 
                            CONVERT(int,c.CHARACTER_MAXIMUM_LENGTH) as [characterlength],
                            CONVERT(int,c.NUMERIC_PRECISION) as [numericprecision],
                            CONVERT(int,c.NUMERIC_SCALE) as [numericscale],
                            CONVERT(int,c.DATETIME_PRECISION) as [datetimeprecision],
						    CAST(crmCaptions.Capt_US as nvarchar(max)) as [translation],
						    CAST(crmCaptions2.capt_code as nvarchar(max)) as [captionCode],
						    CAST(crmCaptions2.Capt_US as nvarchar(max)) as [translationValue]

                            FROM INFORMATION_SCHEMA.TABLES AS cu
                            INNER JOIN INFORMATION_SCHEMA.COLUMNS AS c ON c.TABLE_SCHEMA = cu.TABLE_SCHEMA AND c.TABLE_CATALOG = cu.TABLE_CATALOG AND c.TABLE_NAME = cu.TABLE_NAME
                        
						    LEFT OUTER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS k on k.TABLE_SCHEMA = cu.TABLE_SCHEMA AND k.TABLE_CATALOG = cu.TABLE_CATALOG AND k.TABLE_NAME = cu.TABLE_NAME AND k.COLUMN_NAME = c.COLUMN_NAME
                            LEFT OUTER JOIN [dbo].[Custom_Tables] [crmtable] on [crmtable].[Bord_Name] = cu.TABLE_NAME AND [Bord_Deleted] IS NULL

						    --LEFT OUTER JOIN [dbo].[Custom_Captions] [crmCaptions] on [crmCaptions].[Capt_Code] = c.COLUMN_NAME and [crmCaptions].[capt_family] != 'GenCaptions' 
                            LEFT OUTER JOIN [dbo].[Custom_Captions] [crmCaptions] on [crmCaptions].[Capt_Code] = c.COLUMN_NAME and [crmCaptions].[capt_family] = 'ColNames' and [crmCaptions].[Capt_FamilyType] = 'Tags' -- Added this to minimise the metadata load from 66590 to 51934 stock CRM db
						    left outer join [dbo].[Custom_Captions] [crmCaptions2] on [crmCaptions2].[capt_family] = c.COLUMN_NAME 

                            LEFT OUTER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc on tc .CONSTRAINT_NAME = k .CONSTRAINT_NAME -- this should provide the correct primary keys
						    WHERE cu.TABLE_SCHEMA = 'dbo'
							and crmCaptions2.Capt_Deleted is null
							and crmCaptions.Capt_Deleted is null
                            and cu.table_name LIKE '_t" + c + "%'";

                                #endregion

                                var metadataQueryResults = con.Query<Metadata>(metadataQuery).GroupBy(b => b.objectname.ToLower()).ToDictionary(g => g.Key.ToLower(), g => g.ToList());

                                foreach (var result in metadataQueryResults)
                                {
                                    if (!metadataList.ContainsKey(result.Key))
                                    {
                                        metadataList.Add(result.Key, result.Value);
                                    }
                                }
                            }

                            #endregion

                            #region AF VIEWS
                            Console.WriteLine("LOADING AF VIEWS...");
                            foreach (char c in alphabet)
                            {
                                #region Query

                                metadataQuery = @"SELECT DISTINCT
                            --cu.TABLE_CATALOG as [database],
                            --cu.TABLE_SCHEMA as [schema],
                            cu.TABLE_NAME as [objectname],
                            c.COLUMN_NAME as [columnname],
                            CONVERT(int,c.ORDINAL_POSITION) as [position],
                            CASE 
                            WHEN c.Data_type IN ('char' , 'varchar', 'text', 'ntext','nvarchar', 'ntext', 'nchar') THEN 'String'
                            WHEN c.Data_type IN ('bit') THEN 'Boolean'
                            WHEN c.Data_type = 'tinyint' THEN 'int'
                            ELSE c.Data_type
                            END as [datatype],
                            CASE
                               WHEN ((k .ORDINAL_POSITION IS NULL) AND ([crmtable]. Bord_IdField != c. COLUMN_NAME)) THEN 0
                               --WHEN ((k.ORDINAL_POSITION IS NOT NULL) OR ([crmtable].Bord_IdField = c.COLUMN_NAME)) THEN 1 -- pete's original
                                                 WHEN ( (tc. CONSTRAINT_TYPE = 'PRIMARY KEY') AND ( tc.CONSTRAINT_NAME = k.CONSTRAINT_NAME )
                                                        --OR ([crmtable].Bord_IdField = c.COLUMN_NAME)
                                                        )THEN 1
                                                 ELSE 0
                               end
                            AS [isprimarykey] ,

                            CASE 
	                            WHEN c.IS_NULLABLE = 'NO' THEN 0
	                            WHEN c.IS_NULLABLE = 'YES' THEN 1
	                            END AS [isnullable], 
                            CONVERT(int,c.CHARACTER_MAXIMUM_LENGTH) as [characterlength],
                            CONVERT(int,c.NUMERIC_PRECISION) as [numericprecision],
                            CONVERT(int,c.NUMERIC_SCALE) as [numericscale],
                            CONVERT(int,c.DATETIME_PRECISION) as [datetimeprecision],
						    CAST(crmCaptions.Capt_US as nvarchar(max)) as [translation],
						    CAST(crmCaptions2.capt_code as nvarchar(max)) as [captionCode],
						    CAST(crmCaptions2.Capt_US as nvarchar(max)) as [translationValue]

                            FROM INFORMATION_SCHEMA.TABLES AS cu
                            INNER JOIN INFORMATION_SCHEMA.COLUMNS AS c ON c.TABLE_SCHEMA = cu.TABLE_SCHEMA AND c.TABLE_CATALOG = cu.TABLE_CATALOG AND c.TABLE_NAME = cu.TABLE_NAME
                        
						    LEFT OUTER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS k on k.TABLE_SCHEMA = cu.TABLE_SCHEMA AND k.TABLE_CATALOG = cu.TABLE_CATALOG AND k.TABLE_NAME = cu.TABLE_NAME AND k.COLUMN_NAME = c.COLUMN_NAME
                            LEFT OUTER JOIN [dbo].[Custom_Tables] [crmtable] on [crmtable].[Bord_Name] = cu.TABLE_NAME AND [Bord_Deleted] IS NULL

						    --LEFT OUTER JOIN [dbo].[Custom_Captions] [crmCaptions] on [crmCaptions].[Capt_Code] = c.COLUMN_NAME and [crmCaptions].[capt_family] != 'GenCaptions' 
                            LEFT OUTER JOIN [dbo].[Custom_Captions] [crmCaptions] on [crmCaptions].[Capt_Code] = c.COLUMN_NAME and [crmCaptions].[capt_family] = 'ColNames' and [crmCaptions].[Capt_FamilyType] = 'Tags' -- Added this to minimise the metadata load from 66590 to 51934 stock CRM db
						    left outer join [dbo].[Custom_Captions] [crmCaptions2] on [crmCaptions2].[capt_family] = c.COLUMN_NAME 

                            LEFT OUTER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc on tc .CONSTRAINT_NAME = k .CONSTRAINT_NAME -- this should provide the correct primary keys
						    WHERE cu.TABLE_SCHEMA = 'dbo'
							and crmCaptions2.Capt_Deleted is null
							and crmCaptions.Capt_Deleted is null
                            and cu.table_name LIKE '_v" + c + "%'";

                                #endregion

                                var metadataQueryResults = con.Query<Metadata>(metadataQuery).GroupBy(b => b.objectname.ToLower()).ToDictionary(g => g.Key.ToLower(), g => g.ToList());

                                foreach (var result in metadataQueryResults)
                                {
                                    if (!metadataList.ContainsKey(result.Key))
                                    {
                                        metadataList.Add(result.Key, result.Value);
                                    }
                                }
                            }

                            #endregion
                            break;
                        }
                    case "crmtables":
                        {
                            #region SAGE CRM TABLES
                            Console.WriteLine("LOADING SAGE CRM TABLES...");
                            foreach (char c in alphabet)
                            {
                                #region Query

                                metadataQuery = @"SELECT DISTINCT
                            --cu.TABLE_CATALOG as [database],
                            --cu.TABLE_SCHEMA as [schema],
                            cu.TABLE_NAME as [objectname],
                            c.COLUMN_NAME as [columnname],
                            CONVERT(int,c.ORDINAL_POSITION) as [position],
                            CASE 
                            WHEN c.Data_type IN ('char' , 'varchar', 'text', 'ntext','nvarchar', 'ntext', 'nchar') THEN 'String'
                            WHEN c.Data_type IN ('bit') THEN 'Boolean'
                            WHEN c.Data_type = 'tinyint' THEN 'int'
                            ELSE c.Data_type
                            END as [datatype],
                            CASE
                               WHEN ((k .ORDINAL_POSITION IS NULL) AND ([crmtable]. Bord_IdField != c. COLUMN_NAME)) THEN 0
                               --WHEN ((k.ORDINAL_POSITION IS NOT NULL) OR ([crmtable].Bord_IdField = c.COLUMN_NAME)) THEN 1 -- pete's original
                                                 WHEN ( (tc. CONSTRAINT_TYPE = 'PRIMARY KEY') AND ( tc.CONSTRAINT_NAME = k.CONSTRAINT_NAME )
                                                        --OR ([crmtable].Bord_IdField = c.COLUMN_NAME)
                                                        )THEN 1
                                                 ELSE 0
                               end
                            AS [isprimarykey] ,



                            CASE 
	                            WHEN c.IS_NULLABLE = 'NO' THEN 0
	                            WHEN c.IS_NULLABLE = 'YES' THEN 1
	                            END AS [isnullable], 
                            CONVERT(int,c.CHARACTER_MAXIMUM_LENGTH) as [characterlength],
                            CONVERT(int,c.NUMERIC_PRECISION) as [numericprecision],
                            CONVERT(int,c.NUMERIC_SCALE) as [numericscale],
                            CONVERT(int,c.DATETIME_PRECISION) as [datetimeprecision],
						    CAST(crmCaptions.Capt_US as nvarchar(max)) as [translation],
						    CAST(crmCaptions2.capt_code as nvarchar(max)) as [captionCode],
						    CAST(crmCaptions2.Capt_US as nvarchar(max)) as [translationValue]

                            FROM INFORMATION_SCHEMA.TABLES AS cu
                            INNER JOIN INFORMATION_SCHEMA.COLUMNS AS c ON c.TABLE_SCHEMA = cu.TABLE_SCHEMA AND c.TABLE_CATALOG = cu.TABLE_CATALOG AND c.TABLE_NAME = cu.TABLE_NAME
                        
						    LEFT OUTER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS k on k.TABLE_SCHEMA = cu.TABLE_SCHEMA AND k.TABLE_CATALOG = cu.TABLE_CATALOG AND k.TABLE_NAME = cu.TABLE_NAME AND k.COLUMN_NAME = c.COLUMN_NAME
                            LEFT OUTER JOIN [dbo].[Custom_Tables] [crmtable] on [crmtable].[Bord_Name] = cu.TABLE_NAME AND [Bord_Deleted] IS NULL

                            --LEFT OUTER JOIN [dbo].[Custom_Captions] [crmCaptions] on [crmCaptions].[Capt_Code] = c.COLUMN_NAME and [crmCaptions].[capt_family] != 'GenCaptions' 
                            LEFT OUTER JOIN [dbo].[Custom_Captions] [crmCaptions] on [crmCaptions].[Capt_Code] = c.COLUMN_NAME and [crmCaptions].[capt_family] = 'ColNames' and [crmCaptions].[Capt_FamilyType] = 'Tags' -- Added this to minimise the metadata load from 66590 to 51934 stock CRM db
						    left outer join [dbo].[Custom_Captions] [crmCaptions2] on [crmCaptions2].[capt_family] = c.COLUMN_NAME 

                            LEFT OUTER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc on tc .CONSTRAINT_NAME = k .CONSTRAINT_NAME -- this should provide the correct primary keys
						    WHERE cu.TABLE_SCHEMA = 'dbo'
							and crmCaptions2.Capt_Deleted is null
							and crmCaptions.Capt_Deleted is null
                            and cu.table_name LIKE '" + c + "%'";

                                #endregion

                                var metadataQueryResults = con.Query<Metadata>(metadataQuery).GroupBy(b => b.objectname.ToLower()).ToDictionary(g => g.Key.ToLower(), g => g.ToList());

                                foreach (var result in metadataQueryResults)
                                {
                                    if (!metadataList.ContainsKey(result.Key))
                                    {
                                        metadataList.Add(result.Key, result.Value);
                                    }
                                }
                            }

                            #endregion

                            break;
                        }
                    case "crmviews":
                        {
                            #region SAGE CRM VIEWS

                            Console.WriteLine("LOADING SAGE CRM VIEWS...");
                            foreach (char c in alphabet)
                            {
                                #region Query

                                metadataQuery = @"SELECT DISTINCT
                            --cu.TABLE_CATALOG as [database],
                            --cu.TABLE_SCHEMA as [schema],
                            cu.TABLE_NAME as [objectname],
                            c.COLUMN_NAME as [columnname],
                            CONVERT(int,c.ORDINAL_POSITION) as [position],
                            CASE 
                            WHEN c.Data_type IN ('char' , 'varchar', 'text', 'ntext','nvarchar', 'ntext', 'nchar') THEN 'String'
                            WHEN c.Data_type IN ('bit') THEN 'Boolean'
                            WHEN c.Data_type = 'tinyint' THEN 'int'
                            ELSE c.Data_type
                            END as [datatype],
                            CASE
                               WHEN ((k .ORDINAL_POSITION IS NULL) AND ([crmtable]. Bord_IdField != c. COLUMN_NAME)) THEN 0
                               --WHEN ((k.ORDINAL_POSITION IS NOT NULL) OR ([crmtable].Bord_IdField = c.COLUMN_NAME)) THEN 1 -- pete's original
                                                 WHEN ( (tc. CONSTRAINT_TYPE = 'PRIMARY KEY') AND ( tc.CONSTRAINT_NAME = k.CONSTRAINT_NAME )
                                                        --OR ([crmtable].Bord_IdField = c.COLUMN_NAME)
                                                        )THEN 1
                                                 ELSE 0
                               end
                            AS [isprimarykey] ,

                            CASE 
	                            WHEN c.IS_NULLABLE = 'NO' THEN 0
	                            WHEN c.IS_NULLABLE = 'YES' THEN 1
	                            END AS [isnullable], 
                            CONVERT(int,c.CHARACTER_MAXIMUM_LENGTH) as [characterlength],
                            CONVERT(int,c.NUMERIC_PRECISION) as [numericprecision],
                            CONVERT(int,c.NUMERIC_SCALE) as [numericscale],
                            CONVERT(int,c.DATETIME_PRECISION) as [datetimeprecision],
						    CAST(crmCaptions.Capt_US as nvarchar(max)) as [translation],
						    CAST(crmCaptions2.capt_code as nvarchar(max)) as [captionCode],
						    CAST(crmCaptions2.Capt_US as nvarchar(max)) as [translationValue]

                            FROM INFORMATION_SCHEMA.TABLES AS cu
                            INNER JOIN INFORMATION_SCHEMA.COLUMNS AS c ON c.TABLE_SCHEMA = cu.TABLE_SCHEMA AND c.TABLE_CATALOG = cu.TABLE_CATALOG AND c.TABLE_NAME = cu.TABLE_NAME
                        
						    LEFT OUTER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS k on k.TABLE_SCHEMA = cu.TABLE_SCHEMA AND k.TABLE_CATALOG = cu.TABLE_CATALOG AND k.TABLE_NAME = cu.TABLE_NAME AND k.COLUMN_NAME = c.COLUMN_NAME
                            LEFT OUTER JOIN [dbo].[Custom_Tables] [crmtable] on [crmtable].[Bord_Name] = cu.TABLE_NAME AND [Bord_Deleted] IS NULL

						    --LEFT OUTER JOIN [dbo].[Custom_Captions] [crmCaptions] on [crmCaptions].[Capt_Code] = c.COLUMN_NAME and [crmCaptions].[capt_family] != 'GenCaptions' 
                            LEFT OUTER JOIN [dbo].[Custom_Captions] [crmCaptions] on [crmCaptions].[Capt_Code] = c.COLUMN_NAME and [crmCaptions].[capt_family] = 'ColNames' and [crmCaptions].[Capt_FamilyType] = 'Tags' -- Added this to minimise the metadata load from 66590 to 51934 stock CRM db
						    left outer join [dbo].[Custom_Captions] [crmCaptions2] on [crmCaptions2].[capt_family] = c.COLUMN_NAME 

                            LEFT OUTER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc on tc .CONSTRAINT_NAME = k .CONSTRAINT_NAME -- this should provide the correct primary keys
						    WHERE cu.TABLE_SCHEMA = 'dbo'
							and crmCaptions2.Capt_Deleted is null
							and crmCaptions.Capt_Deleted is null
                            and cu.table_name LIKE 'v" + c + "%'";

                                #endregion

                                var metadataQueryResults = con.Query<Metadata>(metadataQuery).GroupBy(b => b.objectname.ToLower()).ToDictionary(g => g.Key.ToLower(), g => g.ToList());

                                foreach (var result in metadataQueryResults)
                                {
                                    if (!metadataList.ContainsKey(result.Key))
                                    {
                                        metadataList.Add(result.Key, result.Value);
                                    }
                                }
                            }

                            #endregion

                            break;
                        }
                    case "aftables":
                        {
                            #region AF TABLES
                            Console.WriteLine("LOADING AF TABLES...");
                            foreach (char c in alphabet)
                            {
                                #region Query

                                metadataQuery = @"SELECT DISTINCT
                            --cu.TABLE_CATALOG as [database],
                            --cu.TABLE_SCHEMA as [schema],
                            cu.TABLE_NAME as [objectname],
                            c.COLUMN_NAME as [columnname],
                            CONVERT(int,c.ORDINAL_POSITION) as [position],
                            CASE 
                            WHEN c.Data_type IN ('char' , 'varchar', 'text', 'ntext','nvarchar', 'ntext', 'nchar') THEN 'String'
                            WHEN c.Data_type IN ('bit') THEN 'Boolean'
                            WHEN c.Data_type = 'tinyint' THEN 'int'
                            ELSE c.Data_type
                            END as [datatype],
                            CASE
                               WHEN ((k .ORDINAL_POSITION IS NULL) AND ([crmtable]. Bord_IdField != c. COLUMN_NAME)) THEN 0
                               --WHEN ((k.ORDINAL_POSITION IS NOT NULL) OR ([crmtable].Bord_IdField = c.COLUMN_NAME)) THEN 1 -- pete's original
                                                 WHEN ( (tc. CONSTRAINT_TYPE = 'PRIMARY KEY') AND ( tc.CONSTRAINT_NAME = k.CONSTRAINT_NAME )
                                                        --OR ([crmtable].Bord_IdField = c.COLUMN_NAME)
                                                        )THEN 1
                                                 ELSE 0
                               end
                            AS [isprimarykey] ,

                            CASE 
	                            WHEN c.IS_NULLABLE = 'NO' THEN 0
	                            WHEN c.IS_NULLABLE = 'YES' THEN 1
	                            END AS [isnullable], 
                            CONVERT(int,c.CHARACTER_MAXIMUM_LENGTH) as [characterlength],
                            CONVERT(int,c.NUMERIC_PRECISION) as [numericprecision],
                            CONVERT(int,c.NUMERIC_SCALE) as [numericscale],
                            CONVERT(int,c.DATETIME_PRECISION) as [datetimeprecision],
						    CAST(crmCaptions.Capt_US as nvarchar(max)) as [translation],
						    CAST(crmCaptions2.capt_code as nvarchar(max)) as [captionCode],
						    CAST(crmCaptions2.Capt_US as nvarchar(max)) as [translationValue]

                            FROM INFORMATION_SCHEMA.TABLES AS cu
                            INNER JOIN INFORMATION_SCHEMA.COLUMNS AS c ON c.TABLE_SCHEMA = cu.TABLE_SCHEMA AND c.TABLE_CATALOG = cu.TABLE_CATALOG AND c.TABLE_NAME = cu.TABLE_NAME
                        
						    LEFT OUTER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS k on k.TABLE_SCHEMA = cu.TABLE_SCHEMA AND k.TABLE_CATALOG = cu.TABLE_CATALOG AND k.TABLE_NAME = cu.TABLE_NAME AND k.COLUMN_NAME = c.COLUMN_NAME
                            LEFT OUTER JOIN [dbo].[Custom_Tables] [crmtable] on [crmtable].[Bord_Name] = cu.TABLE_NAME AND [Bord_Deleted] IS NULL

						    --LEFT OUTER JOIN [dbo].[Custom_Captions] [crmCaptions] on [crmCaptions].[Capt_Code] = c.COLUMN_NAME and [crmCaptions].[capt_family] != 'GenCaptions' 
                            LEFT OUTER JOIN [dbo].[Custom_Captions] [crmCaptions] on [crmCaptions].[Capt_Code] = c.COLUMN_NAME and [crmCaptions].[capt_family] = 'ColNames' and [crmCaptions].[Capt_FamilyType] = 'Tags' -- Added this to minimise the metadata load from 66590 to 51934 stock CRM db
						    left outer join [dbo].[Custom_Captions] [crmCaptions2] on [crmCaptions2].[capt_family] = c.COLUMN_NAME 

                            LEFT OUTER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc on tc .CONSTRAINT_NAME = k .CONSTRAINT_NAME -- this should provide the correct primary keys
						    WHERE cu.TABLE_SCHEMA = 'dbo'
							and crmCaptions2.Capt_Deleted is null
							and crmCaptions.Capt_Deleted is null
                            and cu.table_name LIKE '_t" + c + "%'";

                                #endregion

                                var metadataQueryResults = con.Query<Metadata>(metadataQuery).GroupBy(b => b.objectname.ToLower()).ToDictionary(g => g.Key.ToLower(), g => g.ToList());

                                foreach (var result in metadataQueryResults)
                                {
                                    if (!metadataList.ContainsKey(result.Key))
                                    {
                                        metadataList.Add(result.Key, result.Value);
                                    }
                                }
                            }

                            #endregion

                            break;
                        }
                    case "afviews":
                        {
                            #region AF VIEWS
                            Console.WriteLine("LOADING AF VIEWS...");
                            foreach (char c in alphabet)
                            {
                                #region Query

                                metadataQuery = @"SELECT DISTINCT
                            --cu.TABLE_CATALOG as [database],
                            --cu.TABLE_SCHEMA as [schema],
                            cu.TABLE_NAME as [objectname],
                            c.COLUMN_NAME as [columnname],
                            CONVERT(int,c.ORDINAL_POSITION) as [position],
                            CASE 
                            WHEN c.Data_type IN ('char' , 'varchar', 'text', 'ntext','nvarchar', 'ntext', 'nchar') THEN 'String'
                            WHEN c.Data_type IN ('bit') THEN 'Boolean'
                            WHEN c.Data_type = 'tinyint' THEN 'int'
                            ELSE c.Data_type
                            END as [datatype],
                            CASE
                               WHEN ((k .ORDINAL_POSITION IS NULL) AND ([crmtable]. Bord_IdField != c. COLUMN_NAME)) THEN 0
                               --WHEN ((k.ORDINAL_POSITION IS NOT NULL) OR ([crmtable].Bord_IdField = c.COLUMN_NAME)) THEN 1 -- pete's original
                                                 WHEN ( (tc. CONSTRAINT_TYPE = 'PRIMARY KEY') AND ( tc.CONSTRAINT_NAME = k.CONSTRAINT_NAME )
                                                        --OR ([crmtable].Bord_IdField = c.COLUMN_NAME)
                                                        )THEN 1
                                                 ELSE 0
                               end
                            AS [isprimarykey] ,

                            CASE 
	                            WHEN c.IS_NULLABLE = 'NO' THEN 0
	                            WHEN c.IS_NULLABLE = 'YES' THEN 1
	                            END AS [isnullable], 
                            CONVERT(int,c.CHARACTER_MAXIMUM_LENGTH) as [characterlength],
                            CONVERT(int,c.NUMERIC_PRECISION) as [numericprecision],
                            CONVERT(int,c.NUMERIC_SCALE) as [numericscale],
                            CONVERT(int,c.DATETIME_PRECISION) as [datetimeprecision],
						    CAST(crmCaptions.Capt_US as nvarchar(max)) as [translation],
						    CAST(crmCaptions2.capt_code as nvarchar(max)) as [captionCode],
						    CAST(crmCaptions2.Capt_US as nvarchar(max)) as [translationValue]

                            FROM INFORMATION_SCHEMA.TABLES AS cu
                            INNER JOIN INFORMATION_SCHEMA.COLUMNS AS c ON c.TABLE_SCHEMA = cu.TABLE_SCHEMA AND c.TABLE_CATALOG = cu.TABLE_CATALOG AND c.TABLE_NAME = cu.TABLE_NAME
                        
						    LEFT OUTER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS k on k.TABLE_SCHEMA = cu.TABLE_SCHEMA AND k.TABLE_CATALOG = cu.TABLE_CATALOG AND k.TABLE_NAME = cu.TABLE_NAME AND k.COLUMN_NAME = c.COLUMN_NAME
                            LEFT OUTER JOIN [dbo].[Custom_Tables] [crmtable] on [crmtable].[Bord_Name] = cu.TABLE_NAME AND [Bord_Deleted] IS NULL

						    --LEFT OUTER JOIN [dbo].[Custom_Captions] [crmCaptions] on [crmCaptions].[Capt_Code] = c.COLUMN_NAME and [crmCaptions].[capt_family] != 'GenCaptions' 
                            LEFT OUTER JOIN [dbo].[Custom_Captions] [crmCaptions] on [crmCaptions].[Capt_Code] = c.COLUMN_NAME and [crmCaptions].[capt_family] = 'ColNames' and [crmCaptions].[Capt_FamilyType] = 'Tags' -- Added this to minimise the metadata load from 66590 to 51934 stock CRM db
						    left outer join [dbo].[Custom_Captions] [crmCaptions2] on [crmCaptions2].[capt_family] = c.COLUMN_NAME 

                            LEFT OUTER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc on tc .CONSTRAINT_NAME = k .CONSTRAINT_NAME -- this should provide the correct primary keys
						    WHERE cu.TABLE_SCHEMA = 'dbo'
							and crmCaptions2.Capt_Deleted is null
							and crmCaptions.Capt_Deleted is null
                            and cu.table_name LIKE '_v" + c + "%'";

                                #endregion

                                var metadataQueryResults = con.Query<Metadata>(metadataQuery).GroupBy(b => b.objectname.ToLower()).ToDictionary(g => g.Key.ToLower(), g => g.ToList());

                                foreach (var result in metadataQueryResults)
                                {
                                    if (!metadataList.ContainsKey(result.Key))
                                    {
                                        metadataList.Add(result.Key, result.Value);
                                    }
                                }
                            }

                            #endregion

                            break;
                        }
                }
            }

            return metadataList;
        }

        public static Dictionary<String, List<Activity>> GetUserMetadata()
        {
            //Dictionary<String, List<Activity>> _activity2 = new Dictionary<string,List<Activity>>();
            var connectionfactory = new ConnectionFactory();

            using (var con = connectionfactory.GetOpenConnection())
            {

                #region activityView

                string activityView = @"SELECT
                                acty_sid AS [sid],
                                acty_userlogon AS [userLogon],
                                acty_login AS [loginTime],
                                user_fullname AS [userFullName],
                                user_department AS [userDepartment],
                                user_firstname AS [userFirstName],
                                user_lastname AS [userLastName],
                                user_userid AS [id],
                                user_per_admin AS [userPerAdmin]
                            FROM _vAFLoggedInUsers WHERE acty_sid IS NOT NULL";

                #endregion

                Dictionary<String, List<Activity>> listActivity = new Dictionary<string, List<Activity>>();

                Console.WriteLine("LOADING USER ACTIVITY");
                var userActivity = con.Query<Activity>(activityView).GroupBy(b => b.sid.ToLower().TrimEnd()).ToDictionary(g => g.Key.ToLower(), g => g.ToList());

                listActivity = userActivity;

                _activity = listActivity;
            }

            return _activity;

        }

        public static void TestRefresh()
        {
            Timer _timer = new Timer();
            _timer.Elapsed += new ElapsedEventHandler(OnTimedEvent);

            _timer.Interval = 900000; //run every 15 minutes
            //_timer.Interval = 30000; //run every 30 seconds
            //_timer.Interval = 2000; //run every 2 seconds
            _timer.Enabled = true;

            GC.KeepAlive(_timer);
        }

        public static void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            //perform the re-caching here
            GetUserMetadata();

            Console.WriteLine("User Metadata Referesh Occured in :{0}", e.SignalTime);
            log.Info("User Metadata Referesh Occured in : " + e.SignalTime.ToString());
        }




    }
}


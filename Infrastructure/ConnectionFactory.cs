﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace accsys.crmframework.api.infrastructure
{
    public class ConnectionFactory
    {
        
        public DbConnection GetOpenConnection()
        {
            var connString = ConfigurationManager.ConnectionStrings["connString"].ConnectionString;

            SqlConnection conn = new SqlConnection(connString);



            conn.Open();
            return conn;
        }


    }

}

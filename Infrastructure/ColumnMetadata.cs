﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace accsys.crmframework.api.infrastructure
{
   public class ColumnMetadataFields
    {
        public string primarykey { get; set; }
        public string updatedby { get; set; }
        public string deleted { get; set; }
        public string updateddate { get; set; }
        public string createdby { get; set; }
        public string createddate { get; set; }
        //not too sure if need to add a new field here for the table type
    }
}

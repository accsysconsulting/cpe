﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace accsys.crmframework.api.infrastructure
{
    /* This class should replace the standard payload that gets sent to the UI
     * It should follow what Alwyn's email said:
     * {
            "onbo_stage":
            {
                "value":"xyz",
                "translation":"abc"
            },
   
            "onbo_field":
            {
                "value":"123",
                "translation":"987"
            }
       }
     * 
     */
    public class PayloadList
    {
        public string fieldName { get; set; } //use this for later
        public List<PayloadTemplate> payloadList { get; set; }
    }
}

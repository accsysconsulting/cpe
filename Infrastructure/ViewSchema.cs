﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace accsys.crmframework.api.infrastructure
{
    public class ViewSchema
    {
        public string viewName { get; set; }
        public string tableName { get; set; }
        public string columnName { get; set; }
        public string isprimarykey { get; set; }
        public string value { get; set; }
        //public string tableName { get; set; }
        //public string columnName { get; set; }

    }
}

CREATE VIEW [dbo].[_vAFLoggedInUsers]
AS
SELECT Activity.acty_SID AS 'acty_sid', Activity.acty_UserLogon AS 'acty_userlogon', Activity.acty_Login AS 'acty_login', Users.User_UserId AS 'user_userid', LTRIM(RTRIM(ISNULL(Users.User_FirstName,''))) + ' ' + LTRIM(RTRIM(ISNULL(Users.User_LastName,''))) AS 'user_fullname', Acty_Deleted AS 'acty_deleted', User_Department AS 'user_department', User_FirstName AS 'user_firstname', User_LastName as 'user_lastname', user_per_admin as 'user_per_admin'
FROM Activity
	INNER JOIN Users
		ON Users.User_UserId = Activity.acty_UserID
WHERE acty_logout IS NULL
AND acty_sid IS NOT NULL


GO



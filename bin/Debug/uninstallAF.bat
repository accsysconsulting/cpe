@ECHO OFF
echo Uninstalling Accsys Framework API
echo ---------------------------------------------------
accsys.crmframework.api stop --sudo
accsys.crmframework.api uninstall --sudo
echo ---------------------------------------------------
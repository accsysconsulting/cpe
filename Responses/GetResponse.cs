﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Web.UI.WebControls;
using accsys.crmframework.api.infrastructure;
using Nancy;
using Nancy.Responses;
using Nancy.Serializers.Json.ServiceStack;
using Dapper;

namespace accsys.crmframework.api.Responses
{
    public class GetResponse : JsonResponse
    {
        readonly GetResponsePayload GetResponsePayload;

        private GetResponse(GetResponsePayload getresponse)
            : base(getresponse, new ServiceStackJsonSerializer())
        {
            this.GetResponsePayload = getresponse;
        }

        public static GetResponse GenerateGetResponse(NancyContext context, List<object> queryResults)
        {
           return new GetResponse(new GetResponsePayload { _uri = context.Request.Url.ToString(), items = queryResults });
        }
    }
}
